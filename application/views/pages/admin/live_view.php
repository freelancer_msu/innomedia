<div class="row mt-20">
    <div class="col-md-12">
        <div class="panel panel-default card-view">
            <div class="panel-heading">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">ถ่ายทอดสด</h6>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-wrap">
                                <form action="<?php echo base_url();?>admin/live/update" method="post">
                                    <div class="form-body">
                                        <h6 class="txt-dark"><i class='fa fa-bars mr-10'></i></i>รายละเอียด</h6>
                                        <hr class="light-grey-hr">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php
                                        $res_msg = $this->session->flashdata('live_respons');
                                        if($res_msg != ''){
                                            ?>
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">×</button><?=$res_msg;?>
                                                </div>
                                                <?php
                                        }
                                        ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label mb-10">ชื่อเรื่อง</label>
                                                    <input type="text" id="name" name="name" class="form-control"
                                                        placeholder="ชื่อเรื่องการถ่ายทอดสด" value="<?=$data["name"];?>">
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label mb-10">รายละเอียด</label>
                                                    <input type="text" id="detail" name="detail" class="form-control"
                                                        placeholder="รายละเอียด" value="<?=$data["detail"];?>">
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label mb-10">เอ็มเบจโค้ด (Embed Code)</label>
                                                    <textarea type="text" id="embed" name="embed" class="form-control"
                                                        placeholder="<iframe src='https://www.youtube.com/embed/....></iframe>"><?=$data["embed"];?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-default btn-xs"
                                                        id="preview">แสดงตัวอย่าง</button>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!-- /Row -->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label mb-10">สถานะ</label>
                                                    <select class="form-control" tabindex="1" id="status" name="status">
                                                        <option value="active"
                                                            <?=($data['status'] == 'active') ? 'selected':'';?>>เผยแพร่
                                                        </option>
                                                        <option value="inactive"
                                                            <?=($data['status'] == 'inactive') ? 'selected':'';?>>
                                                            ไม่เผยแพร่</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!-- /Row -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-actions mt-10">
                                                    <button type="submit" class="btn btn-success mr-10">บันทึก</button>
                                                    <button type="button" class="btn btn-default">ยกเลิก</button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="seprator-block"></div>

                                        <h6 class="txt-dark"><i class='fa fa-video-camera mr-10'></i></i>รายละเอียด</h6>
                                        <hr class="light-grey-hr">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div id="live-video" style="min-height: 150px;">
                                                    ....รอการถ่ายทอดสด....
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    var embed = $("#embed").val();
    if (embed !== '') {
        $("#live-video").html(embed);
    } else {
        $("#live-video").html('....รอการถ่ายทอดสด....');
    }
    console.log('TEST');
    $("#preview").click(function() {
        var embed = $("#embed").val();
        if (embed !== '') {
            $("#live-video").html(embed);
        } else {
            $("#live-video").html('....รอการถ่ายทอดสด....');
        }
    });
});
</script>