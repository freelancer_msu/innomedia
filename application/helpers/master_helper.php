<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('admin_menu'))
{
	// helper menu with language
	function admin_menu($id=null) {
		$lang_menu = load_lang();
		$_menu = [
			(object)["id"=> 1, "name"=>$lang_menu->lang->line('menu_home'), "active"=> false, "link"=> base_url(),
		], 
		];
		if(!empty($id)){
			foreach ($_menu as $menu) {
				if($menu->id==$id)
					$menu->active = true;
			}
		}
		return $_menu;
	}
}

if ( ! function_exists('load_lang')) {
	function load_lang(){
		$CI = & get_instance();
		if($CI->session->userdata('language')==NULL){
			$lang = "thai";
			$CI->session->set_userdata('language',$lang);
		}else{
			$lang = $CI->session->userdata('language');
		}
		return $CI;
	}
}

if ( ! function_exists('load_style'))
{
	function load_style() {
		return [

		];
	}
}

if ( ! function_exists('load_script'))
{
	function load_script() {
		return [

		];
	}
}

if ( ! function_exists('load_admin_style'))
{
	function load_admin_style() {
		return [
			base_url('assets/css/style.css')
		];
	}
}

if ( ! function_exists('load_admin_script'))
{
	function load_admin_script() {
		return [
			base_url('assets/js/jquery.slimscroll.js'),
			base_url('vendors/bootstrap/dist/js/bootstrap.min.js'),
			base_url('vendors/owl.carousel/dist/owl.carousel.min.js'),
			base_url('vendors/switchery/dist/switchery.min.js'),
			base_url('assets/js/dropdown-bootstrap-extended.js'),
			// base_url('assets/js/init.js')
		];
	}
}