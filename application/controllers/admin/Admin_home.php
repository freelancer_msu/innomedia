<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_home extends Core_Acontroller {

	public $title = 'Home';

	public function __construct()
	{
		parent::__construct();
	}

	public function index() {
		// $this->load_admin_index_script();
		$data['main_menu'] = $this->load_main_menu();
		$data['permission'] = $this->load_permission();
		$data['page_title'] = 'หน้าหลัก';
		$this->breadcrumb($this->title,base_url('admin/home'));
		$this->response('home',$data);
	}
}