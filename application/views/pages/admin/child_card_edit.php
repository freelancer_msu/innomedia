<!-- switchery CSS -->
<link href="<?=base_url('vendors/switchery/dist/switchery.min.css')?>
" rel="stylesheet" type="text/css"/>
<!-- multi-select CSS -->
<link href="<?=base_url('vendors/multiselect/css/multi-select.css')?>" rel="stylesheet" type="text/css"/>

<link href="<?=base_url('vendors/jasny-bootstrap/dist/css/jasny-bootstrap.min.css')?>" rel="stylesheet" type="text/css"/>

<!-- Row -->
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">ข้อมูลเด็กพิเศษ</h6>
				</div>
				<div class="pull-right">
				<a class="btn btn-xs btn-default" href="<?=base_url('admin/child')?>">ย้อนกลับ</a></div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-12 col-xs-12">
							<div class="form-wrap">
								<form action="#">
									<div class="form-body">
										<h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account mr-10"></i>ข้อมูลส่วนตัว</h6>
										<hr class="light-grey-hr"/>
										<div class="row">
											<div class="col-md-2">
												<div class="form-group">
													<label class="control-label mb-10">คำนำหน้า <text class="mandatory">*</text></label>
													<input type="text" id="firstName" class="form-control" placeholder="">
												</div>
											</div>
											<div class="col-md-5">
												<div class="form-group">
													<label class="control-label mb-10">ชื่อจริง <text class="mandatory">*</text></label>
													<input type="text" id="firstName" class="form-control" placeholder="" value="<?=$child_data->chi_first_name?>">
												</div>
											</div>
											<!--/span-->
											<div class="col-md-5">
												<div class="form-group">
													<label class="control-label mb-10">นามสกุล <text class="mandatory">*</text></label>
													<input type="text" id="lastName" class="form-control" placeholder="" value="<?=$child_data->chi_last_name?>">
												</div>
											</div>
											<!--/span-->
										</div>
										<!-- /Row -->
										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label mb-10">เพศ</label>
													<select class="form-control">
														<option <?=$child_data->chi_gender=='M'?'checked':''?> value="M">ชาย</option>
														<option <?=$child_data->chi_gender=='F'?'checked':''?> value="F">หญิง</option>
													</select>
												</div>
											</div>
											<!--/span-->
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label mb-10">รหัสประชาชน</label>

													<input type="text" placeholder="" class="form-control chi-iden-edit" value="<?=$child_data->chi_identification?>">
													<span class="help-block" id="status_label">หมายเลขประจำตัวประชาชน 13 หลัก</span>
												</div>
											</div>
											<!--/span-->
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">สถานะ </label>
													<div>
														<input type="checkbox" <?=$child_data->chi_is_active==1?'checked':''?> class="js-switch-insert js-switch-insert-1"  data-color="#f1a1c7"/>
													</div>
													<span class="help-block" id="status_label">สถานะตอนนี้ : <text id="current_statud"></text></span>
												</div>
											</div>
										</div>
										<!-- /Row -->
										<div class="row">
											<div class="col-md-10">
												<div class="form-group">
													<label class="control-label mb-10">ประเภทเด็กพิเศษ</label>
													<div class="w-100">
														<select class="child-type form-control" multiple name="child_type[]" data-role="tagsinput">
															<?php foreach ($all_child_types as $type) { 
																$checked = '';
																if(in_array($type->ct_code,$child_in_type)){
																	$checked = 'selected';
																}
															?>
																<option <?=$checked?> value="<?=$type->ct_code?>"><?=$type->ct_description?></option>
															<?php } ?>
														</select>
													</div>
													<div class="pull-left">
														<span class="help-block">รายการที่ยังไม่ได้เลือก</span>
													</div>
													<div class="pull-right">
														<span class="help-block">รายการที่ทำการเลือกแล้ว</span>
													</div>
												</div>
											</div>
											<div class="col-md-2">
												<div class="form-group">
													<label class="control-label mb-10"></label>
													<div class="button-box"> 
														<a id="select-all" class="btn btn-danger btn-outline mr-10 mt-15" href="#">เลือกทั้งหมด</a> 
														<a id="deselect-all" class="btn btn-info btn-outline mr-10 mt-15" href="#">ไม่เลือกทั้งหมด</a>
													</div>
												</div>
											</div>
										</div>
										<!-- /Row -->
										<hr class="light-grey-hr"/>
										<div class="row">
											<div class="col-md-9">
												<div class="form-group">
													<label class="control-label mb-10">ผู้ปกครอง <text class="mandatory">*</text></label>
													<input type="text" id="firstName" class="form-control" placeholder="">
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label mb-10">ที่อยู่ของเด็กพิเศษ</label>
													<div class="radio">
														<input type="radio" name="add_type" id="add_type1" value="1" checked>
														<label for="add_type1"> ที่อยู่เดียวกันกับผู้ปกครอง </label>
													</div>
													<div class="radio radio-custom">
														<input type="radio" name="add_type" id="add_type2" value="0">
														<label for="add_type2"> ที่อยู่เฉพาะ (โปรดระบุ)</label>
													</div>
												</div>
											</div>
										</div>
										<!-- /Row -->

										<div class="seprator-block"></div>
										<div class="address-panel"><!--address-panel-->
											
											<h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i>ที่อยู่</h6>
											<hr class="light-grey-hr"/>
											<div class="row">
												<div class="col-md-12 ">
													<div class="form-group">
														<label class="control-label mb-10">บ้านเลขที่ / อาคาร / หมู่ที่ / หมู่บ้าน</label>
														<input type="text" class="form-control">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label mb-10">รหัสไปรษณีย์</label>
														<input type="text" class="form-control">
													</div>
												</div>
												<!--/span-->
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label mb-10">ตำบล</label>
														<input type="text" class="form-control" readonly>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label mb-10">อำเภอ</label>
														<input type="text" class="form-control" readonly>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label mb-10">จังหวัด</label>
														<input type="text" class="form-control" readonly>
													</div>
												</div>
											</div>
											<!-- /Row -->
											
										</div> <!--/address-panel-->
									</div>
									<div class="form-actions mt-10">
										<a href="javascript:onbeforesubmit(this);" class="btn btn-success  mr-10"> บันทึก</a>
										<a href="<?=base_url('admin/child')?>" class="btn btn-default">ยกเลิก และย้อนกลับ</a>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
<!-- /Row -->

<!-- Switchery JavaScript -->
<script src="<?=base_url('vendors/switchery/dist/switchery.min.js')?>"></script>
<!-- Multiselect JavaScript -->
<script src="<?=base_url('vendors/multiselect/js/jquery.multi-select.js')?>"></script>
<script src="<?=base_url('vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js')?>"></script>

<script type="text/javascript">
	$(document).ready(function() {
		var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch-insert'));
		$('.js-switch-insert-1').each(function() {
			new Switchery($(this)[0], $(this).data());
		});
		$('.child-type').multiSelect();   
		$('#select-all').click(function(){
			$('.child-type').multiSelect('select_all');
			return false;
		});
		$('#deselect-all').click(function(){
			$('.child-type').multiSelect('deselect_all');
			return false;
		});
		$('.chi-iden-edit').inputmask({mask: "9-9999-99999-99-9"});
	});

	function onbeforesubmit(curr_form){

	}
</script>