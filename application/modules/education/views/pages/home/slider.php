    <section class="slider">
       <div class="fullwidthbanner-container">
					<div class="fullwidthbanner">
						<ul>
						<?php
						foreach ($slider as $key => $value) {
							?>
							<li data-transition="<?=$value->ws_transition?>" data-slotamount="1" data-masterspeed="300">
								<img class="img-fluid" src="<?=base_url($value->ws_image_path)?>" alt="">
                            </li>

						<?php
						}
						?>
						</ul>

						<div class="tp-bannertimer tp-bottom"></div>
					</div>
				</div>
</section><!--End slider