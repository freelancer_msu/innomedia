<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_districts extends Core_Acontroller
{
  public $title = 'ตำบล';
    public function __construct()
    {
        parent::__construct();
    $this->load->model('admin/districts_model');
    $this->breadcrumb('หน้าหลัก','admin/home');
    }

    public function index()
    {
      $data['main_menu'] = $this->load_main_menu();
      $data['page_title'] = $this->title;
      $data['page_module'] = 'districts';
      $data['source_url'] = 'admin/districts/list';
      $this->breadcrumb($this->title,'admin/districts');
      $this->response('district_list',$data);
    }

  public function districts_list(){
    if ($districts = $this->districts_model->_get_all()){
      $answer['data'] = array();
      $idx = 1;
      foreach ($districts as $district) {
        $row = array($district);
        $row['id'] = $idx++;
        $row['code'] = $district->district_code;
        $row['name_th'] = $district->district_name_th;
        $row['name_en'] = $district->district_name_en;
        $row['init'] = $district->amphure_name_th.' / '.$district->pro_name_th;
        $row['status'] = $district->district_active;
        array_push($answer['data'], $row);
      }
    }

    echo json_encode($answer);
  }

  public function grocery_crud_fn(){
        $this->config->load('grocery_crud');
    $crud = new grocery_CRUD();
      $crud->unset_jquery();
      $crud->unset_export();
      $crud->unset_print();
      $crud->unset_clone();

      $crud->set_table('inno_districts');
      $crud->set_subject('ข้อมูลตำบล');
      $crud->columns('district_code','district_name_th','district_name_en','district_amphures','district_province','district_active');
      $crud->display_as('district_code',' รหัสตำบล')
        ->display_as('district_name_th','ชื่อ (ไทย)')
        ->display_as('district_name_en','ชื่อ (Eng)')
        ->display_as('district_amphures','อำเภอ')
        ->display_as('district_province','จังหวัด')
        ->display_as('district_active','สถานะ');
      $crud->unset_columns('district_region');
      $crud->fields('district_code','district_name_th','district_name_en','district_amphures','district_province','district_active');

      $crud->set_relation('district_amphures','inno_amphures','{amphure_name_th}');
      $crud->set_relation('district_province','inno_province','{pro_name_th}');


      $output = $crud->render();
      $data['crud_data'] = $output->output;

      $this->script($output->js_files); // load js script from crud

      $this->style($output->css_files); // load css script from crud

      $data['main_menu'] = $this->load_main_menu();
      $this->breadcrumb($this->title,base_url('admin/home'));
      $this->response('home',$data);
  }

}
