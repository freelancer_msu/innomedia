<!-- switchery CSS -->
<link href="<?=base_url('vendors/switchery/dist/switchery.min.css')?>
" rel="stylesheet" type="text/css"/>
<!-- multi-select CSS -->
<link href="<?=base_url('vendors/multiselect/css/multi-select.css')?>" rel="stylesheet" type="text/css"/>

<link href="<?=base_url('vendors/jasny-bootstrap/dist/css/jasny-bootstrap.min.css')?>" rel="stylesheet" type="text/css"/>

<!-- Row -->
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">จังหวัด</h6>
				</div>
				<div class="pull-right">
				<a class="btn btn-xs btn-default" href="<?=base_url('admin/province')?>">ย้อนกลับ</a></div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-12 col-xs-12">
							<div class="form-wrap">
								<form action="#">
									<div class="form-body">
										<div class="row">
											<div class="col-md-2">
												<div class="form-group">
													<label class="control-label mb-10">รหัสอ้างอิง <text class="mandatory">*</text></label>
													<input type="text" id="firstName" class="form-control" placeholder="">
												</div>
											</div>
											<div class="col-md-5">
												<div class="form-group">
													<label class="control-label mb-10">ชื่อจังหวัด(ภาษาไทย) <text class="mandatory">*</text></label>
													<input type="text" id="firstName" class="form-control" placeholder="" value="">
												</div>
											</div>
											<div class="col-md-5">
												<div class="form-group">
													<label class="control-label mb-10">ชื่อจังหวัด(ภาษาอังกฤษ) <text class="mandatory">*</text></label>
													<input type="text" id="firstName" class="form-control" placeholder="" value="">
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label mb-10">ภูมิภาค</label>
													<select class="form-control">
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
													</select>
												</div>
											</div>
										</div>
										<!-- /Row -->
										<div class="row">
											
											<!--/span-->
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">สถานะ </label>
													<div>
														<input type="checkbox" class="js-switch-insert js-switch-insert-1"  data-color="#f1a1c7"/>
													</div>
													<span class="help-block" id="status_label">สถานะตอนนี้ : <text id="current_statud"></text></span>
												</div>
											</div>
										</div>
										<!-- /Row -->

										<div class="seprator-block"></div>
										
									</div>
									<div class="form-actions mt-10">
										<a href="javascript:onbeforesubmit(this);" class="btn btn-success  mr-10"> บันทึก</a>
										<a href="<?=base_url('admin/province')?>" class="btn btn-default">ยกเลิก และย้อนกลับ</a>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
<!-- /Row -->

<!-- Switchery JavaScript -->
<script src="<?=base_url('vendors/switchery/dist/switchery.min.js')?>"></script>
<!-- Multiselect JavaScript -->
<script src="<?=base_url('vendors/multiselect/js/jquery.multi-select.js')?>"></script>
<script src="<?=base_url('vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js')?>"></script>

<script type="text/javascript">
	$(document).ready(function() {
		var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch-insert'));
		$('.js-switch-insert-1').each(function() {
			new Switchery($(this)[0], $(this).data());
		});
		$('.child-type').multiSelect();   
		$('#select-all').click(function(){
			$('.child-type').multiSelect('select_all');
			return false;
		});
		$('#deselect-all').click(function(){
			$('.child-type').multiSelect('deselect_all');
			return false;
		});
	});

	function onbeforesubmit(curr_form){

	}
</script>