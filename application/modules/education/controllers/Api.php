<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends Core_Controller {

	public function __construct()
	{
        parent::__construct();
    }
    
    public function index()
	{
    }

    public function get_provice()
    {
        $this->load->model('general_model');
        $data = $this->general_model->_get_provice();
        echo json_encode($data);
    }

    public function get_amphures()
    {
        $this->load->model('general_model');

        $provice_id = $this->input->post('provice_id');
        $data = $this->general_model->_get_amphures_by_province($provice_id);
        echo json_encode($data);
    }

    public function get_districts()
    {
        $this->load->model('general_model');
        
        $provice_id = $this->input->post('provice_id');
        $amphure_id = $this->input->post('amphure_id');
        $data = $this->general_model->_get_districts_by_amphure($provice_id,$amphure_id);
        echo json_encode($data);
    }

}