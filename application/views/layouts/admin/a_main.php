<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title><?=$this->lang->line($title);?></title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content=""/>
	
	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<?php $this->load->view($default_element."head_loader");?>
</head>

<body>
	<!--Preloader-->
	<div class="preloader-it">
		<div class="la-anim-1"></div>
	</div>
	<!--/Preloader-->
	<div class="wrapper theme-1-active pimary-color-green">

		<!-- Top Menu Items -->
		<?php $this->load->view($default_element.'top_menu'); ?>
		<!-- /Top Menu Items -->
		
		<!-- Left Sidebar Menu -->
		<?php $this->load->view($default_element.'left_menu'); ?>
		<!-- /Left Sidebar Menu -->
		
		<!-- Right Sidebar Menu -->
		<?php $this->load->view($default_element.'right_menu'); ?>
		<!-- /Right Sidebar Menu -->
		
		
		
		<!-- Right Sidebar Backdrop -->
		<div class="right-sidebar-backdrop"></div>
		<!-- /Right Sidebar Backdrop -->

		<!-- Main Content -->
		<div class="page-wrapper">
			<div class="container-fluid">

				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h4 class="txt-dark"> </h4>
					</div>
					<!-- Breadcrumb -->
					<?php $this->load->view($default_element."breadcrumb");?>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->

				<?php
				if(!empty($page)):
					if(is_array($page)):
						foreach ($page as $item) {
							if(file_exists(VIEWPATH."$item.php")){
								$this->load->view("{$item}");
							}else{
								echo "File: $item Not Found";
							}
						}
					elseif(isset($page)):
						if(file_exists(VIEWPATH."$page.php")){
							$this->load->view("{$page}");
						}else{
							$this->load->view($default_element."not_found");
						}
					else:
						$this->load->view($default_element."not_found");
					endif;
				endif;
				?>

				<!-- Footer -->
				<footer class="footer container-fluid pl-30 pr-30">
					<div class="row">
						<div class="col-sm-12">
							<p>2019 &copy; Intellect Laboratory. Mahasarakham University</p>
						</div>
					</div>
				</footer>
				<!-- /Footer -->
			</div>
		</div>
		<!-- /Main Content -->

	</div>
	<!-- /#wrapper -->
	
	<?php $this->load->view($default_element."foot_loader");?>

</body>

</html>
