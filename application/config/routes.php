<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'education';

$route['404_override'] = 'error/_404';
$route['translate_uri_dashes'] = FALSE;

// ============== Front main ===================
$route['news'] = 'education/event_and_news';
$route['news/(:num)'] = 'education/event_and_news/card_page/$1';

$route['events'] = 'education/event_and_news/events_list';
$route['events/(:num)'] = 'education/event_and_news/events_card_page/$1';

$route['livestream'] = 'education/live_stream';
$route['livestream/(:num)'] = 'education/live_stream/view/$1';

$route['media'] = 'education/media';
$route['media/(:num)'] = 'education/media/card_page/$1';

$route['find/school'] = 'education/school_map';

// -------------- Parent -----------------------
$route['parent/login'] = 'auth/parent_login';
$route['parent/logout'] = 'auth/logout/1';
// ------------ END Parent ---------------------

// ============ End Front main =================


// ============== Admininstrator ===================
$route['admin/home'] = 'admin/admin_home';

// -- API ---

// ---- School ----
$route['admin/school'] = 'admin/school';
// ---- End School ----

// ---- Child Type ----
$route['admin/child_type'] = 'admin/child_type';
$route['admin/child_type/list'] = 'admin/child_type/child_type_list';
//  ---- End Child Type ----

// ---- School Type ----
$route['admin/school_type'] = 'admin/school_type';
// ---- End School Type ----

// ---- Users ----
$route['admin/users'] = 'admin/admin_users';
$route['admin/users/(:any)'] = 'admin/admin_users';
$route['admin/users/(:any)/(:any)'] = 'admin/admin_users';
$route['admin/users/(:any)/(:any)/(:any)'] = 'admin/admin_users';

$route['admin/parent'] = 'admin/admin_users/list_parent';
$route['admin/parent/(:any)'] = 'admin/admin_users/list_parent';
$route['admin/parent/(:any)/(:any)'] = 'admin/admin_users/list_parent';
$route['admin/parent/(:any)/(:any)/(:any)'] = 'admin/admin_users/list_parent';
// ---- End Users ----

// ---- Groups ----
$route['admin/groups'] = 'admin/admin_groups';
$route['admin/groups/(:any)'] = 'admin/admin_groups';
$route['admin/groups/(:any)/(:any)'] = 'admin/admin_groups';
// ---- End Groups ----

// ---- Patronize ----
$route['admin/patronize'] = 'admin/admin_patronize';
$route['admin/patronize/index'] = 'admin/admin_patronize';
$route['admin/patronize/index/(:any)'] = 'admin/admin_patronize';
$route['admin/patronize/index/(:any)/(:any)'] = 'admin/admin_patronize';

// ---- End Patronize ----

// ---- Patronize Type ----
$route['admin/patronize_type'] = 'admin/admin_patronize_type';
$route['admin/patronize_type/index'] = 'admin/admin_patronize_type';
$route['admin/patronize_type/index/(:any)'] = 'admin/admin_patronize_type';
$route['admin/patronize_type/index/(:any)/(:any)'] = 'admin/admin_patronize_type';
// ---- End Patronize Type ----

// ---- Information ----
$route['admin/information'] = 'admin/admin_information/list_info';
$route['admin/information/(:any)'] = 'admin/admin_information/list_info';
$route['admin/information/(:any)/(:any)'] = 'admin/admin_information/list_info';
// ---- End Information ----
// ---- Information ----
$route['admin/event'] = 'admin/admin_information/list_event';
$route['admin/event/(:any)'] = 'admin/admin_information/list_event';
$route['admin/event/(:any)/(:any)'] = 'admin/admin_information/list_event';
// ---- End Information ----

// ---- Child ----
$route['admin/child'] = 'admin/admin_child';
$route['admin/child/list'] = 'admin/admin_child/child_list';
$route['admin/child/view'] = 'admin/admin_child/child_view';
$route['admin/child/insert'] = 'admin/admin_child/child_insert'; //view
$route['admin/child/edit/(:any)'] = 'admin/admin_child/child_edit/$1'; //view
$route['admin/child/api/insert'] = 'admin/admin_api/child_do_insert'; //view
$route['admin/child/api/edit'] = 'admin/admin_api/child_do_edit'; //view

$route['admin/child/test_noseries/(:any)'] = 'admin/admin_child/test_noseries/$1';
$route['admin/child/index'] = 'admin/admin_child';
$route['admin/child/index/(:any)'] = 'admin/admin_child';
$route['admin/child/index/(:any)/(:any)'] = 'admin/admin_child';
// ---- End Child ----

// ---- VDO ----
$route['admin/vdo'] = 'admin/admin_vdo';
$route['admin/vdo/index'] = 'admin/admin_vdo';
$route['admin/vdo/index/(:any)'] = 'admin/admin_vdo';
$route['admin/vdo/index/(:any)/(:any)'] = 'admin/admin_vdo';
$route['admin/vdo/index/delete_file/(:any)/(:any)'] = 'admin/admin_vdo';
// ---- End VDO ----

// ---- live ----
$route['admin/live'] = 'admin/live';
$route['admin/live/update'] = 'admin/live/update';
// ---- End live ----

// ---- Prefix name ----
$route['admin/prefix'] = 'admin/admin_prefix_name';
$route['admin/prefix/list'] = 'admin/admin_prefix_name/prefix_list';
$route['admin/prefix/view'] = 'admin/admin_prefix_name/prefix_view';
$route['admin/prefix/insert'] = 'admin/admin_prefix_name/prefix_insert'; //view
$route['admin/prefix/edit/(:any)'] = 'admin/admin_prefix_name/prefix_edit/$1'; //view
$route['admin/prefix/api/edit'] = 'admin/admin_api/child_do_edit'; //view

$route['admin/prefix/(:any)'] = 'admin/admin_prefix_name';
$route['admin/prefix/(:any)/(:any)'] = 'admin/admin_prefix_name';
$route['admin/prefix/(:any)/(:any)/(:any)'] = 'admin/admin_prefix_name';
// ---- End Prefix name ----

// ---- Province ----
$route['admin/province'] = 'admin/admin_province';
$route['admin/province/list'] = 'admin/admin_province/province_list';
$route['admin/province/view'] = 'admin/admin_province/province_view';
$route['admin/province/insert'] = 'admin/admin_province/province_insert'; //view
$route['admin/province/edit/(:any)'] = 'admin/admin_province/province_edit/$1'; //view

$route['admin/province/(:any)'] = 'admin/admin_province';
$route['admin/province/(:any)/(:any)'] = 'admin/admin_province';
$route['admin/province/(:any)/(:any)/(:any)'] = 'admin/admin_province';
// ---- End Province ----

// ---- Amphures ----
$route['admin/amphures'] = 'admin/admin_amphures';
$route['admin/amphures/list'] = 'admin/admin_amphures/amphures_list';
$route['admin/amphures/view'] = 'admin/admin_amphures/amphures_view';
$route['admin/amphures/insert'] = 'admin/admin_amphures/amphures_insert'; //view
$route['admin/amphures/edit/(:any)'] = 'admin/admin_amphures/amphures_edit/$1'; //view

$route['admin/amphures/(:any)'] = 'admin/admin_amphures';
$route['admin/amphures/(:any)/(:any)'] = 'admin/admin_amphures';
$route['admin/amphures/(:any)/(:any)/(:any)'] = 'admin/admin_amphures';
// ---- End Amphures ----

// ---- Districts ----
$route['admin/districts'] = 'admin/admin_districts';
$route['admin/districts/list'] = 'admin/admin_districts/districts_list';
$route['admin/districts/view'] = 'admin/admin_districts/districts_view';

$route['admin/districts/(:any)'] = 'admin/admin_districts';
$route['admin/districts/(:any)/(:any)'] = 'admin/admin_districts';
$route['admin/districts/(:any)/(:any)/(:any)'] = 'admin/admin_districts';
// ---- End Districts ----


// ---- Setting ----
$route['admin/setting-menu'] = 'admin/admin_web_setting/setting_menu';

$route['admin/setting-slide'] = 'admin/admin_web_setting/setting_slide';
$route['admin/setting-slide/list'] = 'admin/admin_web_setting/slide_list';
$route['admin/setting-slide/insert'] = 'admin/admin_web_setting/slide_insert';
$route['admin/setting-slide/edit/(:num)'] = 'admin/admin_web_setting/slide_edit/$1';
$route['admin/setting-slide/delete/(:num)'] = 'admin/admin_web_setting/slide_delete/$1';
// ---- End Setting ----

// ============= END Administrator =================