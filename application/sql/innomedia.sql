-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 27, 2020 at 03:21 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `innomedia`
--

-- --------------------------------------------------------

--
-- Table structure for table `inno_child_type`
--

CREATE TABLE `inno_child_type` (
  `ct_code` varchar(20) NOT NULL,
  `ct_description` varchar(100) NOT NULL,
  `ct_last_modify` datetime NOT NULL,
  `ct_last_modify_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inno_child_type`
--

INSERT INTO `inno_child_type` (`ct_code`, `ct_description`, `ct_last_modify`, `ct_last_modify_by`) VALUES
('CT001', 'บุคคลที่มีความบกพร่องทางการเห็น', '2020-01-16 00:00:00', 1),
('CT002', 'การได้ยิน', '0000-00-00 00:00:00', 1),
('CT003', 'ทางสติปัญญา', '0000-00-00 00:00:00', 1),
('CT004', 'ทางร่างกาย หรือการเคลื่อนไหว หรือสุขภาพ', '0000-00-00 00:00:00', 1),
('CT005', 'ทางการเรียนรู้', '0000-00-00 00:00:00', 1),
('CT006', 'ทางการพูด และภาษา', '0000-00-00 00:00:00', 1),
('CT007', 'ทางพฤติกรรม หรืออารมณ์', '0000-00-00 00:00:00', 1),
('CT008', 'ออทิสติก', '0000-00-00 00:00:00', 1),
('CT009', 'พิการซ้อน', '2020-01-19 18:31:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `inno_groups`
--

CREATE TABLE `inno_groups` (
  `grp_id` mediumint(8) UNSIGNED NOT NULL,
  `grp_name` varchar(20) NOT NULL,
  `grp_description` varchar(100) NOT NULL,
  `grp_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inno_groups`
--

INSERT INTO `inno_groups` (`grp_id`, `grp_name`, `grp_description`, `grp_status`) VALUES
(0, 'admin', 'Administrator', 1),
(1, 'teacher', 'Teacher User', 1),
(2, 'staff', 'Staff user', 1),
(100, 'agent', 'Small Agent', 1);

-- --------------------------------------------------------

--
-- Table structure for table `inno_login_attempts`
--

CREATE TABLE `inno_login_attempts` (
  `att_id` int(11) UNSIGNED NOT NULL,
  `att_ip_address` varchar(15) NOT NULL,
  `att_login` varchar(100) NOT NULL,
  `att_time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inno_menu`
--

CREATE TABLE `inno_menu` (
  `men_id` int(11) NOT NULL,
  `men_name` varchar(64) NOT NULL,
  `men_path` varchar(256) NOT NULL DEFAULT '/',
  `men_group_id` int(11) DEFAULT '1',
  `men_default_read` tinyint(1) NOT NULL DEFAULT '1',
  `men_default_write` tinyint(1) NOT NULL DEFAULT '1',
  `men_optional` text,
  `men_last_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inno_menu`
--

INSERT INTO `inno_menu` (`men_id`, `men_name`, `men_path`, `men_group_id`, `men_default_read`, `men_default_write`, `men_optional`, `men_last_modified`) VALUES
(1, 'dashboard', 'admin/home', 1, 1, 1, NULL, NULL),
(2, 'menu1', 'admin/home', 1, 1, 1, NULL, NULL),
(3, 'ประเภทเด็ก', 'admin/child_type', 1, 1, 1, NULL, NULL),
(4, 'ประเภทโรงเรียน', 'admin/school_type', 2, 1, 1, NULL, NULL),
(5, 'โรงเรียน', 'admin/school', 0, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inno_school`
--

CREATE TABLE `inno_school` (
  `sch_code` varchar(20) NOT NULL,
  `sch_name` text NOT NULL,
  `sch_address` text NOT NULL,
  `sch_googlemap` text,
  `sch_image` text,
  `sch_school_type` varchar(20) NOT NULL,
  `sch_last_modify` datetime NOT NULL,
  `sch_last_modify_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inno_school`
--

INSERT INTO `inno_school` (`sch_code`, `sch_name`, `sch_address`, `sch_googlemap`, `sch_image`, `sch_school_type`, `sch_last_modify`, `sch_last_modify_by`) VALUES
('S001', 'โรงเรียนคนตาบอด', 'ทดสอบที่อยู่', '11.99239,2.20029', 'image/none.png', 'ST001', '2020-01-20 16:18:58', 1);

-- --------------------------------------------------------

--
-- Table structure for table `inno_school_type`
--

CREATE TABLE `inno_school_type` (
  `st_code` varchar(20) NOT NULL,
  `st_description` varchar(100) NOT NULL,
  `st_last_modify` datetime NOT NULL,
  `st_last_modify_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inno_school_type`
--

INSERT INTO `inno_school_type` (`st_code`, `st_description`, `st_last_modify`, `st_last_modify_by`) VALUES
('ST001', 'โรงเรียนคนตาบอด', '2020-01-20 16:08:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `inno_users`
--

CREATE TABLE `inno_users` (
  `use_id` int(11) UNSIGNED NOT NULL,
  `use_ip` varchar(45) NOT NULL,
  `use_username` varchar(100) DEFAULT NULL,
  `use_password` varchar(255) NOT NULL,
  `use_salt` varchar(255) DEFAULT NULL,
  `use_email` varchar(100) NOT NULL,
  `use_activation_code` varchar(40) DEFAULT NULL,
  `use_forgotten_password_code` varchar(40) DEFAULT NULL,
  `use_forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `use_remember_me` varchar(40) DEFAULT NULL,
  `use_created` int(11) UNSIGNED NOT NULL,
  `use_last_login` int(11) UNSIGNED DEFAULT NULL,
  `use_active` tinyint(1) UNSIGNED DEFAULT NULL,
  `use_pre_name` int(11) DEFAULT '0',
  `use_first_name` varchar(50) DEFAULT NULL,
  `use_last_name` varchar(50) DEFAULT NULL,
  `use_company` varchar(100) DEFAULT NULL,
  `use_permission` text,
  `use_phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inno_users`
--

INSERT INTO `inno_users` (`use_id`, `use_ip`, `use_username`, `use_password`, `use_salt`, `use_email`, `use_activation_code`, `use_forgotten_password_code`, `use_forgotten_password_time`, `use_remember_me`, `use_created`, `use_last_login`, `use_active`, `use_pre_name`, `use_first_name`, `use_last_name`, `use_company`, `use_permission`, `use_phone`) VALUES
(0, '127.0.0.1', 'administrator', '$2y$08$lrcZOBikh8lqXEkF8dUp6OsSNggFgNTTeIJWzcmoTZWRS5W8FB28m', '', 'natthakit.sri@msu.ac.th', '', NULL, NULL, NULL, 1268889823, 1579880040, 1, 0, 'Admin', 'istrator', 'ADMIN', '{\"dashboard\":{\"read\":true,\"write\":true},\"menu1\":{\"read\":true,\"write\":true},\"menu2\":{\"read\":true,\"write\":true},\"menu3\":{\"read\":true,\"write\":true},\"admin1\":{\"read\":true,\"write\":false}}', '0'),
(5, '::1', 'armong', '$2y$08$cfq3QHl/YZEFYBLoo3TU1ea0P71vOie2NRVDP8D.StpddaguiyIOi', NULL, 'natthakit.sri2@msu.ac.th', '3f27334567edbf25f0cd2002691eaa5c2df50e30', NULL, NULL, NULL, 1575555724, NULL, 0, 0, 'Natthakit', 'Srikanjanapert', 'msu2gether-IntLab', '{\"dashboard\":{\"read\":true,\"write\":true},\"menu1\":{\"read\":true,\"write\":true},\"menu2\":{\"read\":true,\"write\":true},\"menu3\":{\"read\":true,\"write\":true},\"admin1\":{\"read\":false,\"write\":false}}', '+66956295946'),
(6, '::1', 'armong2', '$2y$08$gtPeC0hZt8IqIDbZYyGOFOVeYqs/VMymCfIhgL7/XV0lNfI.Elhuu', NULL, 'natthakit.sri3@msu.ac.th', '59901d2dbb78dbd81f6407cd9f1d4c6478a95b61', NULL, NULL, NULL, 1575555785, NULL, 0, 0, 'Natthakit', 'Srikanjanapert', 'msu2gether-IntLab', NULL, '+66956295946'),
(7, '::1', 'armong3', '$2y$08$u/sFjH/NY0GMHhsnXDQ7aOYAAcDpGssjR1pvagz3UCw7cR.B68dKq', NULL, 'natthakit.sri4@msu.ac.th', NULL, NULL, NULL, NULL, 1575555852, NULL, 1, 0, 'Natthakit', 'Srikanjanapert', 'msu2gether-IntLab', NULL, '+66956295946'),
(8, '::1', 'armong4', '$2y$08$zxue3CKG1qeRav5BG/OOTur.EaoNv1g1rxlt2VRN.u2T2YJFTlnvu', NULL, 'natthakit.sri5@msu.ac.th', NULL, NULL, NULL, NULL, 1575555867, NULL, 1, 0, 'Natthakit', 'Srikanjanapert', 'msu2gether-IntLab', NULL, '+66956295946');

-- --------------------------------------------------------

--
-- Table structure for table `inno_users_groups`
--

CREATE TABLE `inno_users_groups` (
  `ugr_id` int(11) UNSIGNED NOT NULL,
  `use_id` int(11) UNSIGNED NOT NULL,
  `grp_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inno_users_groups`
--

INSERT INTO `inno_users_groups` (`ugr_id`, `use_id`, `grp_id`) VALUES
(0, 0, 0),
(1, 5, 2),
(2, 6, 2),
(7, 7, 1),
(8, 8, 2),
(9, 8, 100);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `inno_child_type`
--
ALTER TABLE `inno_child_type`
  ADD PRIMARY KEY (`ct_code`);

--
-- Indexes for table `inno_groups`
--
ALTER TABLE `inno_groups`
  ADD PRIMARY KEY (`grp_id`);

--
-- Indexes for table `inno_login_attempts`
--
ALTER TABLE `inno_login_attempts`
  ADD PRIMARY KEY (`att_id`);

--
-- Indexes for table `inno_menu`
--
ALTER TABLE `inno_menu`
  ADD PRIMARY KEY (`men_id`);

--
-- Indexes for table `inno_school`
--
ALTER TABLE `inno_school`
  ADD PRIMARY KEY (`sch_code`);

--
-- Indexes for table `inno_school_type`
--
ALTER TABLE `inno_school_type`
  ADD PRIMARY KEY (`st_code`);

--
-- Indexes for table `inno_users`
--
ALTER TABLE `inno_users`
  ADD PRIMARY KEY (`use_id`),
  ADD UNIQUE KEY `use_email` (`use_email`),
  ADD UNIQUE KEY `use_username` (`use_username`);

--
-- Indexes for table `inno_users_groups`
--
ALTER TABLE `inno_users_groups`
  ADD PRIMARY KEY (`ugr_id`),
  ADD UNIQUE KEY `uc_users_groups` (`use_id`,`grp_id`),
  ADD KEY `fk_users_groups_users1_idx` (`use_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`grp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `inno_groups`
--
ALTER TABLE `inno_groups`
  MODIFY `grp_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `inno_login_attempts`
--
ALTER TABLE `inno_login_attempts`
  MODIFY `att_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inno_menu`
--
ALTER TABLE `inno_menu`
  MODIFY `men_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `inno_users`
--
ALTER TABLE `inno_users`
  MODIFY `use_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `inno_users_groups`
--
ALTER TABLE `inno_users_groups`
  MODIFY `ugr_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `inno_users_groups`
--
ALTER TABLE `inno_users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`grp_id`) REFERENCES `inno_groups` (`grp_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`use_id`) REFERENCES `inno_users` (`use_id`) ON DELETE CASCADE ON UPDATE NO ACTION;
