<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prefix_name_model extends CI_Model {

    private $_table = 'prefix';

    public function __construct()
    {
        parent::__construct();
    }

    public function _get_all(){
		$this->db->select('*');
		$this->db->from($this->_table);
		$this->db->where('pre_is_active','A');
		$this->db->order_by('pre_id','asc');

		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		}else {
			return false;
		}
	}

	public function _get_specific($col,$pre_id){
		$this->db->select('*');
		$this->db->from($this->_table);
		$this->db->where($col,$pre_id);

		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->row();
		}else {
			return false;
		}
	}
/*
	public function _get_specific($pre_prefix,$pre_is_active){
		$this->db->insert into ;
		$this->db->($this->_table);
		$this->db->set($pre_prefix,$pre_is_active);

		$query = $this->db->getr;

		if ($query->num_rows() >= 1) {
			return $query->row();
		}else {
			return false;
		}
	}*/

}