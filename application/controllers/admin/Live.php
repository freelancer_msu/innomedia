<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Live extends Core_Acontroller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('admin/live_model');
  }

  public function index()
  {

    $request = $this->ion_auth->get_user_id(); // get user id
    $respons = $this->live_model->_get($request);
    $data['data'] = array(
      'name' => !empty($respons) ? $respons[0]->live_name : '',
      'detail' => !empty($respons) ? $respons[0]->live_description : '',
      'embed' => !empty($respons) ? $respons[0]->live_embed : '',
      'status' => !empty($respons) ? (($respons[0]->live_status == '1') ? 'active' : 'inactive') : ''
    );

    $data['main_menu'] = $this->load_main_menu();
    $this->breadcrumb('live', base_url('admin/home'));
    $this->response('live_view', $data);
  }

  public function update()
  {
    $status = ($this->input->post('status') == 'active') ? 1 : 0;

    $user_id = $this->ion_auth->get_user_id(); // get user id
    $data = array(
      'live_name' => $this->input->post('name'),
      'live_description' => $this->input->post('detail'),
      'live_embed' => $this->input->post('embed'),
      'live_status' => $status,
      'live_use_id' => $user_id,
      'live_updated' => date("Y-m-d H:i:s")
    );

    if ($this->live_model->_get($user_id)) {
      if ($this->live_model->_update($user_id, $data)) {
        $respons = array(
          'message' => 'Successfully'
        );
      } else {
        $respons = array(
          'message' => 'Failed!'
        );
      }
    } else {
      if ($this->live_model->_insert($data)) {
        $respons = array(
          'message' => 'Successfully'
        );
      } else {
        $respons = array(
          'message' => 'Failed!'
        );
      }
    }

    $this->session->set_flashdata('live_respons', $respons['message']);
    redirect(base_url() . 'admin/live', 'refresh');
  }
}
        
    /* End of file  live.php */
