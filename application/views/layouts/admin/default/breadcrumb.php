<?php if(count($breadcrumb)>0 || @$breadcrumb_use): ?>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
	<ol class="breadcrumb">
		<!-- <li><a href="index.html">Dashboard</a></li>
		<li><a href="#"><span>speciality pages</span></a></li>
		<li class="active"><span>blank page</span></li> -->
		<?php foreach ($breadcrumb as $key => $bc):  ?>
			<?php if (count($breadcrumb)==($key+1)):  ?>
				<li class="active"><span><?=$bc->name?></span></li>
			<?php else: ?>
				<li><span>
					<a href="<?=base_url((!empty($bc->link) ? $bc->link : '#'))?>">
						<?=$bc->name?>
					</a>
				</span></li>
			<?php endif; ?>
		<?php endforeach; ?>
	</ol>
</div>
<?php endif; ?>
