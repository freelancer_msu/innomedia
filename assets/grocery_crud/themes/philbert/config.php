<?php 
$config['crud_paging'] = true;

//  dialog colors : from Philbert
$config['grocery_crud_dialog_color'] = 'default';

//  dialog text colors : from Philbert
$config['grocery_crud_dialog_text_color'] = 'dark';

// table resoponsibility
$config['table_responsive'] = true;