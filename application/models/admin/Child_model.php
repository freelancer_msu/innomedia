<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Child_model extends Core_Model {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function _get_all(){
		$this->db->select('*');
		$this->db->from('child');
		$this->db->join('school','chi_school = sch_code','left');
		// $this->db->where('chi_is_active',1);
		$this->db->order_by('chi_id','asc');

		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		}else {
			return false;
		}
	}

	public function _get_specific($col,$value){
		$this->db->select('*');
		$this->db->from('child');
		$this->db->join('school','chi_school = sch_code','left');
		$this->db->where($col,$value);

		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->row();
		}else {
			return false;
		}
	}

	public function _get_child_type($chi_id){
		$this->db->select('*');
		$this->db->from('child_in_type');
		$this->db->join('child_type','cit_type_id = ct_code','left');
		$this->db->where('cit_child_id',$chi_id);

		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		}else {
			return false;
		}
	}
}