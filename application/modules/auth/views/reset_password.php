<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>Philbert I Fast build Admin dashboard for any platform</title>
		<meta name="description" content="Philbert is a Dashboard & Admin Site Responsive Template by hencework." />
		<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Philbert Admin, Philbertadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
		<meta name="author" content="hencework"/>
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
		
		<link href="<?=base_url('vendors/jasny-bootstrap/dist/css/jasny-bootstrap.min.css')?>" rel="stylesheet" type="text/css"/>
	    
	    <!-- Custom CSS -->
	    <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet" type="text/css">
	</head>
	<body>
		<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->
		
		<div class="wrapper pa-0">
			
			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0 auth-page">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="sp-logo-wrap text-center pa-0 mb-30">
											<a href="index.html">
												<img class="brand-img mr-10" src="<?=base_url('assets/imgs/logo.png')?>" alt="brand"/>
												<span class="brand-text">Philbert</span>
											</a>
										</div>
										<div class="mb-30">
											<h3 class="text-center txt-dark mb-10"><?=lang('reset_password_heading')?></h3>
										</div>	
										<div class="form-wrap">
											<div id="infoMessage"><?php echo $message;?></div>
											<form action="<?=base_url('auth/reset_password/'.$code)?>" method="post">
												<div class="form-group">
													<label class="pull-left control-label mb-10" for="<?=$new_password['id']?>"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label>
													<?php 
														$new_password = array_merge($new_password, array(
															'class' => "form-control",
															'required' => TRUE,
															'placeholder' => '',
														));
														echo form_input($new_password);
													?>
												</div>
												<div class="form-group">
													<label class="pull-left control-label mb-10" for="<?=$new_password_confirm['id']?>"><?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?></label>
													<?php 
														$new_password_confirm = array_merge($new_password_confirm, array(
															'class' => "form-control",
															'required' => TRUE,
															'placeholder' => '',
														));
														echo form_input($new_password_confirm);
													?>
												</div>
												<?php echo form_input($user_id);?>
												<?php echo form_hidden($csrf); ?>
												<div class="form-group text-center">
													<button type="submit" class="btn btn-info btn-success btn-rounded"><?=lang('reset_password_submit_btn')?></button>
												</div>
											</form>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->	
				</div>
				
			</div>
			<!-- /Main Content -->
		
		</div>
		<!-- /#wrapper -->
		
		<!-- JavaScript -->
		
		<!-- jQuery -->
	    <script src="<?=base_url('vendors/jquery/dist/jquery.min.js')?>"></script>
	    
	    <!-- Bootstrap Core JavaScript -->
	    <script src="<?=base_url('vendors/bootstrap/dist/js/bootstrap.min.js')?>"></script>
	    <script src="<?=base_url('vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js')?>"></script>
	    
	    <!-- Slimscroll JavaScript -->
	    <script src="<?=base_url('assets/js/jquery.slimscroll.js')?>"></script>
	    
	    <!-- Init JavaScript -->
	    <script src="<?=base_url('assets/js/init.js')?>"></script>
	</body>
</html>
