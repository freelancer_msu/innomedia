<nav>
<div class="megamenu_container">
<a id="megamenu-button-mobile" href="#">เมนู</a><!-- Menu button responsive-->
    
	<!-- Begin Mega Menu Container -->
	<ul class="megamenu">
		<!-- Begin Mega Menu -->
		<li>
			<a href="<?=base_url()?>" class="nodrop-down">หน้าหลัก</a>
		</li><!-- End Item -->
        
		<li>
			<a href="<?=base_url('events')?>" class="nodrop-down">ข่าวสาร &amp; กิจกรรม</a>
		</li><!-- End Item -->

		<li>
			<a href="<?=base_url('news')?>" class="nodrop-down">ความรู้เกี่ยวกับเด็กพิเศษ</a>
		</li><!-- End Item -->
        
		<li>
			<a href="<?=base_url('media')?>" class="nodrop-down">สื่อการเรียนรู้</a>
		</li><!-- End Item -->
        
		<li>
			<a href="<?=base_url('find/school')?>" class="nodrop-down">ค้นหาโรงเรียน</a>
		</li><!-- End Item -->
        <?php if($is_logged_in) { ?>
		<li class="drop-normal"><a href="javascript:void(0)" class="drop-down">ข้อมูลส่วนตัว</a>
        <div class="drop-down-container normal">
                   <ul>
                       <li><a href="#">แก้ไขข้อมูลส่วนตัว</a></li>
                       <li><a href="#">เด็กพิเศษในความดูแล</a></li>
                       <li><a href="<?=base_url('parent/logout')?>">ออกจากระบบ</a></li>
                    </ul>
         </div><!-- End dropdown normal -->
        </li>
        <?php } else {?>
        <li>
        	<a href="javascript:void(0);" class="drop-down"><?=(!$is_logged_in) ? 'เข้าสู่ระบบ' : 'ข้อมูลผู้ใช้';?></a>
        	<!-- Begin Item -->
			<div class="drop-down-container">
	        
				<div class="row">
	            
					<!-- <div class="col-md-5">
					</div> -->
	                <?php if(!$is_logged_in) { ?>
					<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
						<h4>เข้าสู่ระบบ</h4>
						<hr>
						<div class="row" style="margin-bottom: 45px;">
	                    	<form class="" method="post" action="<?=base_url('parent/login')?>">
	                    		<div class="col-md-12">
	                    			<h5>Username : </h5>
	                    			<input type="text" name="p_uname" class="form-control">
	                    		</div>
	                    		<div class="col-md-12">
	                    			<h5>Password : </h5>
	                    			<input type="password" name="p_passw" class="form-control">
	                    		</div>
	                    		<div class="col-md-4">
	                    			<button class="btn btn-lg btn-warning text-light w-100">ลืมรหัสผ่าน</button>
	                    		</div>
	                    		<div class="col-md-8">
	                    			<button class="btn btn-lg btn-success text-light w-100">เข้าสู่ระบบ!</button>
	                    		</div>
	                    	</form>
	                        
						</div><!-- End row -->
					</div><!-- End Span6 -->
					<?php } ?>
				</div><!-- End row-->
			</div><!-- End Item Container -->
			<!-- End Item -->
		</li><!-- End Item -->
		<?php } ?>
	</ul><!-- End Mega Menu -->
</div>
</nav><!-- /navbar -->