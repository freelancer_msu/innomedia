<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event_and_news extends Core_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('news_model');
        $this->load->model('events_model');
    }
	public function index()
	{
		$data['is_logged_in'] = $this->is_logged_in;
		$data['type_list'] = $this->news_model->_get_all_child_type();
		$data['list_data'] = $this->news_model->_get_all();
		$this->response('news_list',$data,'info_page');
	}

	public function card_page($content_id = 1){
		$data['is_logged_in'] = $this->is_logged_in;
		$data['result_data'] = $this->news_model->_get_specified($content_id);
		$this->response('news_card',$data,'info_page');
	}

	public function events_list()
	{
		$data['is_logged_in'] = $this->is_logged_in;
		$data['list_data'] = $this->events_model->_get_all();
		$this->response('events_list',$data,'info_page');
	}

	public function events_card_page($content_id = 1){
		$data['is_logged_in'] = $this->is_logged_in;
		$data['result_data'] = $this->events_model->_get_specified($content_id);
		$this->response('events_card',$data,'info_page');
	}
}
