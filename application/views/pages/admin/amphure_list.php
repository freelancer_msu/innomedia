<link href="<?=base_url('vendors/sweetalert2/dist/sweetalert2.min.css')?>" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<!-- Row -->
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h4 class="panel-title txt-dark"><?=$page_title?></h4>
				</div>
				<div class="pull-right">
				<a class="btn btn-xs btn-success" href="<?=base_url('admin/'.$page_module.'/insert')?>">เพิ่มข้อมูล <?=$page_title?></a></div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="table-wrap">
						<div class="table-responsive">
							<table id="foo-<?=$page_module?>-list" class="table table-hover display pb-30" >
								<thead>
									<tr>
										<th align="center" width="10%">#</th>
										<th width="10%">รหัสอ้างอิง</th>
										<th align="center">ชื่อ<?=$page_title?> (ภาษาไทย)</th>
										<th align="center">ชื่อ<?=$page_title?> (ภาษาอังกฤษ)</th>
										<th align="center">สังกัด จังหวัด</th>
										<th align="center" width="10%">สถานะการใช้งาน</th>
										<th width="10%"></th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<!-- /Row -->

<!-- View Modal -->
<div class="modal fade" id="<?=$page_module?>Modal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title title-from-api" id="<?=$page_module?>ModalLargeModalLabel">loading</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="ปิด">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="<?=$page_module?>ModalLoadContent">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning">บันทึก</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
			</div>
		</div>
	</div>
</div>

<script src="<?=base_url('vendors/sweetalert2/dist/sweetalert2.min.js')?>"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){

		$('#<?=$page_module?>Modal').on('show.bs.modal', function(e) {
			var lookup_id = $(e.relatedTarget).data('lookupid');
			$('#<?=$page_module?>ModalLoadContent').html('loading please wait ..');

			$.post('<?=base_url("admin/".$page_module."/view")?>',{_id:lookup_id},function(res,err){
				var response = $.parseJSON(res);
				if(response.status == 1){
					$('#<?=$page_module?>ModalLargeModalLabel').html(response.title);
					$('#<?=$page_module?>ModalLoadContent').html(response.message);
				}
			});
		});

		$('#foo-<?=$page_module?>-list').DataTable( {
			responsive: {
				details: {
					renderer: function ( api, rowIdx, columns ) {
						var data = $.map( columns, function ( col, i ) {
							return col.hidden ?
							'<tr data-dt-row="'+col.rowIndex+'" data-dt-column="'+col.columnIndex+'">'+
							'<td>'+col.title+':'+'</td> '+
							'<td>'+col.data+'</td>'+
							'</tr>' :
							'';
						} ).join('');

						return data ?
						$('<table/>').append( data ) :
						false;
					}
				}
			},
			autoWidth: false,
			processing: true,
			ajax: '<?=base_url($source_url)?>',
			columns: [
				{ data: 'id' },
				{ data: 'code' },
				{ data: 'name_th' },
				{ data: 'name_en' },
				{ data: 'init' },
				{ data: 'status' },
				{ data: null },
			],
			columnDefs: [
				{
					"render": function ( data, type, row ) { 
						var view_btn = '<a class="btn btn-xs btn-primary admin-action-btn" data-toggle="modal" data-lookupid="'+row[0]['pro_id']+'" data-target="#<?=$page_module?>Modal"><i class="fa fa-eye" style="font-size: 18px;"></i></a>';
						var edit_btn = '<a href="<?=base_url('admin/amphures/edit')?>/'+row[0]['pro_id']+'" class="btn btn-xs btn-warning"><i class="fa fa-edit" style="font-size: 18px;"></i></a>';
						topic = view_btn+' '+edit_btn;
						return topic;
					},
					targets:[6],
					orderable: false
				},
				{
					"render": function ( data, type, row ) { 
						if(row['status'] == 'A'){
							topic = '<a href="javascript:<?=$page_module?>_active_toggle(0);"><label class="label label-success">ใช้งาน</label></a>';
						}else{
							topic = '<a href="javascript:<?=$page_module?>_active_toggle(1);" ><label class="label label-danger">ไม่ใช้งาน</label></a>';
						}
						return topic;
					},
					targets:[5],
					orderable: true
				}
			],
			initComplete: function( settings, json ) {            

			},
			order: [[ 0, "asc" ]]

		});
	});

	function amphures_active_toggle(set_to) {
		var text_disp = set_to==1?'ใช้งาน':'ไม่ใช้งาน';
		Swal.fire({
			title: "กรุณายืนยัน",
			text: "คุณต้องการเปลี่ยนสถานะเป็น '"+text_disp+"' ใช่หรือไม่?",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'ใช่, เปลี่ยนเลย!',
			cancelButtonText: 'ไม่, ฉันไม่ต้องการ'
		}).then((result) => {
			if (result.value) {
				$.post('<?=base_url("")?>',{lookup_id:lookup_id},function(res,err){
					var response = $.parseJSON(res);
					var swal_title = 'สำเร็จ!';
					var swal_type = 'success';
					if(response.status != 1){
						swal_title = 'ล้มเหลว!';
						swal_type = 'error';
					}
					swal({
						title: swal_title, 
						text: response.msg, 
						type: swal_type
					});
				});
			}
		});
	}

</script>