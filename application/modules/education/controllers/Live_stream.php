<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Live_stream extends Core_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('livestream_model');
    }
	public function index()
	{
		$data['title'] = 'ถ่ายทดสอด | Innovation Media';
		$data['page_header'] = 'ถ่ายทอดสด';
		$data['data_lists'] = $this->livestream_model->_getAll();
		$data['is_logged_in'] = $this->is_logged_in;
		$this->response('livestream_list',$data,'info_page');
	}

	public function view($id)
	{
		$livestream = $this->livestream_model->_get($id)[0];

		$data['title'] = 'ถ่ายทดสอด | Innovation Media';
		$data['page_header'] = 'ถ่ายทอดสด : '.$livestream->live_name;
		$data['data_detail'] = $livestream;
		$data['is_logged_in'] = $this->is_logged_in;
		$this->response('livestream_card',$data,'info_page');
	}
}
