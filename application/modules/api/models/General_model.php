<?php

defined('BASEPATH') or exit('No direct script access allowed');

class General_model extends CI_Model
{

    public function _get_provice()
    {
        $this->db->select('pro_id,pro_code,pro_name_th,pro_name_en');
        $this->db->from('province');
        $this->db->where('pro_active', 'A');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function _get_amphures_by_province($provice_id = '')
    {
        $this->db->select('amphure_id,amphure_code,amphure_name_th,amphure_name_en');
        $this->db->from('amphures');
        $this->db->where('amphure_active', 'A');
        $this->db->where('amphure_province', $provice_id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function _get_districts_by_amphure($province_id = '', $amphure_id = '')
    {
        $this->db->select('district_id,district_code,district_name_th,district_name_en');
        $this->db->from('districts');
        $this->db->where('district_active', 'A');
        $this->db->where('district_province', $province_id);
        $this->db->where('district_amphures', $amphure_id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
}
