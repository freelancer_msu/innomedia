<?php

	$this->set_css('http://www.datatables.net/rss.xml');
    $this->set_css($this->default_theme_path.'/philbert/vendors/datatables/media/css/jquery.dataTables.min.css');
    
    <!-- Fancy Dropdown JS -->
    <script src="dist/js/dropdown-bootstrap-extended.js"></script>
    
    <!-- Init JavaScript -->
    <script src="dist/js/init.js"></script>

    //section libs
    $this->set_js_lib($this->default_theme_path.'/philbert/vendors/datatables/media/css/jquery.dataTables.min.css');

    $this->set_js_lib($this->default_theme_path.'/philbert/vendors/datatables.net-buttons/js/dataTables.buttons.min.js');
    $this->set_js_lib($this->default_theme_path.'/philbert/vendors/datatables.net-buttons/js/buttons.flash.min.js');

    if(!$unset_export) {
        $this->set_js_lib($this->default_theme_path.'/philbert/vendors/jszip/dist/jszip.min.js');
        $this->set_js_lib($this->default_theme_path.'/philbert/vendors/pdfmake/build/pdfmake.min.js');
        $this->set_js_lib($this->default_theme_path.'/philbert/vendors/pdfmake/build/vfs_fonts.js');
        $this->set_js_lib($this->default_theme_path.'/philbert/js/export-table-data.js');
    }
    $this->set_js_lib($this->default_theme_path.'/philbert/vendors/datatables.net-buttons/js/buttons.html5.min.js ');
    if(!$unset_print){
        $this->set_js_lib($this->default_theme_path.'/philbert/vendors/datatables.net-buttons/js/buttons.print.min.js');
    }

    $this->set_js_lib($this->default_theme_path.'/philbert/js/jquery.slimscroll.js');


    $this->set_js_lib($this->default_theme_path.'/philbert/vendors/owl.carousel/dist/owl.carousel.min.js');
    $this->set_js_lib($this->default_theme_path.'/philbert/vendors/switchery/dist/switchery.min.js');
    $this->set_js_lib($this->default_theme_path.'/philbert/vendors/sweetalert/dist/sweetalert.min.js');

    $this->set_js_lib($this->default_theme_path.'/philbert/js/dropdown-bootstrap-extended.js');
    
    $colspans = (count($columns) + 2);

    //Start counting the buttons that we have:
    $buttons_counter = 0;

    if (!$unset_edit) {
        $buttons_counter++;
    }

    if (!$unset_read) {
        $buttons_counter++;
    }

    if (!$unset_delete) {
        $buttons_counter++;
    }

    if (!empty($list[0]) && !empty($list[0]->action_urls)) {
        $buttons_counter = $buttons_counter +  count($list[0]->action_urls);
    }

    $list_displaying = str_replace(
        array(
            '{start}',
            '{end}',
            '{results}'
        ),
        array(
            '<span class="paging-starts">1</span>',
            '<span class="paging-ends">10</span>',
            '<span class="current-total-results">'. $this->get_total_results() . '</span>'
        ),
        $this->l('list_displaying'));

	if(config_item('grocery_crud_dialog_color'))
		$this->theme_config['grocery_crud_dialog_color'] = config_item('grocery_crud_dialog_color');

	if(config_item('grocery_crud_dialog_text_color'))
		$this->theme_config['grocery_crud_dialog_text_color'] = config_item('grocery_crud_dialog_text_color');

	if(config_item('table_responsive'))
		$this->theme_config['table_responsive'] = config_item('table_responsive');
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url();?>';

    var subject = '<?php echo $subject?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list_url = '<?php echo $ajax_list_url;?>';
    var unique_hash = '<?php echo $unique_hash; ?>';

    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";

</script>
<div class="success-message" style="display:none"><?php
if($success_message !== null){?>
   <?php echo $success_message; ?> &nbsp; &nbsp;
<?php }
?></div>

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                      <h5 class="txt-dark">Export</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                      <ol class="breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>
                        <li><a href="#"><span>table</span></a></li>
                        <li class="active"><span>Export</span></li>
                      </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->
                
                <!-- Row --> 
                <?php 
                    //echo $this->theme_config['grocery_crud_dialog_text_color'] 
                ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-<?=$this->theme_config['grocery_crud_dialog_color']?> card-view">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-dark"><?=$subject_plural?></h6>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table id="example" class="table table-hover display  pb-30" >
                                                <thead>

                    <tr>
                        <?php foreach($columns as $column){?>
                            <th class="column-with-ordering" data-order-by="<?php echo $column->field_name; ?>"><?php echo $column->display_as; ?></th>
                        <?php }?>
                        <th colspan="2" <?php if ($buttons_counter === 0) {?>class="invisible text-center"<?php }?>>
                            <?php echo $this->l('list_actions'); ?>
                        </th>
                    </tr>

                                                </thead>
                                                <!-- <tfoot>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Position</th>
                                                        <th>Office</th>
                                                        <th>Age</th>
                                                        <th>Start date</th>
                                                        <th>Salary</th>
                                                    </tr>
                                                </tfoot> -->
                                                <tbody>
                                                    <?php include(__DIR__."/list_tbody.php"); ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            </div>
            
            <!-- Footer -->
            <footer class="footer container-fluid pl-30 pr-30">
                <div class="row">
                    <div class="col-sm-12">
                        <p>2017 &copy; Philbert. Pampered by Hencework</p>
                    </div>
                </div>
            </footer>
            <!-- /Footer -->
            
        </div>
        <!-- /Main Content -->

<!-- Modal Structure -->
  <div id="add-edit-modal" class="modal modal-fixed-footer add-edit-modal">
      <div id="add-edit-content"></div
  </div>