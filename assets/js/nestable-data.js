/*Nestable Init*/
$( document ).ready(function() {
	"use strict";

	var data = '';
	var jsondata = '';

	$("#add").click(function(){
		data = $('#nestable').nestable('serialize');
		jsondata = JSON.stringify(data);
		$('.test-html').html(jsondata);
	});

	$('#nestable').nestable();
});