<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_dashboard_model extends Core_Model {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function _get_main_menu(){
		$answer = $this->z_menu_traveler(0);
		return $answer;
	}

	public function _get_permission(){
		$this->db->select('use_permission');
		$this->db->from('users');
		$this->db->where('use_username',$this->session->userdata('use_username'));
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			if ( $answer = $this->z_validate_permission($query->row()) ) {
				return $answer;
			}else{
				return false;
			}
		}else {
			return false;
		}
	}

	private function z_validate_permission($query_row){
		if($query_row->use_permission == '{}' || $query_row->use_permission == '[]'){
			$default_menus = $this->g_generate_default_permission();
			$answer = [];
			foreach ($default_menus as $default_menu) {
				$answer[$default_menu->men_id] = new stdClass;
				$answer[$default_menu->men_id]->read = $default_menu->men_default_read;
				$answer[$default_menu->men_id]->write = $default_menu->men_default_write;
			}
			$where['use_username'] = $this->session->userdata('use_username');
			$set['use_permission'] = json_encode($answer);
			if($this->_update_where('users',$where,$set)){
				return $answer;
			}else{
				return false;
			}
			
		}else{
			return json_decode($query_row->use_permission);
		}
		return false;
	}

	public function g_generate_default_permission(){
		$this->db->select('men_id,men_name,men_path,men_default_read,men_default_write');
		$this->db->from('menu');
		$this->db->where('men_is_serperater','<>1');
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		}else {
			return false;
		}
	}

	private function z_menu_traveler($start_level = 0) {
		if ($this->z_is_has_child($start_level) < 1) {
			return false;
		}else{
			$menu_current_level = $this->z_get_menu_by_level($start_level);
			$answer = [];
			foreach ($menu_current_level as $f_value) {
				//do recursive
				$f_value->child = $this->z_menu_traveler($f_value->men_id);
				array_push($answer,$f_value);
			}
			return $answer;
		}
	}

	private function z_get_menu_by_level($level = 0) {
		$this->db->select('*');
		$this->db->from('menu');
		$this->db->where('men_parent',$level);
		$this->db->order_by('men_order','asc');

		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		}else {
			return false;
		}
	}

	private function z_is_has_child($my_level = 0) {
		$this->db->select('*');
		$this->db->from('menu');
		$this->db->where('men_parent',$my_level);

		$query = $this->db->get();

		return $query->num_rows();
	}
}