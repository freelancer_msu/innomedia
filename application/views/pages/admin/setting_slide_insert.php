<link rel="stylesheet" href="<?= base_url('vendors/dropify/dist/css/dropify.min.css') ?>">
<!-- Row -->
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="pull-left">
					<h4 class="panel-title txt-dark"><?= $page_title ?></h4>
				</div>
				<div class="pull-right"></div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<?php
					$msg_error = $this->session->flashdata('error');
					$msg_success = $this->session->flashdata('success');
					
					if(!empty($msg_error)){
						?>
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <?=$msg_error?>
						</div>
						<?php
					}
					if(!empty($msg_success)){
						?>
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <?=$msg_success?>
						</div>
						<?php
					}
					?>
					
					<form id="insertform" action="<?=base_url('admin/setting-slide/insert')?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<input type="file" id="input-file" name="input-file" data-allowed-file-extensions="jpg jpeg png gif" data-show-remove="false" class="dropify" data-default-file="<?=base_url('assets/uploads/slide/1950x500.png')?>" />
						</div>
						<div class="form-group">
							<label class="control-label mb-10 text-left">คำอธิบาย</label>
							<input type="text" id="caption" name="caption" class="form-control" placeholder="คำอธิบายสไลด์" value="">
						</div>
						<div class="form-group">
							<label class="control-label mb-10 text-left">เอฟเฟคการเปลี่ยนสไลด์</label>
							<input type="text" id="transaction" name="transaction" class="form-control" placeholder="slideleft" value="">
						</div>
						<div class="form-group mb-0">
							<button type="submit" form="insertform" class="btn btn-success"><span class="btn-text">เพิ่ม</span></button>
							<button type="button" class="btn btn-default" onclick="window.location.href='<?=base_url('admin/setting-slide')?>'">ย้อนกลับ</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?= base_url('vendors/dropify/dist/js/dropify.min.js') ?>"></script>

<script>
	$(document).ready(function() {
		"use strict";

		$('.dropify').dropify({
			messages: {
				'default': 'ลากและวางไฟล์ที่นี้หรือคลิก',
				'replace': 'ลากและวางไฟล์ที่นี้หรือคลิกเพื่อแก้ไข',
				'remove':  'ลบ',
				'error':   'เกิดข้อผิดพลาดในการอัพโหลดไฟล์'
			},
			error: {
				'imageFormat': 'สามารถอัพนามสกุลไฟล์ที่รองรับ ({{ value }} เท่านั้น).'
			}
		});
	});
</script>