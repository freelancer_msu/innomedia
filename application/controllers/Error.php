<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error extends Core_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function _404(){
		$this->load->view("errors/html/error_404");
	}
}