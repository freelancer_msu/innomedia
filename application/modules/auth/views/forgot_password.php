<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>Philbert I Fast build Admin dashboard for any platform</title>
		<meta name="description" content="Philbert is a Dashboard & Admin Site Responsive Template by hencework." />
		<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Philbert Admin, Philbertadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
		<meta name="author" content="hencework"/>
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
		
		<!-- vector map CSS -->
		<!-- vector map CSS -->
	    <link href="<?=base_url('vendors/jasny-bootstrap/dist/css/jasny-bootstrap.min.css')?>" rel="stylesheet" type="text/css"/>
	    
	    <!-- Custom CSS -->
	    <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet" type="text/css">
	</head>
	<body>
		<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->
		
		<div class="wrapper pa-0">
			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0 auth-page">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="sp-logo-wrap text-center pa-0 mb-30">
											<a href="index.html">
												<img class="brand-img mr-10" src="<?=base_url('assets/imgs/logo.png')?>" alt="brand"/>
												<span class="brand-text">Innivation Medias</span>
											</a>
										</div>
										<div class="mb-30">
											<h3 class="text-center txt-dark mb-10"><?=lang('forgot_password_heading')?></h3>
											<h6 class="text-center txt-grey nonecase-font"><?=sprintf(lang('forgot_password_subheading'), $identity_label)?></h6>
										</div>	
										<div class="form-wrap">
											<div id="infoMessage"><?php echo $message;?></div>
											<form action="<?=base_url('auth/forgot_password')?>" method="post"> 
												<div class="form-group">
													<label class="control-label mb-10" for="identity"><?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label>
													<?php 
														$identity = array_merge($identity, array(
															'class' => 'form-control',
															'required' => TRUE
														));
														echo form_input($identity);
													?>
												</div>
												
												<div class="form-group text-center">
													<button type="submit" class="btn btn-info btn-success btn-rounded"><?=lang('forgot_password_submit_btn')?></button>
												</div>
											</form>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->	
				</div>
				
			</div>
			<!-- /Main Content -->
		</div>
		<!-- /#wrapper -->
		
		<!-- JavaScript -->
		
		<!-- jQuery -->
	    <script src="<?=base_url('vendors/jquery/dist/jquery.min.js')?>"></script>
	    
	    <!-- Bootstrap Core JavaScript -->
	    <script src="<?=base_url('vendors/bootstrap/dist/js/bootstrap.min.js')?>"></script>
	    <script src="<?=base_url('vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js')?>"></script>
	    
	    <!-- Slimscroll JavaScript -->
	    <script src="<?=base_url('assets/js/jquery.slimscroll.js')?>"></script>
	    
	    <!-- Init JavaScript -->
	    <script src="<?=base_url('assets/js/init.js')?>"></script>
	</body>
</html>
