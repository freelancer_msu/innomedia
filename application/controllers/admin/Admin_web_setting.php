<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_web_setting extends Core_Acontroller
{
    private $_error_retreived = 'มีปัญหาเกี่ยวกับการดึงข้อมูล กรุณาลองใหม่อีกครั้งค่ะ';
    
    public function __construct()
    {
        parent::__construct();
        $this->breadcrumb('หน้าหลัก','admin/home');
        $this->load->model('admin/web_setting_model');
        $this->config->load('grocery_crud');
    }

    public function index()
    {
        $data['main_menu'] = $this->load_main_menu();
        $this->breadcrumb($this->title, base_url('admin/home'));
        $this->response('home', $data);
    }

    public function setting_menu()
    {
        $data = array(
            'title' => 'ตั้งค่าเมนูส่วนหลังบ้าน'
            ,'main_menu' => $this->load_main_menu()
            ,'page_data' => $this->load_main_menu()
        );

        $this->style(array(
            base_url('vendors/nestable2/jquery.nestable.css')
        ));

        $this->script(array(
            base_url('vendors/nestable2/jquery.nestable.js')
            ,base_url('assets/js/nestable-data.js')
        ));

        $this->breadcrumb($data['title'], base_url('admin/home'));
        $this->response('setting_menu_view', $data);
    }

    // Setting slide

    public function setting_slide(){
        $title_page = 'ตั้งค่าสไลด์หน้าเว็บหลัก';
        
        $data = array(
            'page_title' => $title_page
            ,'main_menu' => $this->load_main_menu()
            ,'source_url' => 'admin/setting-slide/list'
        );
        $this->breadcrumb($data['page_title'], '');
        $this->response('setting_slide_view', $data);
    }

    public function slide_list(){
        if ($slides = $this->web_setting_model->_getAllSlide())
        {
			$result['data'] = array();
			$idx = 1;
			foreach ($slides as $slide) {
				$row = array();
				$row['id'] = $slide->ws_id;
				$row['iRow'] = $idx++;
				$row['image_path'] = '<img src="'.base_url($slide->ws_image_path).'" alt="" width="150px">';
                $row['description'] = $slide->ws_description;
                $row['upload_by'] = $slide->use_first_name;
                $row['upload_date'] = $this->globals->dateformat_mini($slide->ws_upload_date,'thai');
				array_push($result['data'], $row);
			}
		}
		echo json_encode($result);
    }

    public function slide_insert(){
        $title_page = 'เพิ่มสไลด์หน้าเว็บหลัก';
        
        $data = array(
            'page_title' => $title_page
            ,'main_menu' => $this->load_main_menu()
        );

        if ($this->input->method(TRUE) == 'POST'){
            $data_insert = array(
                'ws_description' => $this->input->post('caption')
                ,'ws_transition' => $this->input->post('transaction')
                ,'ws_upload_date' => date('Y-m-d H:i:s')
            );

            $config['upload_path'] = './assets/uploads/slide/'; //path folder
            $config['allowed_types'] = 'gif|jpg|jpeg|png'; //Allowing types
            $config['encrypt_name'] = TRUE; //encrypt file name
            $this->load->library('upload',$config);
            if(!empty($_FILES['input-file']['name'])){
                if ($this->upload->do_upload('input-file')){
                    $filedata   = $this->upload->data();
                    $image  = $filedata['file_name']; //get file name
                    
                    $data_insert['ws_image_path'] = 'assets/uploads/slide/'.$image;
                    
                    if($this->web_setting_model->_insertSlide($data_insert)){
                        $this->session->set_flashdata('success','เพิ่มไลด์เรียบร้อยแล้ว');
                    }else{
                        unlink('assets/uploads/slide/'.$image);
                        $this->session->set_flashdata('error','ไม่สามารถอัพโหลดไฟล์ได้');
                    }
                }else{
                    $this->session->set_flashdata('error','ไม่สามารถอัพโหลดไฟล์ได้');
                }
            }else{
                $this->session->set_flashdata('error','กรุณาเลือกไฟล์ที่จะอัพโหลด');
            }
        }

        $this->breadcrumb('ตั้งค่าสไลด์หน้าเว็บหลัก','admin/setting-slide');
		$this->breadcrumb($title_page,'');
        $this->response('setting_slide_insert',$data);
    }
    
    public function slide_edit($id){
        $title_page = 'แก้ไขสไลด์หน้าเว็บหลัก';
        
        $data = array(
            'page_title' => $title_page
            ,'main_menu' => $this->load_main_menu()
            ,'slide_data' => $this->web_setting_model->_getSlide($id)
        );

        if ($this->input->method(TRUE) == 'POST'){
            $data_modify = array(
                        'ws_description' => $this->input->post('caption')
                        ,'ws_transition' => $this->input->post('transaction')
                        ,'ws_upload_date' => date('Y-m-d H:i:s')
                    );

            $config['upload_path'] = './assets/uploads/slide/'; //path folder
            $config['allowed_types'] = 'gif|jpg|jpeg|png'; //Allowing types
            $config['encrypt_name'] = TRUE; //encrypt file name
            $this->load->library('upload',$config);
            if(!empty($_FILES['input-file']['name'])){
                if ($this->upload->do_upload('input-file')){
                    $filedata   = $this->upload->data();
                    $image  = $filedata['file_name']; //get file name
                    
                    $data_modify['ws_image_path'] = 'assets/uploads/slide/'.$image;
                    
                    if($this->web_setting_model->_updateSlide($data['slide_data']->ws_id,$data_modify)){  
                        unlink($data['slide_data']->ws_image_path);  
                        $this->session->set_flashdata('success','อัพเดตสไลด์เรียบร้อยแล้ว');
                    }else{
                        unlink('assets/uploads/slide/'.$image);
                        $this->session->set_flashdata('error','ไม่สามารถอัพโหลดไฟล์ได้');
                    }
                }else{
                    $this->session->set_flashdata('error','ไม่สามารถอัพโหลดไฟล์ได้');
                }
                            
            }else{
                if($this->web_setting_model->_updateSlide($data['slide_data']->ws_id,$data_modify)){    
                    $this->session->set_flashdata('success','อัพเดตสไลด์เรียบร้อยแล้ว');
                }else{
                    $this->session->set_flashdata('error','ไม่สามารถอัพโหลดไฟล์ได้');
                }
            }
            
            $data['slide_data'] = $this->web_setting_model->_getSlide($id);
        }
		$this->breadcrumb('ตั้งค่าสไลด์หน้าเว็บหลัก','admin/setting-slide');
		$this->breadcrumb($title_page,'');
        $this->response('setting_slide_edit',$data);
    }

    public function slide_delete($id){
        $data = array(
            'status' => 'success'
            ,'message' => 'ลบสไลด์เรียบร้อยแล้ว'
        );
        
        $slide = $this->web_setting_model->_getSlide($id);

        if(!empty($slide)&&$this->web_setting_model->_deleteSlide($id)){
            if(!unlink($slide->ws_image_path)){
                $data = array(
                    'status' => 'error'
                    ,'message' => 'ลบสไลด์เรียบร้อยแล้ว แต่ไม่สามารถลบรูปได้โปรดติดต่อผู้ดูแลระบบ'
                );
            }
        }else{
            $data = array(
                'status' => 'error'
                ,'message' => 'พบข้อผิดพลาดในการลบสไลด์ โปรดลองใหม่อีกครั้ง!'
            );
        }
    
		echo json_encode($data);
    }
    
    // End Setting slide
    
}
