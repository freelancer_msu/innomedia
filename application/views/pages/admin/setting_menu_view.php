<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default card-view">
            <div class="panel-heading">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark"><?=$title?></h6>
                </div>
                <div class="clearfix"></div>
            </div>
            <hr style="margin-bottom: 10px">
            <div>
                <button id='add' class="btn btn-default btn-xs"><i class='glyphicon glyphicon-plus'></i> เพิ่มเมนู</button>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="dd" id="nestable">
                        <!-- Start Level 1 -->
                        <ol class="dd-list">
                            <?php
                            foreach ($page_data as $index => $value) {
                                ?>
                            <li class="dd-item" data-id="<?=$value->men_id;?>">
                                <div class="dd-handle"> <?=$value->men_name;?> - <a href="#" style="color: #337ab7;">แก้ไข</a> : <a class="lnkDel" href="#;" style="color: red;">ลบ</a></div>
                                <!-- Start Level 2 -->
                                <?php
                                if(is_array($value->child)){
                                    echo('<ol class="dd-list">');
                                    foreach ($value->child as $index2 => $value2) {
                                        ?>
                                        <li class="dd-item" data-id="<?=$value2->men_id;?>">
                                            <div class="dd-handle"> <?=$value2->men_name;?> - <a href="#" style="color: #337ab7;">แก้ไข</a> : <a href="#" style="color: red;">ลบ</a></div>
                                            <!-- Start Level 3 -->
                                            <?php
                                            if(is_array($value2->child)){
                                                echo('<ol class="dd-list">');
                                                foreach ($value2->child as $index3 => $value3) {
                                                    ?>
                                                    <li class="dd-item" data-id="<?=$value3->men_id;?>">
                                                        <div class="dd-handle"> <?=$value3->men_name;?> - <a href="#" style="color: #337ab7;">แก้ไข</a> : <a href="#" style="color: red;">ลบ</a></div>
                                                    </li>
                                                    <?php
                                                }
                                                echo('</ol>');
                                            }
                                            ?>
                                        </li>
                                        <?php
                                    }
                                    echo('</ol>');
                                }
                                ?>
                            </li>
                                <?php
                            }
                            ?>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="test-html"></div>