<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Core_Controller extends CI_Controller {

	public $layout = "layouts/main";
	public $page = "";
	public $title = "X-Child - ระบบสำหรับผู้ดูแลเด็กพิเศษ"; // title from LANGUAGE file
	public $sub_title = "";
	public $author = "IT Transport";
	public $description = "IT Transport";
	public $keywords = "IT Transport";
	public $default_element = "default/";
	public $menu = null;
	public $breadcrumb_use = false;
	public $_style = [];
	public $_script = [];
	public $_breadcrumb = [];
	protected $_default = [];

	public $home_element = "pages/home/";

	public $default_language = "thai";

	public $login_session_name = 'parent_logged_in';

	public $is_logged_in = FALSE;

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Bangkok');

		$this->_style = load_style();
		$this->_script = load_script();

		if($this->session->userdata('language')==NULL){
			$lang = $this->default_language;
			$this->session->set_userdata('language',$lang);
		}else{
			$lang = $this->session->userdata('language');
		}
		$this->lang->load('message',$lang);

		$this->is_logged_in = $this->ion_auth->logged_in();
		
		$this->_default = [
			"menu"=> $this->menu,
			"title"=> $this->title, 
			"sub_title"=> $this->sub_title, 
			"author"=> $this->author,
			"breadcrumb_use"=> $this->breadcrumb_use,
			"description"=> $this->description,
			"keywords"=> $this->keywords,
			"default_element" => $this->default_element,
			"home_element" => $this->home_element,
			"layout" => implode('/', array_slice(explode('/', $this->layout),0,2)).'/',
		];
	}

	protected function breadcrumb($name='', $link='')
	{
		array_push($this->_breadcrumb, (object)[ 'name'=> $name, 'link'=> $link ]);
	}

	protected function response($_page='', $param=[], $_layout=NULL)
	{
		if($_layout!=NULL) $this->layout = 'layouts/'.$_layout;
		
		$this->_default['page'] = 'pages/'.$_page;
		$this->_default['title'] = $this->title;
		$this->_default['sub_title'] = $this->sub_title;
		$this->_default['_script'] = $this->_script;
		$this->_default['_style'] = $this->_style;
		$this->_default['breadcrumb'] = $this->_breadcrumb;
		$_param = array_merge($this->_default, (array)$param);
		$this->load->view($this->layout, $_param);
	}
	
	protected function style($links=[])
	{
		$this->_style = array_merge($this->_style, $links);
	}
	protected function script($links=[])
	{
		$this->_script = array_merge($this->_script, $links);
	}

}
