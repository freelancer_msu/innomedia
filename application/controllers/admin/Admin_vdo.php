<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_vdo extends Core_Acontroller {

	public $title = 'วีดีโอ';

	public function __construct()
	{
		parent::__construct();
		$this->config->load('grocery_crud');
	}

	public function index() {
		$crud = new grocery_CRUD();

		$crud->unset_jquery();
		$crud->unset_export();
		$crud->unset_print();

		$crud->set_table('inno_vdo');
		$crud->set_subject($this->title);
		$crud->columns('vdo_title','vdo_desc','vdo_school','vdo_upload_by','vdo_upload_date');
		$crud->display_as('vdo_title','หัวข้อวีดีโอ')
			->display_as('vdo_desc','รายละเอียดวีดีโอ')
			->display_as('vdo_upload_by','เพิ่มโดย')
			->display_as('vdo_school','สังกัดโรงเรียน')
			->display_as('vdo_upload_date','เพิ่มเมื่อ')
			->display_as('vdo_preview','ภาพตัวอย่าง')
			->display_as('vdo_path','อัพโหลด วีดีโอ')
			->display_as('vdo_keyword','คำสำคัญ')
			->display_as('vdo_remark','หมายเหตุ')
			->display_as('vdo_child_type','ประเภทเด็กพิเศษ');

		$crud->add_fields('vdo_title','vdo_desc','vdo_preview', 'vdo_path', 'vdo_school', 'vdo_keyword', 'vdo_remark','vdo_child_type');
		$crud->edit_fields('vdo_title','vdo_desc','vdo_preview', 'vdo_path', 'vdo_school', 'vdo_keyword', 'vdo_remark','vdo_child_type');

		$crud->set_relation('vdo_school','inno_school','{sch_name}');
		$crud->set_relation('vdo_upload_by','inno_users','{use_first_name} {use_last_name}');

		$crud->set_relation_n_n('vdo_child_type','vdo_in_type','child_type','vit_vdo_id','vit_type_id','ct_description');

		$crud->field_type('vdo_child_type','multiselect');

		$crud->set_field_upload('vdo_preview','assets/imgs/vdo_preview');
		$crud->set_field_upload('vdo_path','assets/imgs/vdo');

		$crud->callback_column('vdo_upload_date',array($this,'modified_date_callback'));

		// $crud->callback_before_insert(array($this,'_save_filepath_uploaded_file'));
		// $crud->callback_before_update(array($this,'_save_filepath_uploaded_file'));

		$output = $crud->render();
		$data['crud_data'] = $output->output;

		$this->script($output->js_files); // load js script from crud

		$this->style($output->css_files); // load css script from crud

		$data['main_menu'] = $this->load_main_menu();
		$this->breadcrumb($this->title,base_url('admin/home'));
		$this->response('home',$data);
	}
	public function _save_filepath_uploaded_file($post_array) 
	{
		if (!empty($post_array['vdo_preview'])) {
			$post_array['vdo_preview'] =  'assets/imgs/vdo_preview/'.$post_array['vdo_preview'];
		}
		if (!empty($post_array['vdo_path'])) {
			$post_array['vdo_path'] =  'assets/imgs/vdo/'.$post_array['vdo_path'];
		}
		return $post_array;
	}
	public function modified_date_callback($value, $row){
		if(empty($row->vdo_upload_date) or $row->vdo_upload_date == '0000-00-00 00:00:00'){
			$answer = '';
		}else{
			$answer = $this->globals->dateformat_full(date($row->vdo_upload_date),'thai');
		}
		return $answer;
	}
}