<?php
	function load_app_controllers()
	{
		spl_autoload_register('my_own_controllers');
		spl_autoload_register('my_own_modules_controllers');
	}
	
	function my_own_controllers($class)
	{
		if (strpos($class, 'CI_') !== 0)
		{
			if (is_readable(APPPATH . 'core/' . $class . '.php'))
			{
				require_once(APPPATH . 'core/' . $class . '.php');
			}
		}
	}

	function my_own_modules_controllers($class)
	{
		if (strpos($class, 'CI_') !== 0)
		{
			if (is_readable(APPPATH . 'modules/bev2dorn/core/' . $class . '.php'))
			{
				require_once(APPPATH . 'modules/bev2dorn/core/' . $class . '.php');
			}
		}
	}