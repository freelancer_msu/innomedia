<link href="<?=base_url('vendors/sweetalert2/dist/sweetalert2.min.css')?>" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<!-- Row -->
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h4 class="panel-title txt-dark"><?=$page_title?></h4>
				</div>
				<div class="pull-right">
				<a class="btn btn-xs btn-success" href="<?=base_url('admin/child/insert')?>">เพิ่มข้อมูล <?=$page_title?></a></div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="table-wrap">
						<div class="table-responsive">
							<table id="foo-child-list" class="table table-hover display pb-30" >
								<thead>
									<tr>
										<th>#</th>
										<th>ประเภทเด็กพิเศษ</th>
										<th align="center">แก้ไขครั้งล่าสุด</th>
										<th align="center">แก้ไขโดย</th>
										<th width="20%"></th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<!-- /Row -->

<!-- View Modal -->
<div class="modal fade" id="viewChildTypeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title title-from-api" id="viewChildTypeModalLargeModalLabel">loading</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="ปิด">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="viewChildTypeModalLoadContent">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning">แก้ไข</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
			</div>
		</div>
	</div>
</div>

<script src="<?=base_url('vendors/sweetalert2/dist/sweetalert2.min.js')?>"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){

		$('#viewChildTypeModal').on('show.bs.modal', function(e) {
			var lookup_id = $(e.relatedTarget).data('lookupid');
			$('#viewChildTypeModalLoadContent').html('loading please wait ..');

			$.post('<?=base_url("admin/child/view")?>',{_id:lookup_id},function(res,err){
				var response = $.parseJSON(res);
				if(response.status == 1){
					$('#viewChildTypeModalLargeModalLabel').html(response.title);
					$('#viewChildTypeModalLoadContent').html(response.message);
				}
			});
		});

		$('#foo-child-list').DataTable( {
			responsive: {
				details: {
					renderer: function ( api, rowIdx, columns ) {
						var data = $.map( columns, function ( col, i ) {
							return col.hidden ?
							'<tr data-dt-row="'+col.rowIndex+'" data-dt-column="'+col.columnIndex+'">'+
							'<td>'+col.title+':'+'</td> '+
							'<td>'+col.data+'</td>'+
							'</tr>' :
							'';
						} ).join('');

						return data ?
						$('<table/>').append( data ) :
						false;
					}
				}
			},
			autoWidth: false,
			processing: true,
			ajax: '<?=base_url($source_url)?>',
			columns: [
				{ data: 'id' },
				{ data: 'description' },
				{ data: 'update_date' },
				{ data: 'update_by' },
				{ data: null },
			],
			columnDefs: [
				{
					"render": function ( data, type, row ) { 
						var view_btn = '<a class="btn btn-xs btn-primary" data-toggle="modal" data-lookupid="'+row[0]['ct_code']+'" data-target="#viewChildTypeModal"><i class="fa fa-eye" style="font-size: 18px;"></i></a>';
						var edit_btn = '<a href="<?=base_url('admin/child/edit')?>/'+row[0]['ct_code']+'" class="btn btn-xs btn-warning"><i class="fa fa-edit" style="font-size: 18px;"></i></a>';
						topic = view_btn+' '+edit_btn;
						return topic;
					},
					targets:[4],
					orderable: false
				},
			],
			initComplete: function( settings, json ) {            

			},
			order: [[ 0, "asc" ]]

		});
	});

</script>