<?php
class My_grocery_model  extends Grocery_crud_model {
  
    function db_update($post_array, $primary_key_value)
    {
        if ($this->field_exists('updated'))
        {
            $this->load->helper('date');
            $post_array['updated'] = date('Y-m-d H:i:s',now());
        }
    
        return parent::db_update($post_array, $primary_key_value);
    }  
  
    function db_insert($post_array)
    {
        if ($this->field_exists('updated') && $this->field_exists('created'))
        {
            $this->load->helper('date');
            $post_array['created'] = date('Y-m-d H:i:s',now());
            $post_array['updated'] = date('Y-m-d H:i:s',now());
        }
        return parent::db_insert($post_array);
    }
  
}