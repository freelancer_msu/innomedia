<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_patronize_type extends Core_Acontroller {

	public $title = 'Groups';

	public function __construct()
	{
		parent::__construct();
		$this->config->load('grocery_crud');
	}

	public function index() {
		$crud = new grocery_CRUD();

		$crud->unset_jquery();
		$crud->unset_export();
		$crud->unset_print();

		$crud->set_table('inno_patronize_type');
		$crud->set_subject('ประเภทความสัมพันธ์');
		$crud->columns('patt_id','patt_name','patt_status','patt_update_date');

		$crud->display_as('patt_id','ผู้ปกครอง')
			->display_as('patt_name','ประเภทการอุปถัมภ์')
			->display_as('patt_status','เด็กพิเศษ')
			->display_as('patt_update_date','วันที่แก้ไขล่าสุด');

		$crud->add_fields('patt_name','patt_status');
		$crud->edit_fields('patt_name','patt_status');

		$crud->field_type('patt_status','enum',array('A' =>'ใช้งาน','C'=>'ไม่ใช้งาน' ));

		$output = $crud->render();
		$data['crud_data'] = $output->output;

		$this->script($output->js_files); // load js script from crud

		$this->style($output->css_files); // load css script from crud

		$data['main_menu'] = $this->load_main_menu();
		$this->breadcrumb($this->title,base_url('admin/home'));
		$this->response('home',$data);
  }
}