<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Education extends Core_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('general_model');
    }

	public function index()
	{
		$this->load->helper('text');
		$data['events_list'] = $this->general_model->_get_all(0);
		$data['news_list'] = $this->general_model->_get_all(1);
		$data['slider'] = $this->general_model->_get_slide();
		$data['is_logged_in'] = $this->is_logged_in;
		$this->response('',$data);
	}
}
