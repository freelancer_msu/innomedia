<div class="container">

     <div class="row" id="main-boxes">
    	<div class="col-md-4">
        	<div class="box-style-2 green">
                <a href="<?=base_url('find/school')?>" title="ค้นหาโรงเรียน">
                    <img class="img-fluid" src="<?=base_url('assets/imgs/education/icon-home-visit.png')?>" alt="">
                    <h3 style="font-size: 24px">ค้นหาโรงเรียน</h3>
                    <p>การค้นหาโรงเรียนสำหรับเด็กพิเศษทั้วประเทศไทย และค้นหาโรงเรียนสำหรับเด็กพิเศษที่อยู่ใกล้ท่าน</p>
                </a>
            </div>
        </div>
        
        <div class="col-md-4">
        	<div class="box-style-2 orange">
            	<a href="<?=base_url('story')?>" title="นิทานสำหรับเด็ก">
                	<img class="img-fluid" src="<?=base_url('assets/imgs/education/icon-home-course.png')?>" alt="">
                    <h3 style="font-size: 24px">นิทานสำหรับเด็ก</h3>
                    <p>เหมาะสำหรับน้องๆ และผู้ปกครองที่สนใจหานิทานให้อ่านลูกๆ ได้ฟังรวมทั้งสามารถฟังนิทานได้จากในเว็บไซต์</p>
                </a>
            </div>
        </div>
        
        <div class="col-md-4">
        	<div class="box-style-2 red">
            	<a href="<?=base_url('contact')?>" title="ติดต่อเรา">
                	<img class="img-fluid" src="<?=base_url('assets/imgs/education/icon-home-apply.png')?>" alt="">
                    <h3 style="font-size: 24px">ติดต่อเรา</h3>
                    <p>สำหรับช่องทางการติดต่อสื่อสารกับทีมงานผู้พัฒนา และรายงานความผิดพลาดของระบบ</p>
                </a>
            </div>
        </div>
    </div>
</div> <!-- end container-->