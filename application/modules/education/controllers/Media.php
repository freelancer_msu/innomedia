<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends Core_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('media_model');
    }
	public function index()
	{
		$this->load->helper('text');
        $data['is_logged_in'] = $this->is_logged_in;
        $data['list_data'] = $this->media_model->_getAll();
		$this->response('media_list',$data,'info_page');
	}
	public function card_page($content_id = null){
        $data['is_logged_in'] = $this->is_logged_in;
        $data['result_data'] = $this->media_model->_get($content_id);
        // print_r($data);
		$this->response('media_card',$data,'info_page');
	}
}
