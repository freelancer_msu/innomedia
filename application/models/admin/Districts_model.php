<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Districts_model extends CI_Model {

    private $_table = 'districts';

    public function __construct()
    {
        parent::__construct();
    }

    public function _get_all(){
		$this->db->select('*');
		$this->db->from($this->_table);
		$this->db->join('amphures','district_amphures = amphure_id','left');
		$this->db->join('province','district_province = pro_id','left');
		$this->db->where('district_active','A');
		$this->db->order_by('district_id','asc');

		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		}else {
			return false;
		}
	}

}