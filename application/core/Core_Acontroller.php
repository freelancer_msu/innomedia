<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Core_Acontroller extends CI_Controller {

	public $layout = "layouts/admin/a_main";
	public $page = "";
	public $title = "web_title"; // title from LANGUAGE file
	public $sub_title = "";
	public $author = "IntLab MSU 2020";
	public $description = "Innovation Project powered by IntLab MSU 2020";
	public $keywords = "Innovation";
	public $default_element = "layouts/admin/default/";
	public $menu = null;
	public $breadcrumb_use = true;
	public $_style = [];
	public $_script = [];
	public $_breadcrumb = [];
	protected $_default = [];

	public $default_language = "thai";

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Bangkok');

		$this->lang->load('auth');

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else
		{
			// already logged in
			$this->init();
		}
	}

	private function init(){
		$this->_style = load_admin_style();
		$this->_script = load_admin_script();

		if($this->session->userdata('language')==NULL){
			$lang = $this->default_language;
			$this->session->set_userdata('language',$lang);
		}else{
			$lang = $this->session->userdata('language');
		}
		$this->lang->load('message',$lang);
		
		$this->_default = [
			"menu"=> $this->menu,
			"title"=> $this->title, 
			"sub_title"=> $this->sub_title, 
			"author"=> $this->author,
			"breadcrumb_use"=> $this->breadcrumb_use,
			"description"=> $this->description,
			"keywords"=> $this->keywords,
			"default_element" => $this->default_element,
			"layout" => implode('/', array_slice(explode('/', $this->layout),0,2)).'/',
		];
	}

	protected function breadcrumb($name='', $link='')
	{
		array_push($this->_breadcrumb, (object)[ 'name'=> $name, 'link'=> $link ]);
	}

	protected function response($_page='', $param=[], $_layout=NULL)
	{
		if($_layout!=NULL) $this->layout = 'layouts/admin/'.$_layout;
		
		$this->_default['page'] = 'pages/admin/'.$_page;
		$this->_default['title'] = $this->title;
		$this->_default['sub_title'] = $this->sub_title;
		$this->_default['_script'] = $this->_script;
		$this->_default['_style'] = $this->_style;
		$this->_default['breadcrumb'] = $this->_breadcrumb;

		$_param = array_merge($this->_default, (array)$param);

		$permission['permission'] = $this->load_permission();
		$_param = array_merge($_param, (array)$permission);
		
		$this->load->view($this->layout, $_param);
	}
	
	protected function style($links=[])
	{
		$this->_style = array_merge($this->_style, $links);
	}
	protected function script($links=[])
	{
		$this->_script = array_merge($this->_script, $links);
	}

	// self scripts
	public function load_main_menu(){
		$this->load->model('admin/admin_dashboard_model');
		return $this->admin_dashboard_model->_get_main_menu();
	}

	public function load_permission(){
		$this->load->model('admin/admin_dashboard_model');
		return $this->admin_dashboard_model->_get_permission();
	}

}
