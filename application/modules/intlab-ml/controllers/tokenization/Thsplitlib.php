<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Thsplitlib extends Core_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('tokenization/segment');
		$this->load->library('utilities/bow');
	}

	public function index()
	{
		$answer['input'] = 'ในวันอังคารที่ 17 มีนาคมนี้เอง 2563 ศูนย์ประสบการณ์วิชาชีพครูขอเปลี่ยนแปลงสถานที่แบบมอบเข็มวิชาชีพครู ประจำปีการศึกษา 2563 จากห้องประชุมสายสุรี จุติกุล เปลี่ยนเป็นห้องตามที่สาขาวิชากำหนด โดยเริ่มพิธีมอบเข็มเวลา 15.30 น. เป็นต้นไป';

		$this->segment->set_stopword_check(false);
		$answer['output_wout_stw'] = $this->segment->get_segment_array($answer['input']);
		$this->segment->set_stopword_check();
		$answer['output_w_stw'] = $this->segment->get_segment_array($answer['input']);
		$this->load->view('test_call',$answer);
	}

	public function bow_test(){
		$sentense_1 = 'ในวันอังคารที่ 17 มีนาคมนี้เอง 2563'; 
		$sentense_2 = 'ศูนย์ประสบการณ์วิชาชีพครูขอเปลี่ยนแปลงสถานที่แบบมอบเข็มวิชาชีพครู';
		$sentense_3 = 'ประจำปีการศึกษา 2563 จากห้องประชุมสายสุรี จุติกุล';
		$sentense_4 = 'เปลี่ยนเป็นห้องตามที่สาขาวิชากำหนด โดยเริ่มพิธีมอบเข็มเวลา 15.30 น. เป็นต้นไป';

		$inn_bow = $this->segment->get_segment_array($sentense_1);
		
		array_merge($inn_bow,$this->segment->get_segment_array($sentense_2));
		array_merge($inn_bow,$this->segment->get_segment_array($sentense_3));
		array_merge($inn_bow,$this->segment->get_segment_array($sentense_4));

		$answer['bow'] = $inn_bow;
		$this->load->view('test_bow',$answer);
	}
}
