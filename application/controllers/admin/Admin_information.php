<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_information extends Core_Acontroller {

	public $title = 'Information';

	public function __construct()
	{
		parent::__construct();
		$this->config->load('grocery_crud');
	}

	public function list_info() {
		$crud = new grocery_CRUD();

		$crud->unset_jquery();
		$crud->unset_export();
		$crud->unset_print();

		$crud->set_table('inno_inforandevent');
		$crud->where('info_type','0');
		$crud->set_subject('ข่าวสาร');

		$crud->columns('info_title','info_school','info_post_by','info_post_date');
		$crud->set_relation('info_post_by','inno_users','{use_first_name}');
		$crud->set_relation('info_school','inno_school','{sch_name}');

		$output = $crud->render();
		$data['crud_data'] = $output->output;

		$this->script($output->js_files); // load js script from crud

		$this->style($output->css_files); // load css script from crud

		$data['main_menu'] = $this->load_main_menu();
		$this->breadcrumb($this->title,base_url('admin/home'));
		$this->response('home',$data);
  }
  public function list_event() {
		$crud = new grocery_CRUD();

		$crud->unset_jquery();
		$crud->unset_export();
		$crud->unset_print();

		$crud->set_table('inno_inforandevent');
		$crud->where('info_type','1');
		$crud->set_subject('กิจกรรม');

		$crud->columns('info_title','info_school','info_post_by','info_post_date');
		$crud->set_relation('info_post_by','inno_users','{use_first_name}');
		$crud->set_relation('info_school','inno_school','{sch_name}');

		$output = $crud->render();
		$data['crud_data'] = $output->output;

		$this->script($output->js_files); // load js script from crud

		$this->style($output->css_files); // load css script from crud

		$data['main_menu'] = $this->load_main_menu();
		$this->breadcrumb($this->title,base_url('admin/home'));
			$this->response('home',$data);
	}
}