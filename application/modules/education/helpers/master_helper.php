<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('admin_menu'))
{
	// helper menu with language
	function menu($id=null) {
		$lang_menu = load_lang();
		$_menu = [
			(object)["id"=> 1, "name"=>$lang_menu->lang->line('menu_home'), "active"=> false, "link"=> base_url(),
		], 
		];
		if(!empty($id)){
			foreach ($_menu as $menu) {
				if($menu->id==$id)
					$menu->active = true;
			}
		}
		return $_menu;
	}
}

if ( ! function_exists('load_lang')) {
	function load_lang(){
		$CI = & get_instance();
		if($CI->session->userdata('language')==NULL){
			$lang = "thai";
			$CI->session->set_userdata('language',$lang);
		}else{
			$lang = $CI->session->userdata('language');
		}
		return $CI;
	}
}

if ( ! function_exists('load_style'))
{
	function load_style() {
		return [
			base_url('vendors/bootstrap/dist/css/bootstrap.min.css'),
			base_url('vendors/font-awesome/css/font-awesome.css'),
			base_url('vendors/fancybox/source/jquery.fancybox.css?v=2.1.4'),
			base_url('vendors/rs-plugin/css/settings.css'),
			base_url('assets/css/fullwidth.css'),
			base_url('assets/css/megamenu.css'),
			base_url('assets/css/edu-style.css')
		];
	}
}

if ( ! function_exists('load_script'))
{
	function load_script() {
		return [
			base_url('vendors/bootstrap/dist/js/bootstrap.min.js'),
			base_url('assets/js/respond.min.js'),
			base_url('assets/js/modernizr.custom.17475.js'),
			base_url('assets/js/jquery.easing.js'),
			base_url('assets/js/megamenu.js'),
			base_url('assets/js/functions.js'),
			base_url('assets/js/validate.js'),
			base_url('vendors/fancybox/source/jquery.fancybox.pack.js?v=2.1.4'),
			base_url('vendors/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5s'),
			base_url('assets/js/fancy_func.js'),
			base_url('vendors/rs-plugin/js/jquery.themepunch.plugins.min.js'),
			base_url('vendors/rs-plugin/js/jquery.themepunch.revolution.min.js'),
			base_url('assets/js/revolutio-slider-func.js')

		];
	}
}