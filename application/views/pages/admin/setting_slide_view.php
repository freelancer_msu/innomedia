<link href="<?=base_url('vendors/sweetalert2/dist/sweetalert2.min.css')?>" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<!-- Row -->
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h4 class="panel-title txt-dark"><?=$page_title?></h4>
				</div>
				<div class="pull-right">
				<a class="btn btn-xs btn-success" href="<?=base_url('admin/setting-slide/insert')?>"><i class="fa fa-plus" style="color: #fff;font-size: unset;"></i> เพิ่มสไลด์</a></div>
				<div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="table-wrap">
						<div class="table-responsive">
							<table id="slide-list" class="table table-hover display pb-30" >
								<thead>
									<tr>
										<th>#</th>
										<th>สไลด์</th>
										<th>คำอธิบาย</th>
										<th>เพิ่มโดย</th>
										<th>เพิ่มเมื่อ</th>
										<th width="20%"></th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>

</div>

<script src="<?=base_url('vendors/sweetalert2/dist/sweetalert2.min.js')?>"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
        $('#slide-list').DataTable( {
			autoWidth: false,
			processing: true,
			ajax: '<?=base_url($source_url)?>',
			columns: [
				{ data: 'iRow' },
				{ data: 'image_path' },
				{ data: 'description' },
				{ data: 'upload_by' },
				{ data: 'upload_date' }
			],
			columnDefs: [
				{
					"render": function ( data, type, row ) { 
						var edit_btn = '<a href="<?=base_url('admin/setting-slide/edit')?>/'+row['id']+'" class="btn btn-xs btn-warning"><i class="fa fa-edit" style="font-size: 18px;"></i></a>';
						var delete_btn = '<a href="<?=base_url('admin/setting-slide/delete')?>/'+row['id']+'" class="btn btn-xs btn-danger"><i class="fa fa-trash-o" style="font-size: 18px;"></i></a>';
						return edit_btn+' '+delete_btn;
					},
					targets:[5],
					orderable: false
				}
			],
			initComplete: function( settings, json ) {
			},
			order: [[ 0, "asc" ]]

		});

    });
</script>
