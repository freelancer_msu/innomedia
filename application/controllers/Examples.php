<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Examples extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}

	public function _example_output($output = null)
	{
		$this->load->view('example.php',(array)$output);
	}

	public function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function offices_management()
	{

			$this->config->load('grocery_crud');
			$this->config->set_item('grocery_crud_dialog_forms',true);
			$this->config->set_item('grocery_crud_dialog_color','light-blue darken-4');
			$this->config->set_item('grocery_crud_dialog_text_color','white');
			$this->config->set_item('table_responsive','false');

		try{
			$crud = new grocery_CRUD();
            $crud->set_theme('materializecss'); // magic code
			$crud->set_table('offices');
			$crud->set_subject('Office');
			$crud->required_fields('city');
			$crud->columns('city','country','phone','addressLine1','postalCode');

			$output = $crud->render();

			$output->codes = nl2br("
			public function offices_management()
			{

			\$this->config->load('grocery_crud');
			\$this->config->set_item('grocery_crud_dialog_forms',true);
			\$this->config->set_item('grocery_crud_dialog_color','light-blue darken-4');
			\$this->config->set_item('grocery_crud_dialog_text_color','white');
			\$this->config->set_item('table_responsive','false');

				try{
					\$crud = new grocery_CRUD();
                    \$crud->set_theme('materializecss'); // magic code
					\$crud->set_table('offices');
					\$crud->set_subject('Office');
					\$crud->required_fields('city');
					\$crud->columns('city','country','phone','addressLine1','postalCode');

					\$output = \$crud->render();

					\$this->_example_output(\$output);

				}catch(Exception \$e){
					show_error(\$e->getMessage().' --- '.\$e->getTraceAsString());
				}
			}");

			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function employees_management()
	{
			$this->config->load('grocery_crud');
			$this->config->set_item('grocery_crud_dialog_forms',true);
			$this->config->set_item('grocery_crud_dialog_color','darken-4 cyan');
			$this->config->set_item('grocery_crud_dialog_text_color','white');

			$crud = new grocery_CRUD();

			$crud->set_theme('materializecss'); // magic code
			$crud->set_table('employees')
				->set_subject('Employee')
				->columns('employeeNumber','firstName','lastName','email','officeCode','jobTitle')
				->set_relation('officeCode','offices','city');

			$crud->display_as('email','E-Mail')
				 ->display_as('employeeNumber','ID')
				 ->display_as('firstName','Name')
				 ->display_as('lastName','Last Name');

			$crud->required_fields('firstName','lastName');

			$crud->add_fields('firstName','lastName','email','jobTitle', 'officeCode');
			$crud->set_field_upload('file_url','assets/uploads/files', 'pdf');

			$crud->unset_jquery_ui();
            $crud->unset_bootstrap();
            //$crud->unset_delete();

			$output = $crud->render();

			$output->codes = nl2br("
			public function employees_management()
			{

			\$this->config->load('grocery_crud');
			\$this->config->set_item('grocery_crud_dialog_forms',true);
			\$this->config->set_item('grocery_crud_dialog_color','darken-4 cyan');
			\$this->config->set_item('grocery_crud_dialog_text_color','white');

				\$this->config->load('grocery_crud');
				\$this->config->set_item('grocery_crud_dialog_forms',true);

				\$crud = new grocery_CRUD();

				\$crud->set_theme('materializecss'); // magic code
				\$crud->set_table('employees')
					->set_subject('Employee')
					->columns('employeeNumber','firstName','lastName','email','officeCode','jobTitle')
					->set_relation('officeCode','offices','city');

				\$crud->display_as('email','E-Mail')
					 ->display_as('employeeNumber','ID')
					 ->display_as('firstName','Name')
					 ->display_as('lastName','Last Name');

				\$crud->required_fields('firstName','lastName');

				\$crud->add_fields('firstName','lastName','email','jobTitle', 'officeCode');

				\$output = \$crud->render();

				\$this->_example_output(\$output);
			}
			");

			$this->_example_output($output);
	}

	public function customers_management()
	{

			$this->config->load('grocery_crud');
			$this->config->set_item('grocery_crud_dialog_forms',true);
			$this->config->set_item('grocery_crud_dialog_color','green darken-4');
			$this->config->set_item('grocery_crud_dialog_text_color','white');

			$crud = new grocery_CRUD();
			$crud->set_theme('materializecss'); // magic code
			$crud->set_table('customers');
			$crud->columns('customerName','contactLastName','phone','city','country','salesRepEmployeeNumber','creditLimit');
			$crud->display_as('salesRepEmployeeNumber','from Employeer')
				 ->display_as('customerName','Name')
				 ->display_as('contactLastName','Last Name');
			$crud->set_subject('Customer');
			$crud->set_relation('salesRepEmployeeNumber','employees','lastName');

			$output = $crud->render();

            $output->codes = nl2br("
            public function customers_management()
			{

			\$this->config->load('grocery_crud');
			\$this->config->set_item('grocery_crud_dialog_forms',true);
			\$this->config->set_item('grocery_crud_dialog_color','green darken-4');
			\$this->config->set_item('grocery_crud_dialog_text_color','white');

					\$crud = new grocery_CRUD();
                    \$crud->set_theme('materializecss'); // magic code
					\$crud->set_table('customers');
					\$crud->columns('customerName','contactLastName','phone','city','country','salesRepEmployeeNumber','creditLimit');
					\$crud->display_as('salesRepEmployeeNumber','from Employeer')
						 ->display_as('customerName','Name')
						 ->display_as('contactLastName','Last Name');
					\$crud->set_subject('Customer');
					\$crud->set_relation('salesRepEmployeeNumber','employees','lastName');

					\$output = \$crud->render();

					\$this->_example_output(\$output);
			}");

			$this->_example_output($output);
	}

	public function orders_management()
	{

			$this->config->load('grocery_crud');
			$this->config->set_item('grocery_crud_dialog_forms',true);
			$this->config->set_item('grocery_crud_dialog_color','deep-orange');
			$this->config->set_item('grocery_crud_dialog_text_color','black');

			$crud = new grocery_CRUD();
            $crud->set_theme('materializecss'); // magic code
			$crud->set_relation('customerNumber','customers','{contactLastName} {contactFirstName}');
			$crud->display_as('customerNumber','Customer');
			$crud->set_table('orders');
			$crud->set_subject('Order');
			$crud->unset_add();
			$crud->unset_delete();

			$output = $crud->render();

            $output->codes = nl2br("
			public function orders_management()
			{

			\$this->config->load('grocery_crud');
			\$this->config->set_item('grocery_crud_dialog_forms',true);
			\$this->config->set_item('grocery_crud_dialog_color','deep-orange');
			\$this->config->set_item('grocery_crud_dialog_text_color','black');

					\$crud = new grocery_CRUD();

					\$crud->set_relation('customerNumber','customers','{contactLastName} {contactFirstName}');
					\$crud->display_as('customerNumber','Customer');
					\$crud->set_table('orders');
					\$crud->set_subject('Order');
					\$crud->unset_add();
					\$crud->unset_delete();

					\$output = \$crud->render();

					\$this->_example_output(\$output);
			}");

			$this->_example_output($output);
	}

	public function products_management()
	{

			$this->config->load('grocery_crud');
			$this->config->set_item('grocery_crud_dialog_forms',true);
			$this->config->set_item('grocery_crud_dialog_color','pink darken-4');
			$this->config->set_item('grocery_crud_dialog_text_color','white');

			$crud = new grocery_CRUD();
            $crud->set_theme('materializecss'); // magic code
			$crud->set_table('products');
			$crud->set_subject('Product');
			$crud->unset_columns('productDescription');
			$crud->callback_column('buyPrice',array($this,'valueToEuro'));

			$output = $crud->render();

			$output->codes = nl2br("
			public function products_management()
			{

			\$this->config->load('grocery_crud');
			\$this->config->set_item('grocery_crud_dialog_forms',true);
			\$this->config->set_item('grocery_crud_dialog_color','pink darken-4');
			\$this->config->set_item('grocery_crud_dialog_text_color','white');


					\$crud = new grocery_CRUD();
		            \$crud->set_theme('materializecss'); // magic code
					\$crud->set_table('products');
					\$crud->set_subject('Product');
					\$crud->unset_columns('productDescription');
					\$crud->callback_column('buyPrice',array(\$this,'valueToEuro'));

					\$output = \$crud->render();

					\$this->_example_output(\$output);
			}");

			$this->_example_output($output);
	}

	public function valueToEuro($value, $row)
	{
		return $value.' &euro;';
	}

	public function film_management()
	{
			$this->config->load('grocery_crud');
			$this->config->set_item('grocery_crud_dialog_forms',true);
			$this->config->set_item('grocery_crud_dialog_color','blue');

		$crud = new grocery_CRUD();
        $crud->set_theme('materializecss'); // magic code

		$crud->set_table('film');
		$crud->set_relation_n_n('actors', 'film_actor', 'actor', 'film_id', 'actor_id', 'fullname','priority');
		$crud->set_relation_n_n('category', 'film_category', 'category', 'film_id', 'category_id', 'name');
		$crud->unset_columns('special_features','description','actors');

		$crud->fields('title', 'description', 'actors' ,  'category' ,'release_year', 'rental_duration', 'rental_rate', 'length', 'replacement_cost', 'rating', 'special_features');

		$output = $crud->render();

        $output->codes = nl2br("
		public function film_management()
		{

			\$this->config->load('grocery_crud');
			\$this->config->set_item('grocery_crud_dialog_forms',true);
			\$this->config->set_item('grocery_crud_dialog_color','blue');

			\$crud = new grocery_CRUD();
	        \$crud->set_theme('materializecss'); // magic code
			\$crud->set_table('film');
			\$crud->set_relation_n_n('actors', 'film_actor', 'actor', 'film_id', 'actor_id', 'fullname','priority');
			\$crud->set_relation_n_n('category', 'film_category', 'category', 'film_id', 'category_id', 'name');
			\$crud->unset_columns('special_features','description','actors');

			\$crud->fields('title', 'description', 'actors' ,  'category' ,'release_year', 'rental_duration', 'rental_rate', 'length', 'replacement_cost', 'rating', 'special_features');

			\$output = \$crud->render();

			\$this->_example_output(\$output);
		}");

		$this->_example_output($output);
	}

	public function film_management_twitter_bootstrap()
	{
		try{
			$crud = new grocery_CRUD();
            $crud->set_theme('materializecss'); // magic code
			$crud->set_table('film');
			$crud->set_relation_n_n('actors', 'film_actor', 'actor', 'film_id', 'actor_id', 'fullname','priority');
			$crud->set_relation_n_n('category', 'film_category', 'category', 'film_id', 'category_id', 'name');
			$crud->unset_columns('special_features','description','actors');

			$crud->fields('title', 'description', 'actors' ,  'category' ,'release_year', 'rental_duration', 'rental_rate', 'length', 'replacement_cost', 'rating', 'special_features');

			$output = $crud->render();
			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

}
