<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Province_model extends CI_Model {

    private $_table = 'province';

    public function __construct()
    {
        parent::__construct();
    }

    public function _get_all(){
		$this->db->select('*');
		$this->db->from($this->_table);
		$this->db->where('pro_active','A');
		$this->db->order_by('pro_id','asc');

		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		}else {
			return false;
		}
	}

	public function _get_specific($col,$value){
		$this->db->select('*');
		$this->db->from($this->_table);
		$this->db->where($col,$value);

		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->row();
		}else {
			return false;
		}
	}

}