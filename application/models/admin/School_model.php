
<?php

defined('BASEPATH') or exit('No direct script access allowed');

class School_model extends CI_Model
{

    private $TABLE = 'school';

    public function __construct()
    {
        parent::__construct();
    }

    public function _getAll()
    {
        $this->db->select('*');
        $this->db->from($this->TABLE);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function _insert($data)
    {
        return $this->db->insert($this->TABLE, $data);
    }

    public function _update($code, $data)
    {
        $this->db->where('sch_code', $code);
        return $this->db->update($this->TABLE, $data);
    }

    public function _delete($code)
    {
        $this->db->where('sch_code', $code);
        return $this->db->delete($this->TABLE);
    }
}
                        
/* End of file School_model.php */
