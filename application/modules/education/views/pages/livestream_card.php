<div class="container">

<div class="row">
	<div class="col-md-12">
	<h1><?=$page_header;?></h1>
		<ul class="breadcrumb">
			<li><a href="<?=base_url()?>">หน้าหลัก</a><span class="divider"></span></li>
            <li><a href="<?=base_url('livestream')?>">ถ่ายทอดสด</a><span class="divider"></span></li>
			<li class="active"><?=$data_detail->live_name;?></li>
		</ul>
	</div>
    
	<!-- =========================Start Col center section ============================= -->
<section class="col-md-12 col-sm-12">
<div class="col-right">
	<div class="post">
		<h2><a href="#"><?=$data_detail->live_name;?></a></h2>
		<img src="img/blog-1.jpg" alt="" class="img-responsive">
		<div class="post_info clearfix">
			<div class="post-left">
				<ul>
					<li><i class="icon-calendar-empty"></i>On <span><?=$data_detail->live_updated;?></span></li>
					<li><i class="icon-user"></i>By <a href="#">Administrator</a></li>
				</ul>
			</div>
		</div>
		<div class="livestream-view">
		<?=$data_detail->live_embed;?>
		</div>
		<hr>
		<p>
		<?=$data_detail->live_description;?>
		</p>
	</div><!-- end post -->
</div><!-- end col-center-->
</section><!-- end section-->
</div><!-- end row-->
</div><!-- end container-->