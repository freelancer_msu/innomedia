<link rel="stylesheet" href="<?=base_url('vendors/owl.carousel/dist/assets/owl.carousel.min.css')?>" />
<link rel="stylesheet" href="<?=base_url('vendors/owl.carousel/dist/assets/owl.theme.default.min.css')?>" />
<div class="container">
  <div class="row">

    <section class="col-md-12">
      <div class="col-right">
        <h2>ข่าว / กิจกรรมเกี่ยวกับเด็กพิเศษ</h2>
        <hr>
        <div class="strip-news">
          <div class="owl-carousel owl-theme">
            <?php
            $news_count = 12;
            $news_i = 0;
            foreach($news_list as $key => $value){
              $news_i++;
              if($news_i > $news_count) break;
              ?>
              <div class="info-card">
                <a href="<?=base_url('events/'.$value->info_id)?>">
                  <img class="owl-lazy" data-src="<?=$value->info_sample_pic?>" alt="">
                  <div class="info-caption">
                    <p><?=character_limiter(strip_tags($value->info_content), 100)?></p>
                  </div>
                </a>
              </div>
              <?php
            }
            ?>
          </div>
          <hr>
          <p class="text-center"><a href="<?= base_url('events') ?>" class="button_large">ดูทั้งหมด</a></p>
        </div>
      </div>
    </section>

    <script src="<?=base_url('vendors/owl.carousel/dist/owl.carousel.min.js')?>"></script>
    <script>
    $(document).ready(function(){
      $('.owl-carousel').owlCarousel({
        lazyLoad:true,
        // loop:true,
        dots:true,
        margin:10,
        responsiveClass:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
      });
    });
    </script>

    <aside class="col-md-4 ">
      <p><a href="<?=base_url('livestream');?>">
          <img class="live-picture img-rounded" src="<?= base_url('assets/imgs/education/live-picture-4.png') ?>" alt="Banner">
        </a></p>

      <div class="box-style-1 ribbon borders">

        <div class="feat">
          <i class="icon-film icon-3x"></i>
          <h3><img class="img-rounded" src="<?= base_url('assets/imgs/education/live-picture-small.png') ?>" style="max-height: 24px;"> เทคนิคการสอนเด็กสมาธิสั้น</h3>
          <p>จากสถาบันวิจัยระบบสารธารณสุข, สถาบันวิจัยเด็กราชนครินทร์..</p>
          <p>
            <ul class="data-lessons">
              <li><i class="glyphicon glyphicon-time"></i> 19/03/2020</li>
              <li><i class="glyphicon glyphicon-user"></i> Administrator</li>
            </ul>
          </p>
        </div>

        <hr class="double">

        <div class="feat">
          <i class="icon-film icon-3x"></i>
          <h3>แนะนำวิธีการดูแลเด็กออทิสติก</h3>
          <p>แม่ผู้มีลูกเป็นเด็ก ออทิสติกคนหนึ่งระบุว่าแม้เป็นเรื่องยาก..</p>
          <p>
            <ul class="data-lessons">
              <li><i class="glyphicon glyphicon-time"></i> 19/03/2020</li>
              <li><i class="glyphicon glyphicon-user"></i> Administrator</li>
            </ul>
          </p>
        </div>

        <!-- <hr class="double">
            
            <div class="feat">
              <i class="icon-film icon-3x"></i>
              <h3>โรคออทิสติกรักษาได้ด้วยกาบำบัด</h3>
              <p>
                การบำบัดออทิสติกในวัยเด็ก..
              </p>
              <p>
                <ul class="data-lessons">
                  <li><i class="glyphicon glyphicon-time"></i> Duration: 3 hours</li>
                  <li><i class="glyphicon glyphicon-user"></i>By : <a href="#">Tayloy Swiff</a></li>
                </ul>
              </p>
            </div>
            
            <hr class="double">
            
            <div class="feat">
              <i class="icon-film icon-3x"></i>
              <h3>ห้องเรียนแรกของเด็กพิเศษ</h3>
              <p>
                ข้อมูลเด็กพิเศษที่ถูกถ่ายทอดผ่านทางสื่อออนไลน์..
              </p>
              <p>
                <ul class="data-lessons">
                  <li><i class="glyphicon glyphicon-time"></i> Duration: 3 hours</li>
                  <li><i class="glyphicon glyphicon-user"></i>By : <a href="#">Tayloy Swiff</a></li>
                </ul>
              </p>
            </div>  -->

      </div>

    </aside>

    <section class="col-md-8">
      <div class="col-right">
        <h2>ความรู้เกี่ยวกับเด็กพิเศษ</h2>
        <p></p>
        <hr>
        <?php $limit = 3;
        $counter = 0 ?>
        <?php foreach ($events_list as $info_key => $info) { ?>
          <?php if ($counter++ >= $limit) break; ?>
          <div class="strip-lessons">
            <div class="row">
              <div class="col-md-2">
                <div class="box-style-one borders"><img class="icon-picture" src="<?=base_url('assets/imgs/education/special-child/'.$info->info_icon) ?>" alt="">
                  <h5><?php if ($info->info_type == 1) echo 'กิจกรรม';
                      else echo 'ข่าวสาร'; ?></h5>
                </div>
              </div>
              <div class="col-md-10">
                <h4><a href="<?=base_url('news/'.$info->info_id)?>"><?= $info->info_title ?></a></h4>
                <p><?= character_limiter(strip_tags($info->info_content), 150) ?></p>
                <ul class="data-lessons">
                  <li><i class="glyphicon glyphicon-time"></i> <?=$this->globals->dateformat_full($info->info_post_date,'thai')?></li>
                  <li><i class="glyphicon glyphicon-user"></i> <?=$info->use_first_name?></li>
                </ul>
              </div>
            </div>
          </div><!-- End Strip course -->
        <?php } ?>
        <p class="text-center"><a href="<?= base_url('news') ?>" class="button_large">ดูทั้งหมด</a></p>

      </div>


    </section>

  </div>
</div><!-- end container-->