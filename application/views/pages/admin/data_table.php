<link href="<?=base_url('vendors/sweetalert2/dist/sweetalert2.min.css')?>" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.bootstrap.min.css" rel="stylesheet" />
<!-- Row -->
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h4 class="panel-title txt-dark"><?=$page_title?></h4>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="table-wrap">
						<div class="table-responsive">
							<table id="foo-child-list" class="table table-hover display pb-30" >
								<thead>
									<tr>
										<th>Name</th>
										<th>Position</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<!-- /Row -->
<script src="<?=base_url('vendors/sweetalert2/dist/sweetalert2.min.js')?>"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			$('#foo-child-list').DataTable( {
				responsive: {
					details: {
						renderer: function ( api, rowIdx, columns ) {
							var data = $.map( columns, function ( col, i ) {
								return col.hidden ?
								'<tr data-dt-row="'+col.rowIndex+'" data-dt-column="'+col.columnIndex+'">'+
								'<td>'+col.title+':'+'</td> '+
								'<td>'+col.data+'</td>'+
								'</tr>' :
								'';
							} ).join('');

							return data ?
							$('<table/>').append( data ) :
							false;
						}
					}
				},
				autoWidth: false,
				processing: true,
				ajax: '<?=base_url($source_url)?>',
				columns: [
					{ data: 'id' },
                    { data: 'first_name' },
				],
				columnDefs: [
				
				],
				initComplete: function( settings, json ) {            

				},
				order: [[ 0, "asc" ]]

			});
		});

	</script>