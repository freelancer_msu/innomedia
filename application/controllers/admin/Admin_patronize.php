<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_patronize extends Core_Acontroller {

	public $title = 'Groups';

	public function __construct()
	{
		parent::__construct();
		$this->config->load('grocery_crud');
	}

	public function index() {
		$crud = new grocery_CRUD();

		$crud->unset_jquery();
		$crud->unset_export();
		$crud->unset_print();

		$crud->set_table('inno_patronize');
		$crud->set_subject('Patronize');
		$crud->columns('patr_user','patr_child','patr_type','patr_patronize_date');

		$crud->display_as('patr_user','ผู้ปกครอง')
			->display_as('patr_patronize_date','วันที่อุปถัมภ์')
			->display_as('patr_child','เด็กพิเศษ')
			->display_as('patr_type','ประเภทการอุปถัมภ์');

		$crud->set_relation('patr_user','inno_users','{use_first_name} {use_last_name}');
		$crud->set_relation('patr_child','inno_child','{chi_first_name} {chi_last_name}');
		$crud->set_relation('patr_type','inno_patronize_type','{patt_name}');

		$crud->add_fields('patr_user','patr_child','patr_type','patr_patronize_date');
		$crud->edit_fields('patr_user','patr_child','patr_type','patr_patronize_date');

		// $crud->set_relation_n_n('patr_relation','inno_patronize','inno_patronize_type','patr_child','patr_type','patt_name','cit_order');
		$output = $crud->render();
		$data['crud_data'] = $output->output;

		$this->script($output->js_files); // load js script from crud

		$this->style($output->css_files); // load css script from crud

		$data['main_menu'] = $this->load_main_menu();
		$this->breadcrumb($this->title,base_url('admin/home'));
		$this->response('home',$data);
  }
}