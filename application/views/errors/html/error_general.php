<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>Philbert I Fast build Admin dashboard for any platform</title>
		<meta name="description" content="Philbert is a Dashboard & Admin Site Responsive Template by hencework." />
		<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Philbert Admin, Philbertadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
		<meta name="author" content="hencework"/>
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
		
		<link href="../vendors/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
		
		<!-- Custom CSS -->
		<link href="../assets/css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->
		
		<div class="wrapper error-page pa-0">
			<header class="sp-header">
				<div class="sp-logo-wrap pull-left">
					<a href="../auth">
						<img class="brand-img mr-10" src="../assets/imgs/logo.png" alt="brand"/>
						<span class="brand-text">Philbert</span>
					</a>
				</div>
				<div class="form-group mb-0 pull-right">
					<a class="inline-block btn btn-success btn-rounded btn-outline nonecase-font" href="../auth">Back to Home</a>
				</div>
				<div class="clearfix"></div>
			</header>
			
			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0 error-bg-img">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<span class="block error-head text-center txt-success mb-10">501</span>
											<span class="text-center nonecase-font mb-20 block error-comment"><?php echo $heading; ?></span>
											<p class="text-center"><?php echo $message; ?></p>
										</div>	
									</div>	
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->	
				</div>
				
			</div>
			<!-- /Main Content -->
		
		</div>
		<!-- /#wrapper -->
		
		<!-- JavaScript -->
		
		<!-- jQuery -->
	    <script src="../vendors/jquery/dist/jquery.min.js"></script>
	    
	    <!-- Bootstrap Core JavaScript -->
	    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	    <script src="../vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
	    
	    <!-- Slimscroll JavaScript -->
	    <script src="../assets/js/jquery.slimscroll.js"></script>
	    
	    <!-- Init JavaScript -->
	    <script src="../assets/js/init.js"></script>
	</body>
</html>
