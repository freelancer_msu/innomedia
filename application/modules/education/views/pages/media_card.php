
<div class="container">

<div class="row">
    <h1 class="col-md-12">สื่อการเรียนรู้</h1>
    <div class="col-md-12">
        <ul class="breadcrumb">
            <li><a href="<?=base_url('')?>">หนัาหลัก</a><span class="divider"></span></li>
            <li><a href="<?=base_url('media')?>">สื่อการเรียนรู้</a><span class="divider"></span></li>
            <li class="active"><?=$result_data[0]->vdo_title;?></li>
        </ul>
    </div>
    
    <!-- =========================Start Col right section ============================= -->
<section class="col-md-12">
<div class="col-right">
    <div class="post">
        <h2><a href="#"><?=$result_data[0]->vdo_title;?></a></h2>
        <!-- <img src="img/blog-1.jpg" alt=""> -->
        <div class="post_info clearfix">
            <div class="post-left">
                <ul>
                    <li><i class="icon-calendar-empty"></i>เมื่อ <span><?=$this->globals->dateformat_full($result_data[0]->vdo_update_date,'thai')?></span></li>
                    <li><i class="icon-user"></i>โดย <?=$result_data[0]->use_first_name?></li>
                    <!-- <li><i class="icon-tags"></i>แท็กส์ <a href="#">Works</a><a href="#">Personal</a></li> -->
                </ul>
            </div>
        </div>
        <div class='video-content'><?php
            if (!empty($result_data[0]->vdo_path)){
                ?>
                <video width="100%" height="540" autoplay>
                    <source src="<?=base_url($result_data[0]->vdo_path)?>" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
                <?php
            }else{
                echo $result_data[0]->vdo_iframe;
            }
        ?>
        </div>
        <br>
        <p><?=$result_data[0]->vdo_desc?></p>
    </div><!-- end post -->
    <hr>
    
</div><!-- end col-right-->
</section><!-- end section-->
</div><!-- end row-->
</div><!-- end container-->