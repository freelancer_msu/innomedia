<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Admin_prefix_name extends Core_Acontroller {

  public $title = 'คำนำหน้า';

  private $_error_retreived = 'มีปัญหาเกี่ยวกับการดึงข้อมูล กรุณาลองใหม่อีกครั้งค่ะ';
    public function __construct()
    {
      parent::__construct();
      $this->load->model('admin/prefix_name_model');
      $this->breadcrumb('หน้าหลัก','admin/home');
    }
  
    public function index()
    {
      $data['main_menu'] = $this->load_main_menu();
      $data['page_title'] = $this->title;
      $data['page_module'] = 'prefix';
      $data['source_url'] = 'admin/prefix/list';
      $this->breadcrumb($this->title,'admin/prefix');
      $this->response('prefix_list',$data);
    }

  public function prefix_list(){
    if ($prefixes = $this->prefix_name_model->_get_all()){
      $answer['data'] = array();
      $idx = 1;
      foreach ($prefixes as $prefix) {
        $row = array($prefix);
        $row['id'] = $idx++;
        $row['prefix'] = $prefix->pre_prefix;
        $row['innitial'] = $prefix->pre_initial;
        $row['status'] = $prefix->pre_is_active;
        array_push($answer['data'], $row);
      }
    }

    echo json_encode($answer);
  }

  public function prefix_view(){
    if ($prefix = $this->prefix_name_model->_get_specific('pre_id',$this->input->post('_id'))){
      $answer['status'] = 1;
      $answer['title'] = "<i class='zmdi zmdi-account mr-10'></i>ข้อมูลคำนำหน้าชื่อ";
      $answer['message'] = $this->load->view('pages/admin/modals/view_prefix',$prefix,TRUE);
      
    } else {
      $answer['status'] = 0;
      $answer['message'] = $this->_error_retreived;
    }
    echo json_encode($answer);
  }

  public function prefix_insert(){
    $data['main_menu'] = $this->load_main_menu();
    $data['page_title'] = 'คำนำหน้าชื่อ';
    $data['source_url'] = 'admin/prefix/list';
    $this->load->model('admin/prefix_name_model');
    $this->breadcrumb($this->title,'admin/prefix');
    $this->breadcrumb('เพิ่มข้อมูลคำนำหน้าชื่อ','admin/prefix/insert');
    $this->response('prefix_insert',$data);
  }

  public function prefix_edit($pre_id){
    $data['main_menu'] = $this->load_main_menu();
    $data['page_title'] = 'คำนำหน้าชื่อ';
    $data['source_url'] = 'admin/prefix/list';
    $data['pref_data'] = $this->prefix_name_model->_get_specific('pre_id',$pre_id);
    
    $this->load->model('admin/prefix_name_model');
    $this->breadcrumb($this->title,'admin/prefix');
    $this->breadcrumb('แก้ไขข้อมูลคำนำหน้าชื่อ','admin/prefix/edit');
    $this->response('prefix_edit',$data);
  }

    public function grocery_crud_fn(){
      $this->config->load('grocery_crud');
      $crud = new grocery_CRUD();
      $crud->unset_jquery();
      $crud->unset_export();
      $crud->unset_print();

      $crud->set_table('inno_prefix');
      $crud->set_subject('คำนำหน้าชื่อ');
      $crud->columns('pre_prefix');
      $crud->display_as('pre_prefix','คำนำหน้าชื่อ');
      $crud->edit_fields('pre_prefix');
      $crud->add_fields('pre_prefix');

      $output = $crud->render();
      $data['crud_data'] = '<div class="row">
                              <div class="col-md-10 col-lg-7">'
                              .$output->output
                            .'</div>
                            </div>';

      $this->script($output->js_files); // load js script from crud

      $this->style($output->css_files); // load css script from crud

      $data['main_menu'] = $this->load_main_menu();
      $this->breadcrumb($this->title,base_url('admin/home'));
      $this->response('home',$data);
    }
}
        
    /* End of file  admin_prefix_name.php */
        
                            