<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
  }

  public function _get($code){
    $this->db->select('*');
    $this->db->from('vdo');
    $this->db->where('vdo_id', $code);
    $this->db->join('users','use_id = vdo_update_by','left');
    

		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		}else {
			return false;
		}
  }

  public function _getAll(){
    $this->db->select('*');
    $this->db->from('vdo');
    $this->db->join('users','use_id = vdo_update_by','left');

    $query = $this->db->get();

    if ($query->num_rows() > 0) {
        return $query->result();
    }else {
        return false;
    }
  }

}