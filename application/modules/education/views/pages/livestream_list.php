<div class="container">

    <div class="row">
        <div class="col-md-12">
            <h1><?=$page_header;?></h1>
            <ul class="breadcrumb">
                <li><a href="index.html">หน้าหลัก</a> <span class="divider">/</span></li>
                <li class="active"><?=$page_header;?></li>
            </ul>
        </div>
        <!-- =========================Start Col left section ============================= -->
        <!-- <aside class="col-md-4 col-sm-4">
            <div class="col-left">
                <h3>Browse Courses</h3>
                <ul class="submenu-col">
                    <li><a href="#" id="active">All Courses</a></li>
                    <li><a href="course-detail.html">Administration (10)</a></li>
                    <li><a href="course-detail.html">Business (08)</a></li>
                    <li><a href="course-detail.html">Communication (05) <img src="img/new.png" alt="New"></a></li>
                    <li><a href="course-detail.html">Computing (08) </a></li>
                    <li><a href="course-detail.html">Counseling (04)</a></li>
                    <li><a href="course-detail.html">Education (06)</a></li>
                    <li><a href="course-detail.html">Engineering (08)</a></li>
                </ul>

                <hr>

                <h5>Upcoming Courses</h5>
                <p>Suspendisse quis risus turpis, ut pharetra arcu. Donec adipiscing, quam non faucibus luctus, mi arcu
                    blandit diam, at faucibus mi ante vel augue.</p>
                <p><a href="#" class=" button_red_small">View Courses</a></p>

            </div>
            <p><img src="img/banner.jpg" alt="Banner" class="img-rounded img-responsive"></p>
        </aside> -->

        <section class="col-md-12 col-sm-12">
            <div class="col-right">
                <div class="main-img">
                    <img src="img/pic-2.jpg" alt="" class="img-responsive">
                    <p class="lead">Tibique dolores adversarium ne vel. At vide errem duo, vis luptatum menandri
                        ullamcorper id. </p>
                </div>

            <!-- View Content -->
                <?php
            foreach ($data_lists as $key => $value) {
                ?>
                <div class="strip-courses">
                    <div class="title-course">
                        <h3><?=$value->live_name;?></h3>
                    </div>
                    <div class="description">
                        <p><?=$value->live_description;?></p>
                        <ul>
                            <li><i class="icon-calendar"></i> <?=$value->live_updated;?></li>
                        </ul>
                        <a href='<?=base_url("livestream/$value->live_id");?>' class="button_medium button-align-2">ดูถ่ายทอดสด</a>
                    </div>
                </div>
                <?php
            }
            ?>

                <hr>

                <div class="text-center">
                    <ul class="pagination">
                        <li><a href="#">Prev</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">Next</a></li>
                    </ul>
                </div><!-- end pagination-->

            </div><!-- end col right-->

        </section>

    </div><!-- end row-->
</div> <!-- end container-->