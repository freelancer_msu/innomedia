<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_users extends Core_Acontroller {

	public $title = 'Users';

	public function __construct()
	{
		parent::__construct();
		$this->config->load('grocery_crud');
	}

	public function index() {
		$crud = new grocery_CRUD();

		$crud->unset_jquery();
		$crud->unset_export();
		$crud->unset_print();

		$crud->set_table('inno_users');
		$crud->where('use_is_parent','0');
		$crud->set_subject('Users');
		$crud->columns('use_username','use_first_name','use_last_name','use_last_login');

		$output = $crud->render();
		$data['crud_data'] = $output->output;

		$this->script($output->js_files); // load js script from crud

		$this->style($output->css_files); // load css script from crud

		$data['main_menu'] = $this->load_main_menu();
		$this->breadcrumb($this->title,base_url('admin/home'));
		$this->response('home',$data);
	}

	public function list_parent() {
		$crud = new grocery_CRUD();

		$crud->unset_jquery();
		$crud->unset_jquery_ui();
		$crud->unset_export();
		$crud->unset_print();

		$crud->set_table('inno_users','inno_users_address');
		$crud->where('use_is_parent','1');
		$crud->set_subject('ผู้ปกครอง');
		$crud->columns('use_username','use_first_name','use_last_name','use_last_login');
		$crud->display_as('use_username','รหัสเข้าใช้งาน')
			->display_as('use_first_name','ชื่อจริง')
			->display_as('use_last_name','นามสกุล')
			->display_as('use_last_login','ลงชื่อเข้าใช้ล่าสุด')
			->display_as('uadd_district','ตำบล')
			->display_as('uadd_amphure','อำเภอ')
			->display_as('uadd_province','จังหวัด')
			->display_as('uadd_postcode','รหัสไปรษณีย์')
			->display_as('use_password','รหัสผ่าน');

		$crud->add_fields('use_username','use_password','use_first_name', 'use_last_name', 'uadd_district','uadd_postcode', 'uadd_amphure','uadd_province');
		$crud->edit_fields('use_first_name', 'use_last_name', 'uadd_district','uadd_postcode', 'uadd_amphure','uadd_province');

		$crud->required_fields('use_username', 'use_password','use_first_name');

		$crud->field_type('uadd_amphure', 'readonly');
		$crud->field_type('uadd_province', 'readonly');

		$crud->field_type('use_password', 'password');

		$crud->callback_before_insert('use_password',array($this,'password_callback'));

		// $crud->set_relation('uadd_district','uadd_user_id','{sch_name}');
		// $crud->set_relation('uadd_amphure','inno_school','{sch_name}');
		// $crud->set_relation('uadd_province','inno_school','{sch_name}');

		$output = $crud->render();
		$data['crud_data'] = $output->output;

		$this->script($output->js_files); // load js script from crud

		$this->style($output->css_files); // load css script from crud

		$data['main_menu'] = $this->load_main_menu();
		$this->breadcrumb($this->title,base_url('admin/home'));
			$this->response('home',$data);
	}

	public function password_callback($post_array){
		$this->load->model('auth/ion_auth_model');
		$post_array['use_password'] = $this->ion_auth_model->hash_password($post_array['use_password'],$post_array['use_salt']);
		return $post_array;
	}

	function set_password_input_to_empty() {
	    return "<input type='password' name='password' value='' />";
	}

	public function district_callback($value, $primary_key){

	}
}