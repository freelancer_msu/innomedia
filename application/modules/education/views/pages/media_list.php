<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>สื่อการเรียนรู้</h1>
            <ul class="breadcrumb">
                <li><a href="<?= base_url() ?>">หนัาหลัก</a> <span class="divider"></span></li>
                <li class="active">สื่อการเรียนรู้</li>
            </ul>
        </div>
        <!-- =========================Start Col left section ============================= -->
        <aside class="col-md-4 col-sm-4">
            <div class="col-left">
                <h3>ค้นหาสื่อการเรียนรู้</h3>
                <div class="form-group">
                    <form class="form-search form-inline">
                        <input id="speechText" type="text" class="input-medium form-control">
                        <button type="submit" class="button_medium" style="position:relative; top:2px;">ค้นหา</button>
                        <a class="button_medium btn_speech">พูด</a>
                    </form>
                </div>
                <hr>
                <h3>หมวดหมู่ที่น่าสนใจ</h3>
                <div class="widget">
                    <ul class="latest_news">
                        <li><i class="icon-bookmark-empty"></i>
                            <div>
                                <a href="#">สำหรับเด็กออทิสติก</a>
                            </div>
                        </li>
                        <li><i class="icon-bookmark-empty"></i>
                            <div>
                                <a href="#">สอนการเลี้ยงดูเด็กพิเศษ</a>
                            </div>
                        </li>
                        <li><i class="icon-bookmark-empty"></i>
                            <div>
                                <a href="#">นิทานเสริมพัฒนาการ</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

        </aside>

        <!-- =========================Start Col right section ============================= -->
        <section class="col-md-8 col-sm-8">
            <div class="col-right">
                <div class="media-strip">
                    <ul>
                        <?php
                        foreach($list_data as $key => $value){
                            ?>
                        <li class="row">
                            <div class="media-icon">
                                <i class="icon-film icon-3x"></i>
                            </div>
                            <h5>
                                <a href="<?= base_url('media/'.$value->vdo_id) ?>"><?=$value->vdo_title?></a>
                            </h5>
                            <span>
                                <ul class="data-lessons">
                                    <li><i class="glyphicon glyphicon-time"></i> <?=$this->globals->dateformat_full($value->vdo_update_date,'thai')?></li>
                                    <li><i class="glyphicon glyphicon-user"></i> <?=$value->use_first_name?></li>
                                </ul>
                            </span>
                            <p><?= character_limiter($value->vdo_desc, 160); ?></p>
                        </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>

                <hr>
                <div class="text-center">
                    <ul class="pagination">
                        <li><a href="#">Prev</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">Next</a></li>
                    </ul>
                </div><!-- end pagination-->

            </div>

        </section>

    </div><!-- end row-->
</div> <!-- end container-->