// SCROLL TO TOP ===============================================================================
$(function () {
	$(window).scroll(function () {
		if ($(this).scrollTop() != 0) {
			$('#toTop').fadeIn();
		} else {
			$('#toTop').fadeOut();
		}
	});

	$('#toTop').click(function () {
		$('body,html').animate({ scrollTop: 0 }, 500);
	});

	// <!-- Toggle -->			
	$('.togglehandle').click(function () {
		$(this).toggleClass('active')
		$(this).next('.toggledata').slideToggle()
	});

	// alert close 
	$('.clostalert').click(function () {
		$(this).parent('.alert').fadeOut()
	});


	// <!-- Tooltip -->	
	$('.tooltip-test').tooltip();


	// <!-- Accrodian -->	
	var $acdata = $('.accrodian-data'),
		$acclick = $('.accrodian-trigger');

	$acdata.hide();
	$acclick.first().addClass('active').next().show();

	$acclick.on('click', function (e) {
		if ($(this).next().is(':hidden')) {
			$acclick.removeClass('active').next().slideUp(300);
			$(this).toggleClass('active').next().slideDown(300);
		}
		e.preventDefault();
	});

	$(".news-strip ul li").click(function () {
		window.location = $(this).find("a").attr("href");
		return false;
	});

	// Speech to text and text to speech
	var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition

	var recognition = new SpeechRecognition();

	recognition.lang = 'th-TH';
	recognition.pitch = 1;
	recognition.rate = 1;
	recognition.interimResults = false;

	recognition.onresult = function (event) {
		var btnspeech = $(".btn_speech")[0];
		var saidText = "";
		for (var i = event.resultIndex; i < event.results.length; i++) {
			if (event.results[i].isFinal) {
				saidText = event.results[i][0].transcript;
			} else {
				saidText += event.results[i][0].transcript;
			}
		}
		// Update Textbox value
		document.getElementById('speechText').value = saidText;
		btnspeech.text = 'พูด';

		if(saidText != ''){
			utterance('กำลังค้นหา '+saidText);
		}
	}

	function utterance(pText = '') {
		var utterance = new SpeechSynthesisUtterance(pText);
		utterance.lang = 'th-TH';
		utterance.pitch = 1;
		utterance.rate = 1;
		speechSynthesis.speak(utterance);
	}


	$(".btn_speech").click(function () {
		if (this.text == 'กำลังฟัง'){
			this.innerHTML = 'พูด';
			recognition.abort();
		}else{
			this.innerHTML = 'กำลังฟัง';
			recognition.start();
		}
	});
});

// HOVER IMAGE MAGNIFY V.1.4 ===============================================================================
$(document).ready(function () {
	"use strict";
	//Set opacity on each span to 0%
	$(".photo_icon").css({ 'opacity': '0' });

	$('.picture a').hover(
		function () {
			$(this).find('.photo_icon').stop().fadeTo(800, 1);
		},
		function () {
			$(this).find('.photo_icon').stop().fadeTo(800, 0);
		}
	),
		$(".video_icon_youtube").css({ 'opacity': '0' });

	$('.picture a').hover(
		function () {
			$(this).find('.video_icon_youtube').stop().fadeTo(800, 1);
		},
		function () {
			$(this).find('.video_icon_youtube').stop().fadeTo(800, 0);
		}
	),

		$(".video_icon_vimeo").css({ 'opacity': '0' });

	$('.picture a').hover(
		function () {
			$(this).find('.video_icon_vimeo').stop().fadeTo(800, 1);
		},
		function () {
			$(this).find('.video_icon_vimeo').stop().fadeTo(800, 0);
		}
	)
});	