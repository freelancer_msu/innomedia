  <footer>
  <div class="container">
  	<div class="row">
    	<div class="col-md-4" id="brand-footer">
   	    	<p><a href="http://www.thaimediafund.or.th/"><img class="img-fluid" src="<?=base_url('assets/imgs/education/innomedia-logo-wo.png')?>" alt="" style="max-height: 200px;"></a></p>
            <p>Copyright © 2020</p>
        </div>
        <div class="col-md-8" id="contacts-footer">
            <div class="col-md-6" id="">
            	<h4>ติดต่อ</h4>
                <ul>
                	<li><i class="glyphicon glyphicon-bookmark"></i> Intellect Laboratory, Mahasarakham University</li>
                    <li><i class="glyphicon glyphicon-envelope"></i> Email: <a href="#">intlab@intlab-msu.com</a></li>
                </ul>
            </div>
            <div class="col-md-6" id="quick-links">
                <h4>ลิ้งค์ที่เกี่ยวข้อง</h4>
                <ul>
                    <li><a href="http://special.obec.go.th/" >สำนักบริหารงานการศึกษาพิเศษ</a></li>
                    <li><a href="http://www.thaimediafund.or.th/" >กองทุนพัฒนาสื่อปลอดภัยและสร้างสรรค์</a></li>
                    <li><a href="http://special.obec.go.th/belong3.php" >รายชื่อศูนย์การศึกษาพิเศษ</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-8 text-right" id="">
            <hr>
            <h4><a href="#">เข้าดูการกระจายตัวของเด็กพิเศษ</a> | <a href="<?=base_url('admin/home')?>">สำหรับผู้ดูแลระบบ</a></h4>
        </div>
    </div>
  </div>
  </footer><!-- End footer-->