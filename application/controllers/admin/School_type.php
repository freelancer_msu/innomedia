<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class School_type extends Core_Acontroller {

    public function __construct()
    {
      parent::__construct();
      $this->load->model('admin/school_type_model');

      $this->config->load('grocery_crud');
      $this->config->set_item('grocery_crud_dialog_forms',true);
      $this->config->set_item('grocery_crud_dialog_color','blue');

    }
  
    public function index()
    {
      $crud = new grocery_CRUD();
      $crud->unset_jquery();
      $crud->unset_export();
      $crud->unset_print();

      $crud->set_table('inno_school_type');
      $crud->set_subject('ประเภทโรงเรียน');
      $crud->columns('st_code','st_description','st_last_modify_by');
      $crud->display_as('st_code','รหัส')
          ->display_as('st_description','รายละเอียด')
          ->display_as('st_last_modify','แก้ไขล่าสุดเมื่อ')
          ->display_as('st_last_modify_by','แก้ไขล่าสุดโดย');
      $crud->set_relation('st_last_modify_by','inno_users','{use_username}');

      $crud->edit_fields('st_description');
      $crud->add_fields('st_description');

      $output = $crud->render();
      $data['crud_data'] = '<div class="row">
                            <div class="col-md-12">'
                              .$output->output
                            .'</div>
                            </div>';

      $this->script($output->js_files); // load js script from crud

      $this->style($output->css_files); // load css script from crud

      $data['main_menu'] = $this->load_main_menu();
      $this->breadcrumb($this->title,base_url('admin/home'));
      $this->response('home',$data);
    }
  
}
        
    /* End of file  school_type.php */
        
                            