
<div class="container">

<div class="row">
    <h1 class="col-md-12">ข่าวสาร &amp; กิจกรรม</h1>
    <div class="col-md-12">
        <ul class="breadcrumb">
            <li><a href="<?=base_url('')?>">หนัาหลัก</a><span class="divider"></span></li>
            <li><a href="<?=base_url('events')?>">ข่าวสาร &amp; กิจกรรม</a><span class="divider"></span></li>
            <li class="active"><?=$result_data->info_title?></li>
        </ul>
    </div>
    
    <!-- =========================Start Col right section ============================= -->
<section class="col-md-12">
<div class="col-right">
    <div class="post">
        <h2><a href="blog_post.html"><?=$result_data->info_title?></a></h2>
        <img class='img-fluid img-cover' src="<?=base_url($result_data->info_sample_pic)?>" alt="">
        <div class="post_info clearfix">
            <div class="post-left">
                <ul>
                    <li><i class="icon-calendar-empty"></i>เมื่อ <span><?=$this->globals->dateformat_full($result_data->info_post_date,'thai')?></span></li>
                    <li><i class="icon-user"></i>โดย <a href="#"><?=$result_data->use_first_name.' '.$result_data->use_last_name?></a></li>
                    <!-- <li><i class="icon-tags"></i>แท็กส์ <a href="#">Works</a><a href="#">Personal</a></li> -->
                </ul>
            </div>
        </div>
        <p><?=$result_data->info_content?></p>
    </div><!-- end post -->
    <hr>
    
</div><!-- end col-right-->
</section><!-- end section-->
</div><!-- end row-->
</div><!-- end container-->