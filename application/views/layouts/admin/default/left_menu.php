<div class="fixed-sidebar-left">
	<ul class="nav navbar-nav side-nav nicescroll-bar">
		<?php 
		?>
		<?php
		function travel_main_menu($main_menu = false,$permission = false)
		{
			$display_groupname_array = array('เมนูหลัก','ข้อมูลหลักของระบบ','ตั้งค่า');
			$display_groupname_running = 0;
			$display_groupname_toggle = false; 
			foreach ($main_menu as $mkey => $menu) {
				if($menu->men_is_serperater == 0)
					if($permission->{$menu->men_id}->read != 1) 
						continue;

					if (!$display_groupname_toggle) { ?>
						<li class="navigation-header">
							<span><?=$display_groupname_array[$display_groupname_running++]?></span>
							<i class="zmdi zmdi-more"></i>
						</li>
						<?php 
						$display_groupname_toggle = true;
					}
					if ($menu->men_is_serperater==1) {
						?> 
						<li>
							<hr class="light-grey-hr mb-10" />
						</li>
						<?php
						if($display_groupname_toggle){ ?>
							<li class="navigation-header">
								<span><?=$display_groupname_array[$display_groupname_running++]?></span>
								<i class="zmdi zmdi-more"></i>
							</li>
						<?php }
						continue;
					}
					if ($menu->child != false) {
						?>
						<li>
							<a href="javascript:void(0);" data-toggle="collapse" data-target="#for_<?= $menu->men_name ?>">
								<div class="pull-left"><i class="<?= $menu->men_icon ?> mr-20"></i><span class="right-nav-text"><?= $menu->men_name ?></span></div>
								<div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div>
								<div class="clearfix"></div>
							</a>
						</li>
						<?php
						travel_level_2_menu($menu->child, $menu->men_name, $permission);
					} else {
						?>
						<li>
							<a href="<?= base_url($menu->men_path) ?>">
								<div class="pull-left">
									<i class="<?= $menu->men_icon ?> mr-20"></i>
									<span class="right-nav-text"><?= $menu->men_name ?></span>
								</div>
								<div class="clearfix"></div>
							</a>
						</li>
						<?php
					}
				}
			}

			function travel_level_2_menu($lv2child = false, $for_tag_id = '',$permission=false)
			{
				?>
				<li>
					<ul id="for_<?= $for_tag_id ?>" class="collapse collapse-level-1">
						<?php
						foreach ($lv2child as $lv2key => $lv2menu) {
							if($lv2menu->men_is_serperater == 0)
								if($permission->{$lv2menu->men_id}->read != 1) 
									continue;

								if ($lv2menu->child != false) {
									?>
									<li>
										<a href="javascript:void(0);" data-toggle="collapse" data-target="#for_<?= $lv2menu->men_name ?>"><?= $lv2menu->men_name ?><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div>
											<div class="clearfix"></div>
										</a>

										<?php
										travel_level_3_menu($lv2menu->child, $lv2menu->men_name, $permission);
										?>

									</li>
									<?php
								} else {
									?>
									<li>
										<a href="<?= base_url($lv2menu->men_path) ?>"><?= $lv2menu->men_name ?></a>
									</li>
									<?php
								}
							}
							?>
						</ul>
					</li>
					<?php
				}

				function travel_level_3_menu($lv3child = false, $for_tag_id = '',$permission=false)
				{

					?>
					<li>
						<ul id="for_<?= $for_tag_id ?>" class="collapse collapse-level-2">
							<?php
							foreach ($lv3child as $lv3key => $lv3menu) {
								//echo $lv3menu->men_id."xxxxxx".json_encode($permission->{$lv3menu->men_id})."xxxxxx";
								if($lv3menu->men_is_serperater == 0)
									if($permission->{$lv3menu->men_id}->read != 1) 
										continue;
									?>
									<li>
										<a href="<?= base_url($lv3menu->men_path) ?>"><?= $lv3menu->men_name ?></a>
									</li>
									<?php
								}
								?>
							</ul>
						</li>
						<?php
					}
					?>

					<?php travel_main_menu($main_menu,$permission); ?>

	</ul>
</div>