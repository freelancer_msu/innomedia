<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Livestream_model extends CI_Model {

  private $TABLE = 'live';

  public function __construct()
  {
    parent::__construct();
  }

  public function _get($code){
    $this->db->select('*');
    $this->db->from($this->TABLE);
    $this->db->where('live_id', $code);
    

		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		}else {
			return false;
		}
  }

  public function _getAll(){
    $this->db->select('*');
    $this->db->from($this->TABLE);
    $this->db->where('live_status',1);

    $query = $this->db->get();

    if ($query->num_rows() > 0) {
        return $query->result();
    }else {
        return false;
    }
  }

}