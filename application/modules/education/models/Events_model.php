<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events_model extends Core_Model {

  private $_table = 'inforandevent';
  private $_type = 1;

  public function __construct()
  {
    parent::__construct();
  }

  public function _get_all(){
    $this->db->select('*');
    $this->db->from($this->_table);
    $this->db->where('info_type',$this->_type);

    $query = $this->db->get();

    if ($query->num_rows() > 0) {
        return $query->result();
    }else {
        return false;
    }
  }

  public function _get_specified($id = 1){
    $this->db->select('*');
    $this->db->from($this->_table);
    $this->db->join('users','use_id = info_post_by');
    $this->db->where('info_type',$this->_type);
    $this->db->where('info_id',$id);

    $query = $this->db->get();

    if ($query->num_rows() > 0) {
        return $query->row();
    }else {
        return false;
    }
  }

}