<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_province extends Core_Acontroller
{
  public $title = 'จังหวัด';

  public function __construct()
  {
    parent::__construct();
    $this->load->model('admin/province_model');
    $this->breadcrumb('หน้าหลัก','admin/home');
  }

  public function index()
  {
    $data['main_menu'] = $this->load_main_menu();
    $data['page_title'] = $this->title;
    $data['page_module'] = 'province';
    $data['source_url'] = 'admin/province/list';
    $this->breadcrumb($this->title,'admin/province');
    $this->response('province_list',$data);
  }

  public function province_list(){
    if ($provinces = $this->province_model->_get_all()){
      $answer['data'] = array();
      $idx = 1;
      foreach ($provinces as $province) {
        $row = array($province);
        $row['id'] = $idx++;
        $row['code'] = $province->pro_code;
        $row['name_th'] = $province->pro_name_th;
        $row['name_en'] = $province->pro_name_en;
        $row['init'] = $province->pro_region;
        $row['status'] = $province->pro_active;
        array_push($answer['data'], $row);
      }
    }
    echo json_encode($answer);
  }

  public function province_view(){
    if ($province = $this->province_model->_get_specific('pro_id',$this->input->post('_id'))){
      $answer['status'] = 1;
      $answer['title'] = "<i class='zmdi zmdi-account mr-10'></i>ข้อมูลจังหวัด";
      $answer['message'] = $this->load->view('pages/admin/modals/view_province',$province,TRUE);
      
    } else {
      $answer['status'] = 0;
      $answer['message'] = $this->_error_retreived;
    }
    echo json_encode($answer);
  }

  public function province_insert(){
    $data['main_menu'] = $this->load_main_menu();
    $data['page_title'] = 'จังหวัด';
    $data['source_url'] = 'admin/province/list';
    $this->load->model('admin/province_model');
    $this->breadcrumb($this->title,'admin/province');
    $this->breadcrumb('เพิ่มข้อมูลจังหวัด','admin/province/insert');
    $this->response('province_insert',$data);
  }

  public function province_edit($pro_id){
    $data['main_menu'] = $this->load_main_menu();
    $data['page_title'] = 'จังหวัด';
    $data['source_url'] = 'admin/province/list';
    $data['prov_data'] = $this->province_model->_get_specific('pro_id',$pro_id);
    
    $this->load->model('admin/province_model');
    $this->breadcrumb($this->title,'admin/province');
    $this->breadcrumb('แก้ไขข้อมูลข้อมูลจังหวัด','admin/province/edit');
    $this->response('province_edit',$data);
  }

public function grocery_crud_fn(){
  $this->config->load('grocery_crud');
  $crud = new grocery_CRUD();
  $crud->unset_jquery();
  $crud->unset_export();
  $crud->unset_print();
  $crud->unset_clone();

  $crud->set_table('inno_province');
  $crud->set_subject('ข้อมูลจังหวัด');
  $crud->columns('pro_code','pro_name_th','pro_name_en','pro_active');
  $crud->display_as('pro_code',' รหัสจังหวัด')
  ->display_as('pro_name_th','ชื่อ (ไทย)')
  ->display_as('pro_name_en','ชื่อ (Eng)')
  ->display_as('pro_active','สถานะ');
  $crud->fields('pro_code','pro_name_th','pro_name_en','pro_active');


  $output = $crud->render();
  $data['crud_data'] = $output->output;

      $this->script($output->js_files); // load js script from crud

      $this->style($output->css_files); // load css script from crud

      $data['main_menu'] = $this->load_main_menu();
      $this->breadcrumb($this->title,base_url('admin/home'));
      $this->response('home',$data);
    }

  }
