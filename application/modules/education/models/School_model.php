<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School_model extends Core_Model {

  private $_table = 'school';

  public function __construct()
  {
    parent::__construct();
  }

  public function _GetAll()
  {
    $this->db->select('*');
    $this->db->from($this->_table);
    $this->db->join('school_type', 'st_code = sch_school_type', 'left');

    $query = $this->db->get();

    if ($query->num_rows() > 0) {
        return $query->result();
    }else {
        return false;
    }
  }
}