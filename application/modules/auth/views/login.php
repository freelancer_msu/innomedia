<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Innovation Medias System</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content=""/>
    
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    
    <!-- vector map CSS -->
    <link href="<?=base_url('vendors/jasny-bootstrap/dist/css/jasny-bootstrap.min.css')?>" rel="stylesheet" type="text/css"/>
    
    <!-- Custom CSS -->
    <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet" type="text/css">
  </head>
  <body>
    <!--Preloader-->
    <div class="preloader-it">
      <div class="la-anim-1"></div>
    </div>
    <!--/Preloader-->
    
    <div class="wrapper pa-0">
      <header class="sp-header">
        <div class="sp-logo-wrap pull-left">
          <a href="index.html">
            <img class="brand-img mr-10" src="<?=base_url('assets/imgs/logo.png')?>" alt="brand"/>
            <span class="brand-text">Innovation Medias</span>
          </a>
        </div>
        <!-- <div class="form-group mb-0 pull-right">
          <span class="inline-block pr-10">Don't have an account?</span>
          <a class="inline-block btn btn-info btn-success btn-rounded btn-outline" href="signup.html">Sign Up</a>
        </div> -->
        <!-- <div class="clearfix"></div> -->
      </header>
      
      <!-- Main Content -->
      <div class="page-wrapper pa-0 ma-0 auth-page">
        <div class="container-fluid">
          <!-- Row -->
          <div class="table-struct full-width full-height">
            <div class="table-cell vertical-align-middle auth-form-wrap">
              <div class="auth-form  ml-auto mr-auto no-float">
                <div class="row">
                  <div class="col-sm-12 col-xs-12">
                    <div class="mb-30">
                      <h3 class="text-center txt-dark mb-10"><?=lang('login_heading')?></h3>
                      <h6 class="text-center nonecase-font txt-grey"><?=lang('login_subheading')?></h6>
                    </div>  
                    <div class="form-wrap">
                      <div id="infoMessage"><?php echo $message;?></div>
                      <form action="<?=base_url('auth/login')?>" method="post">
                        <div class="form-group">
                          <label class="control-label mb-10" for="identity"><?=lang('login_identity_label')?></label>
                          <input type="text" class="form-control" required="" id="identity" placeholder="ชื่อผู้ใช้งาน" name="identity">
                        </div>
                        <div class="form-group">
                          <label class="pull-left control-label mb-10" for="password"><?=lang('login_password_label', 'password')?></label>
                          <a class="capitalize-font txt-primary block mb-10 pull-right font-12" href="<?=base_url('auth/forgot_password')?>"><?=lang('login_forgot_password')?></a>
                          <div class="clearfix"></div>
                          <input type="password" class="form-control" required="" id="password" placeholder="รหัสผ่าน" name="password">
                        </div>
                        
                        <div class="form-group">
                          <div class="checkbox checkbox-primary pr-10 pull-left">
                            <input id="remember" type="checkbox">
                            <label for="remember"> <?=lang('login_remember_label')?></label>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="form-group text-center">
                          <button type="submit" class="btn btn-info btn-success btn-rounded"><?=lang('login_submit_btn')?></button>
                        </div>
                      </form>
                    </div>
                  </div>  
                </div>
              </div>
            </div>
          </div>
          <!-- /Row --> 
        </div>
        
      </div>
      <!-- /Main Content -->
    
    </div>
    <!-- /#wrapper -->
    
    <!-- JavaScript -->
    
    <!-- jQuery -->
    <script src="<?=base_url('vendors/jquery/dist/jquery.min.js')?>"></script>
    
    <!-- Bootstrap Core JavaScript -->
    <script src="<?=base_url('vendors/bootstrap/dist/js/bootstrap.min.js')?>"></script>
    <script src="<?=base_url('vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js')?>"></script>
    
    <!-- Slimscroll JavaScript -->
    <script src="<?=base_url('assets/js/jquery.slimscroll.js')?>"></script>
    
    <!-- Init JavaScript -->
    <script src="<?=base_url('assets/js/init.js')?>"></script>
  </body>
</html>
