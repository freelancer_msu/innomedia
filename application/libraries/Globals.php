<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Globals  {
    protected $my_ci;

    // Pass array as an argument to constructor function
    public function __construct($config = array()) {
        // Create associative array from the passed array
        $data = null;
        foreach ($config as $key => $value) {
            $data[$key] = $value;
        }
        // Make instance of CodeIgniter to use its resources
        $this->my_ci = & get_instance();
        // Load data into CodeIgniter
        $this->my_ci->load->vars($data);
    }

    function month_name ($num,$language){
        if($language=='english'){
            return $this->month_en($num);
        }else if($language=='thai'){
            return  $this->month_th($num);
        }else {
            return false;
        }
    }

    function month_name_mini ($num,$language){
        if($language=='english'){
            return $this->month_mini_en($num);
        }else if($language=='thai'){
            return  $this->month_mini_th($num);
        }else {
            return false;
        }
    }

    function month_mini_en($number){

        if ($number == 1) return "Jan";
        if ($number == 2) return "Feb";
        if ($number == 3) return "Mar";
        if ($number == 4) return "Apr";
        if ($number == 5) return "May";
        if ($number == 6) return "Jun";
        if ($number == 7) return "Jul";
        if ($number == 8) return "Aug";
        if ($number == 9) return "Sep";
        if ($number == 10) return "Oct";
        if ($number == 11) return "Nov";
        if ($number == 12) return "Dec";

        return false;
    }

    function month_en($number){

        if ($number == 1) return "January";
        if ($number == 2) return "Febuary";
        if ($number == 3) return "March";
        if ($number == 4) return "April";
        if ($number == 5) return "May";
        if ($number == 6) return "Jun";
        if ($number == 7) return "July";
        if ($number == 8) return "August";
        if ($number == 9) return "September";
        if ($number == 10) return "October";
        if ($number == 11) return "November";
        if ($number == 12) return "December";

        return false;
    }

    function month_th($number){

        if ($number == 1) return "มกราคม";
        if ($number == 2) return "กุมภาพันธ์";
        if ($number == 3) return "มีนาคม";
        if ($number == 4) return "เมษายน";
        if ($number == 5) return "พฤษภาคม";
        if ($number == 6) return "มิถุนายน";
        if ($number == 7) return "กรกฏาคม";
        if ($number == 8) return "สิงหาคม";
        if ($number == 9) return "กันยายน";
        if ($number == 10) return "ตุลาคม";
        if ($number == 11) return "พฤศจิกายน";
        if ($number == 12) return "ธันวาคม";

        return false;
    }

    function month_mini_th($number){

        if ($number == 1) return "ม.ค.";
        if ($number == 2) return "ก.พ.";
        if ($number == 3) return "มี.ค.";
        if ($number == 4) return "เม.ย.";
        if ($number == 5) return "พ.ค.";
        if ($number == 6) return "มิ.ย.";
        if ($number == 7) return "ก.ค.";
        if ($number == 8) return "ส.ค.";
        if ($number == 9) return "ก.ย.";
        if ($number == 10) return "ต.ค.";
        if ($number == 11) return "พ.ย.";
        if ($number == 12) return "ธ.ค.";

        return false;
    }

    function dateformat_mini ($date_in,$language){
        $datetime = explode(" ",$date_in);
        $date = explode("-", $datetime[0]);
        if($language=='english'){
            return $date[2]." ".$this->month_mini_en($date[1])." ".($date[0]);
        }else if($language=='thai'){
            return  $date[2]." ".$this->month_mini_th($date[1])." ".($date[0]+543);
        }else {
            return false;
        }
    }

    function dateformat_full ($date_in,$language){
        $datetime = explode(" ",$date_in);
        $date = explode("-", $datetime[0]);
        if($language=='english'){
            return $date[2]." ".$this->month_en($date[1])." ".($date[0]);
        }else if($language=='thai'){
            return  $date[2]." ".$this->month_th($date[1])." ".($date[0]+543);
        }else {
            return false;
        }
    }

    function get_age($birthday) {
        $then = strtotime($birthday);
        return(floor((time()-$then)/31556926));
    }

    public function postCURL($_url, $_param){

        $postData = '';
        //create name value pairs seperated by &
        foreach($_param as $k => $v) 
        { 
          $postData .= $k . '='.$v.'&'; 
        }
        rtrim($postData, '&');
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, strlen($postData));
        //echo "<br><br>curl url : ".$_url;
        // echo "<br><br>curl postdata : ".$postData;
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    

        $output=curl_exec($ch);
        //echo "<br><br> output : ".$output;
        if($output){
            curl_close($ch);
            return $output;
        }else {
            $error = curl_error($ch). '(' .curl_errno($ch). ')';
            curl_close($ch);
            return $error;
        }
        
    }

    public function write_log($id,$action) {
        //return $this->my_ci->authentication_model->_add_log($id,$action);
    }

    function generate_random_string($length) {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function datetimeformat_mini ($date_in,$language){
        $datetime = explode(" ",$date_in);
        $date = explode("-", $datetime[0]);
        if($language=='english'){
            return $date[2]." ".$this->month_mini_en($date[1])." ".($date[0])." ".$datetime[1];
        }else if($language=='thai'){
            return  $date[2]." ".$this->month_mini_th($date[1])." ".($date[0]+543)." ".$datetime[1];
        }else {
            return false;
        }
    }

}