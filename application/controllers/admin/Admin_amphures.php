<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_amphures extends Core_Acontroller
{
  public $title = 'อำเภอ';
  public function __construct()
  {
    parent::__construct();
    $this->load->model('admin/amphures_model');
    $this->breadcrumb('หน้าหลัก','admin/home');
  }

  public function index()
  {
    $data['main_menu'] = $this->load_main_menu();
    $data['page_title'] = $this->title;
    $data['page_module'] = 'amphures';
    $data['source_url'] = 'admin/amphures/list';
    $this->breadcrumb($this->title,'admin/amphures');
    $this->response('amphure_list',$data);
  }

  public function amphures_list(){
    if ($amphures = $this->amphures_model->_get_all()){
      $answer['data'] = array();
      $idx = 1;
      foreach ($amphures as $amphure) {
        $row = array($amphure);
        $row['id'] = $idx++;
        $row['code'] = $amphure->amphure_code;
        $row['name_th'] = $amphure->amphure_name_th;
        $row['name_en'] = $amphure->amphure_name_en;
        $row['init'] = $amphure->pro_name_th;
        $row['status'] = $amphure->amphure_active;
        array_push($answer['data'], $row);
      }
    }
    echo json_encode($answer);
  }

  public function amphures_insert(){
    $data['main_menu'] = $this->load_main_menu();
    $data['page_title'] = 'อำเภอ';
    $data['source_url'] = 'admin/amphures/list';
    $data['prov_data'] = $this->amphures_model->_get_province();

    $this->load->model('admin/amphures_model');
    $this->breadcrumb($this->title,'admin/amphures');
    $this->breadcrumb('เพิ่มข้อมูลอำเภอ','admin/amphures/insert');
    $this->response('amphure_insert',$data);
  }

  public function amphures_edit($pro_id){
    $data['main_menu'] = $this->load_main_menu();
    $data['page_title'] = 'อำเภอ';
    $data['source_url'] = 'admin/amphures/list';
    $data['amphure_data'] = $this->amphures_model->_get_specific('amphure_id',$pro_id);
    
    $this->load->model('admin/amphures_model');
    $this->breadcrumb($this->title,'admin/amphures');
    $this->breadcrumb('แก้ไขข้อมูลอำเภอ','admin/amphures/edit');
    $this->response('amphure_edit',$data);
  }

  public function amphures_view(){
    if ($amphure = $this->amphures_model->_get_specific('amphure_id',$this->input->post('_id'))){
      $answer['status'] = 1;
      $answer['title'] = "<i class='zmdi zmdi-account mr-10'></i>ข้อมูลคำนำหน้าชื่อ";
      $answer['message'] = $this->load->view('pages/admin/modals/view_amphure',$amphure,TRUE);
      
    } else {
      $answer['status'] = 0;
      $answer['message'] = $this->_error_retreived;
    }
    echo json_encode($answer);
  }

  public function grocery_crud_fn(){
    $this->config->load('grocery_crud');
    $crud = new grocery_CRUD();
    $crud->unset_jquery();
    $crud->unset_export();
    $crud->unset_print();
    $crud->unset_clone();

    $crud->set_table('inno_amphures');
    $crud->set_subject('ข้อมูลอำเภอ');
    $crud->columns('amphure_code','amphure_name_th','amphure_name_en','amphure_province','amphure_active');
    $crud->display_as('amphure_code',' รหัสอำเภอ')
    ->display_as('amphure_name_th','ชื่อ (ไทย)')
    ->display_as('amphure_name_en','ชื่อ (Eng)')
    ->display_as('amphure_province','จังหวัด')
    ->display_as('amphure_active','สถานะ');
    $crud->unset_columns('amphure_region');
    $crud->fields('amphure_code','amphure_name_th','amphure_name_en','amphure_province','amphure_active');

    $crud->set_relation('amphure_province','inno_province','{pro_name_th}');


    $output = $crud->render();
    $data['crud_data'] = $output->output;

      $this->script($output->js_files); // load js script from crud

      $this->style($output->css_files); // load css script from crud

      $data['main_menu'] = $this->load_main_menu();
      $this->breadcrumb($this->title,base_url('admin/home'));
      $this->response('home',$data);
    }

  }
