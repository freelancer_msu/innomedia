<?php

defined('BASEPATH') or exit('No direct script access allowed');

class School extends Core_Acontroller
{
    public function __construct()
    {
        parent::__construct();
        $this->config->load('grocery_crud');
      $this->config->set_item('grocery_crud_dialog_forms',true);
    }

    public function index()
    {
      $crud = new grocery_CRUD();

      $crud->unset_jquery();
      $crud->unset_export();
      $crud->unset_print();

      $crud->set_table('inno_school');
      $crud->set_subject('โรงเรียน');
      $crud->columns('sch_code','sch_name','sch_school_type','sch_last_modify_by','sch_last_modify');
      $crud->display_as('sch_code','รหัส')
          ->display_as('sch_name','ชื่อโรงเรียน')
          ->display_as('sch_school_type','ประเภทโรงเรียน')
          ->display_as('sch_address','ที่อยู่')
          ->display_as('sch_gmap_lat','พิกัด ละติจูด')
          ->display_as('sch_gmap_lon','พิกัด ลองติจูด')
          ->display_as('sch_image','รูปภาพ')
          ->display_as('sch_last_modify','แก้ไขล่าสุดเมื่อ')
          ->display_as('sch_last_modify_by','แก้ไขล่าสุดโดย');
      $crud->set_relation('sch_school_type','inno_school_type','st_description');
      $crud->set_relation('sch_last_modify_by','inno_users','{use_username}');

      $crud->add_fields('sch_name','sch_school_type','sch_address','sch_gmap_lat','sch_gmap_lon','sch_image');
      $crud->edit_fields('sch_name','sch_school_type','sch_address','sch_gmap_lat','sch_gmap_lon','sch_image');

      $crud->set_field_upload('sch_image','assets/imgs/school');
      
      $output = $crud->render();
      $data['crud_data'] = $output->output;

      $this->script($output->js_files); // load js script from crud

      $this->style($output->css_files); // load css script from crud

      $data['main_menu'] = $this->load_main_menu();
      $this->breadcrumb($this->title,base_url('admin/home'));
      $this->response('home',$data);
    }
}
        
    /* End of file  school.php */
