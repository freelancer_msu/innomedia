<!-- switchery CSS -->
<link href="<?=base_url('vendors/switchery/dist/switchery.min.css')?>
" rel="stylesheet" type="text/css"/>
<!-- bootstrap-tagsinput CSS -->
<link href="<?=base_url('vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')?>" rel="stylesheet" type="text/css"/>

<link href="<?=base_url('vendors/jasny-bootstrap/dist/css/jasny-bootstrap.min.css')?>" rel="stylesheet" type="text/css"/>


<div class="panel-wrapper collapse in">
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-12 col-xs-12">
				<div class="form-wrap">
					<form action="#">
						<div class="form-body">
							<div class="row">
								<div class="col-md-2">
									<div class="form-group">
										<label class="control-label mb-10">คำนำหน้า</label>
										<input type="text" id="firstName" class="form-control" placeholder="" value="<?=$pre_prefix?>" readonly>
										<!-- <span class="help-block"> This is inline help </span> --> 
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<label class="control-label mb-10">อักษรย่อ</label>
										<input type="text" id="firstName" class="form-control" placeholder="" value="<?=$pre_initial?>" readonly>
										<!-- <span class="help-block"> This is inline help </span> --> 
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label">สถานะ </label>
										<div>
											<input type="checkbox" <?=$pre_is_active=='A'?'checked':''?> class="js-switch js-switch-1"  data-color="#f1a1c7"/>
										</div>
										<span class="help-block" id="status_label">สถานะตอนนี้ : <text id="current_statud"><?=$pre_is_active=='A'?'ใช้งาน':'ไม่ใช้งาน'?></text></span>
									</div>
								</div>
								<!--/span-->
							</div>
							<!-- /Row -->
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Switchery JavaScript -->
<script src="<?=base_url('vendors/switchery/dist/switchery.min.js')?>"></script>
<!-- Bootstrap Tagsinput JavaScript -->
<script src="<?=base_url('vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')?>"></script>
<script src="<?=base_url('vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js')?>"></script>



<script type="text/javascript">
	$(document).ready(function() {
		var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
		$('.js-switch-1').each(function() {
			new Switchery($(this)[0], $(this).data());
		});
		$('.chi-iden').inputmask({mask: "9-9999-99999-99-9"});
	});
</script>