<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<html lang="en">
<!--<![endif]--><head>

<!-- Basic Page Needs -->
<meta charset="utf-8">
<title><?=$title?></title>
<meta name="description" content="EDU - Educational, College and Courses Boostrap site template with Responsive Megamenu 14$">
<meta name="author" content="Ansonika">

<!-- Favicons-->
<link rel="shortcut icon" href="<?=base_url('assets/imgs/education/favicon.ico')?>" type="image/x-icon"/>
<link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-glyphicon glyphicon-57x57-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-glyphicon glyphicon-72x72-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-glyphicon glyphicon-114x114-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-glyphicon glyphicon-144x144-precomposed.png">

<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- CSS -->
<?php $this->load->view($default_element."head_loader");?>

</head>

<body>
<!--[if !IE]><!--><script>if(/*@cc_on!@*/false){document.documentElement.className+=' ie10';}</script><!--<![endif]--> <!-- Border radius fixed IE10-->

<header>
	<div class="container">
   	  <div class="row">

        	<div class="col-md-4" id="logo">
                <a href="<?=base_url()?>">
                    <img src="<?=base_url('assets/imgs/education/logo.png')?>" alt="Logo">
                </a>
            </div>

        </div><!-- End row-->
    </div><!-- End container-->
</header><!-- End Header-->

<!-- navigate (menu) -->
<?php $this->load->view($default_element."navigate_pane");?>

<!-- content here -->
          <?php
            if(!empty($page)):
              if(is_array($page)):
                foreach ($page as $item) {
                  if(file_exists(APPPATH."modules/education/views/"."$item.php")){
                    $this->load->view("{$item}");
                  }else{
                    echo "File: $item Not Found";
                  }
                }
              elseif(isset($page)):
                if(file_exists(APPPATH."modules/education/views/"."$page.php")){
                  $this->load->view("{$page}");
                }else{
                  $this->load->view($default_element."not_found");
                }
              else:
                $this->load->view($default_element."not_found");
              endif;
            endif;
          ?>

<!-- footer -->
<?php $this->load->view($default_element."footer");?>

<div id="toTop">Back to Top</div>

<?php $this->load->view($default_element."foot_loader");?>

</body>
</html>