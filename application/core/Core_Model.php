<?php
class Core_Model extends CI_Model{

    public $_prefix = 'inno_';

    function __construct(){
        parent::__construct();
    }

    public function _insert($table,$data){
        return !$this->db->insert($table, $data) ? false : $this->db->insert_id();
    }

    public function _insert_get_id($table,$data){
        return !$this->db->insert($table, $data) ? false : $this->db->insert_id();
    }

    public function _insert_batch($table,$data){
        return !$this->db->insert_batch($table, $data) ? false : true;
    }

    public function _update($table,$col,$val,$data){
        $this->db->where($col, $val);
        return !$this->db->update($table, $data) ? false : true;
    }

    public function _update_where($table,$arr_where,$data){
        $this->db->where($arr_where);
        return !$this->db->update($table, $data) ? false : true;
    }

    public function _delete($table,$col,$val) {
        $this->db->where($col,$val);
        return !$this->db->delete($table) ? false : true;
    }

}