<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Child_type_model extends CI_Model {

  private $TABLE = 'child_type';

  public function __construct()
  {
    parent::__construct();
  }

  public function _get($code){
    $this->db->select('*');
    $this->db->from($this->TABLE);
    $this->db->where('ct_code', $code);
    

		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		}else {
			return false;
		}
  }

  public function _getAll(){
    $this->db->select('*');
		$this->db->from($this->TABLE);

		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		}else {
			return false;
		}
  }

  public function _get_all_with_user(){
    $this->db->select('*');
    $this->db->from('child_type');
    $this->db->join('users','ct_last_modify_by = use_id','left');

    $query = $this->db->get();

    if ($query->num_rows() >= 1) {
      return $query->result();
    }else {
      return false;
    }
  }

  public function _insert($data){
    return $this->db->insert($this->TABLE, $data);
  }

  public function _update($code,$data)
  {
      $this->db->where('ct_code',$code);
      return $this->db->update($this->TABLE, $data);
  }

  public function _delete($code)
  {
    $this->db->where('ct_code', $code);
    return $this->db->delete($this->TABLE);  
  }

}

/* End of file Child_type_model.php */
/* Location: ./application/models/admin/Child_type_model.php */