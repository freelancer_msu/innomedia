<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>ความรู้เกี่ยวกับเด็กพิเศษ</h1>
            <ul class="breadcrumb">
                <li><a href="<?= base_url('') ?>">หนัาหลัก</a> <span class="divider"></span></li>
                <li class="active">ความรู้เกี่ยวกับเด็กพิเศษ</li>
            </ul>
        </div>
        <!-- =========================Start Col left section ============================= -->
        <aside class="col-md-4 col-sm-4">
            <div class="col-left">
                <h3>ค้นหาบทความ</h3>
                <div class="form-group">
                    <form class="form-search form-inline">
                        <input id="speechText" type="text" class="input-medium form-control">
                        <button type="submit" class="button_medium" style="position:relative; top:2px;">ค้นหา</button>
                        <a class="button_medium btn_speech">พูด</a>
                    </form>
                </div>
                <hr>
                <h3>ประเภทเด็กพิเศษ</h3>
                <div class="widget">
                    <ul class="latest_news">
                        <?php
                        foreach($type_list as $key => $value){
                            ?>
                        <li><i class="icon-user"></i>
                            <div>
                                <a href="#"><?=$value->ct_description?></a>
                            </div>
                        </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>


        </aside>

        <!-- =========================Start Col right section ============================= -->
        <section class="col-md-8 col-sm-8">
            <div class="col-right">


                <div class="news-strip">
                    <ul>
                        <?php
                        foreach($list_data as $key => $value){
                            ?>
                        <li class="row">
                            <div class="date-news">
                                <img src="<?= base_url('assets/imgs/education/special-child/'.$value->info_icon) ?>">
                            </div>
                            <h5><a href="<?= base_url('news/'.$value->info_id) ?>"><?=$value->info_title?></a></h5>
                            <p><?=character_limiter(strip_tags($value->info_content), 100)?></p>
                        </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>

                <hr>
                <div class="text-center">
                    <ul class="pagination">
                        <li><a href="#">Prev</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">Next</a></li>
                    </ul>
                </div><!-- end pagination-->

            </div>

        </section>

    </div><!-- end row-->
</div> <!-- end container-->