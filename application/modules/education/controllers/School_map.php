<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School_map extends Core_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('school_model');
    }
	public function index()
	{
		$data['is_logged_in'] = $this->is_logged_in;
		$data['title'] = 'ค้นหาโรงเรียน';
		$data['data_lists'] = $this->school_model->_GetAll();
		$this->response('school_map',$data,'info_page');
	}

	public function get_All()
	{
		$data = $this->school_model->_GetAll();
        echo json_encode($data);
	}
}
