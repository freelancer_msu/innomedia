<!-- switchery CSS -->
<link href="<?=base_url('vendors/switchery/dist/switchery.min.css')?>
" rel="stylesheet" type="text/css"/>
<!-- bootstrap-tagsinput CSS -->
<link href="<?=base_url('vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')?>" rel="stylesheet" type="text/css"/>

<link href="<?=base_url('vendors/jasny-bootstrap/dist/css/jasny-bootstrap.min.css')?>" rel="stylesheet" type="text/css"/>


<div class="panel-wrapper collapse in">
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-12 col-xs-12">
				<div class="form-wrap">
					<form action="#">
						<div class="form-body">
							<div class="row">
								<div class="col-md-2">
									<div class="form-group">
										<label class="control-label mb-10">คำนำหน้า</label>
										<input type="text" id="firstName" class="form-control" placeholder="" readonly>
										<!-- <span class="help-block"> This is inline help </span> --> 
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<label class="control-label mb-10">ชื่อจริง</label>
										<input type="text" id="firstName" class="form-control" placeholder="" value="<?=$chi_first_name?>" readonly>
										<!-- <span class="help-block"> This is inline help </span> --> 
									</div>
								</div>
								<!--/span-->
								<div class="col-md-5">
									<div class="form-group">
										<label class="control-label mb-10">นามสกุล</label>
										<input type="text" id="lastName" class="form-control" placeholder="" value="<?=$chi_last_name?>" readonly>
										<!-- <span class="help-block"> This field has error. </span> --> 
									</div>
								</div>
								<!--/span-->
							</div>
							<!-- /Row -->
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label mb-10">เพศ</label>
										<select class="form-control" disabled>
											<option <?=$chi_gender=='M'?'selected':''?> value="">ชาย</option>
											<option <?=$chi_gender=='F'?'selected':''?> value="">หญิง</option>
										</select>
										<!-- <span class="help-block"> Select your gender </span> --> 
									</div>
								</div>
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label mb-10">รหัสประชาชน</label>
										
										<input type="text" class="form-control chi-iden" value="<?=$chi_identification?>" readonly>
										<span class="help-block" id="status_label">หมายเลขประจำตัวประชาชน 13 หลัก</span>
									</div>
								</div>
								<!--/span-->
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label">สถานะ </label>
										<div>
											<input type="checkbox" <?=$chi_is_active==1?'checked':''?> class="js-switch js-switch-1"  data-color="#f1a1c7"/>
										</div>
										<span class="help-block" id="status_label">สถานะตอนนี้ : <text id="current_statud"><?=$chi_is_active==1?'ใช้งาน':'ไม่ใช้งาน'?></text></span>
									</div>
								</div>
							</div>
							<!-- /Row -->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label mb-10">ประเภทเด็กพิเศษ</label>
										<div class="w-100">
											<select class="child-type form-control" multiple data-role="tagsinput" disabled>
												<?php if(empty($child_type)) { ?>
													<?php foreach ($child_types as $child_type) { ?>
														<option value="<?=$child_type->ct_description?>"></option>
													<?php } ?>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>
							<!-- /Row -->

							<div class="seprator-block"></div>

							<h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i>ที่อยู่</h6>
							<hr class="light-grey-hr"/>
							<div class="row">
								<div class="col-md-12 ">
									<div class="form-group">
										<label class="control-label mb-10">บ้านเลขที่ / อาคาร / หมู่ที่ / หมู่บ้าน</label>
										<input type="text" class="form-control">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label mb-10">ตำบล</label>
										<input type="text" class="form-control">
									</div>
								</div>
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label mb-10">อำเภอ</label>
										<input type="text" class="form-control">
									</div>
								</div>
								<!--/span-->
							</div>
							<!-- /Row -->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label mb-10">รหัสไปรษณีย์</label>
										<input type="text" class="form-control">
									</div>
								</div>
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label mb-10">ประเทศ</label>
										<select class="form-control">
											<option>--Select your Country--</option>
											<option>India</option>
											<option>Sri Lanka</option>
											<option>USA</option>
										</select>
									</div>
								</div>
								<!--/span-->
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Switchery JavaScript -->
<script src="<?=base_url('vendors/switchery/dist/switchery.min.js')?>"></script>
<!-- Bootstrap Tagsinput JavaScript -->
<script src="<?=base_url('vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')?>"></script>
<script src="<?=base_url('vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js')?>"></script>



<script type="text/javascript">
	$(document).ready(function() {
		var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
		$('.js-switch-1').each(function() {
			new Switchery($(this)[0], $(this).data());
		});
		$('.chi-iden').inputmask({mask: "9-9999-99999-99-9"});
	});
</script>