<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Amphures_model extends CI_Model {

    private $_table = 'amphures';
    private $_table_province = 'province';

    public function __construct()
    {
        parent::__construct();
    }

    public function _get_all(){
		$this->db->select('*');
		$this->db->from($this->_table);
		$this->db->join('province','amphure_province = pro_id','left');
		$this->db->where('amphure_active','A');
		$this->db->order_by('amphure_id','asc');

		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		}else {
			return false;
		}
	}

	public function _get_province(){
		$this->db->select('*');
		$this->db->from($this->_table_province);
		$this->db->order_by('pro_id','asc');

		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		}else {
			return false;
		}
	}

	public function _get_specific($col,$value){
		$this->db->select('*');
		$this->db->from($this->_table);
		$this->db->where($col,$value);

		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->row();
		}else {
			return false;
		}
	}
}