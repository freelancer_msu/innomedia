<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends MX_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('general_model');
    }
	public function index()
	{

	}

	public function get_provice()
    {
        $data = $this->general_model->_get_provice();
        echo json_encode($data);
    }

    public function get_amphures()
    {
        $provice_id = $this->input->post('provice_id');
        $data = $this->general_model->_get_amphures_by_province($provice_id);
        echo json_encode($data);
    }

    public function get_districts()
    {
        $provice_id = $this->input->post('provice_id');
        $amphure_id = $this->input->post('amphure_id');
        $data = $this->general_model->_get_districts_by_amphure($provice_id,$amphure_id);
        echo json_encode($data);
	}
	
}
