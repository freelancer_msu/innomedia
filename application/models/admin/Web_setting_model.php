<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Web_setting_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    // Slide manage

    public function _getSlide($id)
    {
        $this->db->select('web_slide.*,use_first_name,use_last_name');
        $this->db->from('web_slide');
        $this->db->join('users','use_id = ws_upload_by');
        $this->db->where('ws_id', $id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function _getAllSlide()
    {
        $this->db->select('web_slide.*,use_first_name,use_last_name');
        $this->db->from('web_slide');
        $this->db->join('users','use_id = ws_upload_by','left');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function _insertSlide($data)
    {
        return $this->db->insert('web_slide', $data);
    }

    public function _updateSlide($id, $data)
    {
        $this->db->where('ws_id', $id);
        return $this->db->update('web_slide', $data);
    }

    public function _deleteSlide($id)
    {
        $this->db->where('ws_id', $id);
        return $this->db->delete('web_slide');
    }

    // End slide manage
}
