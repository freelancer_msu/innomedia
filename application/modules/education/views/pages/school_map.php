<?php 
echo "<link href='".base_url('vendors/datatables/media/css/jquery.dataTables.css')."' rel='stylesheet'>"
    ."<link href='".base_url('vendors/datatables.net-responsive/css/responsive.dataTables.min.css')."' rel='stylesheet'>";
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1><?= $title; ?></h1>
            <ul class="breadcrumb">
                <li><a href="<?= base_url('') ?>">หนัาหลัก</a> <span class="divider"></span></li>
                <li class="active"><?= $title; ?></li>
            </ul>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row map-detail">
                <div class="col-xs-12">
                    <table class="display responsive nowrap" style="width:100%" id="school-table">
                        <thead>
                            <tr>
                                <td>โรงเรียน</td>
                                <td>ที่อยู่</td>
                                <td>ประเภท</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            if (is_array($data_lists) || is_object($data_lists)) {
                                foreach ($data_lists as $key => $value) {
                                    echo "<tr onclick='centermap($value->sch_gmap_lat,$value->sch_gmap_lon);'>";
                                    echo "<td>$value->sch_name</td>";
                                    echo "<td>$value->sch_address</td>";
                                    echo "<td>$value->st_description</td>";
                                    echo "</tr>";
                                }
                            } else {
                                echo "<tr>";
                                echo "<td colspan='4' style='text-align: center;'>ไม่พบข้อมูล</td>";
                                echo "</tr>";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <br>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <form action="#" class="form-inline">
                        <div class="input-group col-md-3 col-sm-6 col-xs-12">
                            <label for="provice">จังหวัด</label>
                            <select name="provice" id="provice" class="form-control"></select>
                        </div>
                        <div class="input-group col-md-3 col-sm-6 col-xs-12">
                            <label for="amphure">อำเภอ</label>
                            <select name="amphure" id="amphure" class="form-control"></select>
                        </div>
                        <div class="input-group col-md-3 col-sm-6 col-xs-12">
                            <label for="district">ตำบล</label>
                            <select name="district" id="district" class="form-control"></select>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id='map_canvas' class="map"></div>
                </div>
            </div>  
        </div>
    </div>
</div>

<?php 
echo "<script src='".base_url('vendors/datatables/media/js/jquery.dataTables.js')."'></script>"
    ."<script src='".base_url('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')."'></script>";
?>

<script>
    var map;
    var GGM;
    var Marker1;
    var infowindow = [];
    var infowindowTmp;
    var Marker = [];

    function initialize() {

        GGM = new Object(google.maps);
        var Latlng = new GGM.LatLng(16.0237418, 102.6136696);

        var mapTypeId = GGM.MapTypeId.ROADMAP;
        var DivObj = $("#map_canvas")[0];

        var mapOptions = {
            zoom: 11,
            center: Latlng,
            mapTypeId: mapTypeId
        };

        map = new GGM.Map(DivObj, mapOptions);

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = new GGM.LatLng(position.coords.latitude, position.coords.longitude);
                Marker1 = new GGM.Marker({
                    position: pos,
                    map: map,
                    animation: google.maps.Animation.DROP,
                    icon: "http://www.ninenik.com/demo/google_map/images/male-2.png",
                    draggable: true,
                });

                map.panTo(Marker1.getPosition());
                map.setCenter(pos);
            }, function() {
                // คำสั่งทำงาน ถ้า ระบบระบุตำแหน่ง geolocation ผิดพลาด หรือไม่ทำงาน
            });
        } else {
            // not support event.
        }

        var trafficLayer = new google.maps.TrafficLayer();
        trafficLayer.setMap(map);

        map.markers = [];
        $.get(
            "<?= base_url('education/school_map/get_All') ?>",
            function(result) {
                $.each(JSON.parse(result), function(key, value) {
                    var markerID = key + 1;
                    var markerName = value.sch_name;
                    var markerAddress = value.sch_address;
                    var markerLat = value.sch_gmap_lat;
                    var markerLng = value.sch_gmap_lon;

                    var markerLatLng = new GGM.LatLng(markerLat, markerLng);
                    Marker[markerID] = new GGM.Marker({
                        position: markerLatLng,
                        map: map,
                        animation: google.maps.Animation.DROP,
                        title: markerName
                    });
                    map.markers.push(Marker[markerID]);

                    infowindow[markerID] = new GGM.InfoWindow({
                        content: markerName + markerAddress
                    });

                    GGM.event.addListener(Marker[markerID], 'click', function() {
                        if (infowindowTmp) {
                            Marker[infowindowTmp].setAnimation(null);
                            infowindow[infowindowTmp].close();
                        }
                        toggleBounce(this);
                        infowindow[markerID].open(map, Marker[markerID]);
                        infowindowTmp = markerID;
                    });
                });
            }
        );

        function toggleBounce(marker) {
            if (marker.getAnimation() !== null) {
                marker.setAnimation(null);
            } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        }
    }

    function centermap(lat, lag) {
        var pos = new GGM.LatLng(lat, lag);
        map.setCenter(pos);
    }

    $(function() {
        $('#school-table').dataTable({
            "responsive": true,
            "oLanguage": {
                    "sLengthMenu": "แสดง _MENU_ ต่อหน้า",
                    "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
                    "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                    "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                    "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                    "sSearch": "ค้นหา :",
                    "oPaginate": {
                            "sFirst": "เริ่มต้น",
                            "sPrevious": "ก่อนหน้า",
                            "sNext": "ถัดไป",
                            "sLast": "สุดท้าย"
                    }
            }
        });

        $("<script/>", {
            "type": "text/javascript",
            src: "https://maps.google.com/maps/api/js?v=3.2&key=AIzaSyBEXF6zA5BPQu6bm5MMfMUhUaKi7CqYblo&language=th&callback=initialize"
        }).appendTo("body");

        var provice_select = $('#provice');
        var amphure_select = $('#amphure');
        var district_select = $('#district');


        provice_select.append('<option value="-1">ทั้งหมด</option>');
        amphure_select.append('<option value="-1">ทั้งหมด</option>');
        district_select.append('<option value="-1">ทั้งหมด</option>');

        amphure_select.prop('disabled', 'disabled');
        district_select.prop('disabled', 'disabled');

        $.get("<?= base_url('education/api/get_provice') ?>", function(result) {
            $.each(JSON.parse(result), function(key, value) {
                provice_select.append('<option value=' + value.pro_id + '>' + value.pro_name_th + '</option>');
            });
        });

        provice_select.on('change', function() {
            if (this.value == -1) {
                amphure_select.prop('disabled', 'disabled');
                district_select.prop('disabled', 'disabled');
            } else {
                geocodeAddress($("#provice option:selected").text());
                $.post("<?= base_url('education/api/get_amphures') ?>", {
                    provice_id: this.value
                }, function(result) {
                    amphure_select.empty();
                    district_select.empty();
                    amphure_select.append('<option value="-1">ทั้งหมด</option>');
                    district_select.append('<option value="-1">ทั้งหมด</option>');

                    amphure_select.prop('disabled', false);
                    $.each(JSON.parse(result), function(key, value) {
                        amphure_select.append('<option value=' + value.amphure_id + '>' + value.amphure_name_th + '</option>');
                    });
                });
            }
        });

        amphure_select.on('change', function() {
            if (this.value == -1) {
                district_select.prop('disabled', 'disabled');
            } else {
                geocodeAddress($("#provice option:selected").text() +
                    $("#amphure option:selected").text());
                $.post("<?= base_url('education/api/get_districts') ?>", {
                        provice_id: provice_select.val(),
                        amphure_id: this.value
                    },
                    function(result) {
                        district_select.empty();
                        district_select.append('<option value="-1">ทั้งหมด</option>');

                        district_select.prop('disabled', false);
                        $.each(JSON.parse(result), function(key, value) {
                            district_select.append('<option value=' + value.district_id + '>' + value.district_name_th + '</option>');
                        });
                    });
            }
        });

        district_select.on('change', function() {
            if (this.value != -1) {
                geocodeAddress($("#provice option:selected").text() +
                    $("#amphure option:selected").text() +
                    $("#district option:selected").text()
                );
            }
        });

    });

    function geocodeAddress(address) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            'address': address
        }, function(results, status) {
            if (status === 'OK') {
                map.setCenter(results[0].geometry.location);
                map.setZoom(12);
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }
</script>