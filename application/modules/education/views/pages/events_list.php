
<div class="container">
<div class="row">
	<div class="col-md-12">
	<h1>ข่าวสาร &amp; กิจกรรม</h1>
        <ul class="breadcrumb">
          <li><a href="<?=base_url('')?>">หนัาหลัก</a> <span class="divider"></span></li>
          <li class="active">ข่าวสาร &amp; กิจกรรม</li>
        </ul>
    </div>
<!-- =========================Start Col left section ============================= -->
<aside  class="col-md-4 col-sm-4">
<div class="col-left">
	<h3>ค้นหาข่าวสาร &amp; กิจกรรม</h3>
	<div class="form-group">
				<form class="form-search form-inline">
					<input id="speechText" type="text" class="input-medium form-control">
					<button type="submit" class="button_medium" style="position:relative; top:2px;">ค้นหา</button>
                    <a class="button_medium btn_speech">พูด</a>
				</form>
                </div>
	<hr>
	
</div>

            
</aside>

<!-- =========================Start Col right section ============================= --> 
<section class="col-md-8 col-sm-8">
	<div class="col-right">
        
        
        <div class="news-strip">
            <ul>
            <?php foreach ($list_data as $row_data) { ?>
                <li class="row">
                    <div class="date-news">
                        <img src="<?=base_url('assets/imgs/education/special-child/type-a.png')?>">
                    </div>
                        <h5><a href="<?=base_url('events/'.$row_data->info_id)?>"><img class="img-rounded" src="<?=base_url('assets/imgs/education/events-label.png')?>" style="max-height: 18px;"> <?=character_limiter($row_data->info_title,50)?></a></h5>
                        <p><?= character_limiter(strip_tags($row_data->info_content), 200) ?></p>
                </li>
            <?php } ?>
            </ul>
        </div>
        
           <hr>
           <div class="text-center">
               <ul class="pagination">
                    <li><a href="#">Prev</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">Next</a></li>
              </ul>
          </div><!-- end pagination-->
           
    </div>
	
</section>

  </div><!-- end row-->
  </div> <!-- end container-->