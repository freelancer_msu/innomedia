<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Child_type extends Core_Acontroller
{
  public $title = 'ประเภทเด็กพิเศษ';

  public function __construct()
  {
    parent::__construct();
    $this->load->model('admin/child_type_model');
    $this->breadcrumb('หน้าหลัก','admin/home');
  }

  public function index()
  {
    $data['main_menu'] = $this->load_main_menu();
    $data['page_title'] = 'ประเภทเด็กพิเศษ';
    $data['source_url'] = 'admin/child_type/list';
    $this->breadcrumb($this->title,'admin/child');
    $this->response('child_type_list',$data);
  }

  public function child_type_list(){
    if ($child_types = $this->child_type_model->_get_all_with_user()){
      $answer['data'] = array();
      $idx = 1;
      foreach ($child_types as $child_type) {
        $row = array($child_type);
        $row['id'] = $idx++;
        $row['description'] = $child_type->ct_description;
        if($child_type->ct_last_modify != ''){
          $row['update_date'] = $this->globals->dateformat_mini($child_type->ct_last_modify,'thai');
        }else{
          $row['update_date'] = '-';
        }
        
        $row['update_by'] = $child_type->use_first_name;
        array_push($answer['data'], $row);
      }
    }
    echo json_encode($answer);
  }

  public function grocery_crud_fn(){
    $this->config->load('grocery_crud');
    $this->config->set_item('grocery_crud_dialog_forms',true);
    $crud = new grocery_CRUD();
    $crud->unset_jquery();
    $crud->unset_export();
    $crud->unset_print();

    $crud->set_table('inno_child_type');
    $crud->set_subject('ประเภทเด็กพิเศษ');
    $crud->columns('ct_code','ct_description','ct_last_modify_by','ct_last_modify');
    $crud->display_as('ct_code','รหัส')
      ->display_as('ct_description','รายละเอียด')
      ->display_as('ct_last_modify','แก้ไขล่าสุดเมื่อ')
      ->display_as('ct_last_modify_by','แก้ไขล่าสุดโดย');
    $crud->set_relation('ct_last_modify_by','inno_users','{use_first_name} {use_last_name}');

    $crud->edit_fields('ct_description');
    $crud->add_fields('ct_description');

    $crud->callback_column('ct_last_modify',array($this,'modified_date_callback'));

    $crud->callback_before_update(array($this,'au_child_type'));

    $output = $crud->render();
    $data['crud_data'] = '<div class="row">
                            <div class="col-md-12">'
                            .$output->output
                          .'</div>
                          </div>';

      $this->script($output->js_files); // load js script from crud

      $this->style($output->css_files); // load css script from crud

      $data['main_menu'] = $this->load_main_menu();
      $this->breadcrumb($this->title,base_url('admin/home'));
      $this->response('home',$data);
  }

  public function modified_date_callback($value, $row){
    if(empty($row->ct_last_modify)){
      $answer = '';
    }else{
      $answer = $this->globals->dateformat_full(date($row->ct_last_modify),'thai');
    }
    return $answer;
  }

  public function au_child_type($post_array, $primary_key){
    $post_array['ct_last_modify_by'] = $this->ion_auth->user()->row()->use_id;
    $post_array['ct_last_modify'] = date('Y-m-d h:i:s', time());
    return $post_array;
  }

}


/* End of file Child_type.php */
/* Location: ./application/controllers/admin/Child_type.php */