<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_child extends Core_Acontroller {

	public $title = 'เด็กพิเศษ';

	private $_error_retreived = 'มีปัญหาเกี่ยวกับการดึงข้อมูล กรุณาลองใหม่อีกครั้งค่ะ';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/child_model');
		$this->breadcrumb('หน้าหลัก','admin/home');
	}

	public function index() {
		$data['main_menu'] = $this->load_main_menu();
		$data['page_title'] = 'เด็กพิเศษ';
		$data['source_url'] = 'admin/child/list';
		$this->breadcrumb($this->title,'admin/child');
		$this->response('child_list',$data);
	}

	public function child_list(){
		if ($childs = $this->child_model->_get_all()){
			$answer['data'] = array();
			$idx = 1;
			foreach ($childs as $child) {
				$row = array($child);
				$row['id'] = $idx++;
				$row['full_name'] = $child->chi_first_name.' '.$child->chi_last_name;
				$row['age'] = $this->globals->get_age($child->chi_birth_date) .' ปี ('. $this->globals->dateformat_mini($child->chi_birth_date,'thai').')';
				$row['school_name'] = $child->sch_name;
				$row['status'] = $child->chi_is_active;
				array_push($answer['data'], $row);
			}
		}

		echo json_encode($answer);
	}

	public function child_view(){
		if ($child = $this->child_model->_get_specific('chi_id',$this->input->post('_id'))){
			if ($child_type = $this->child_model->_get_child_type($this->input->post('_id'))){
				$child->child_types = $child_type;
			}
			$answer['status'] = 1;
			$answer['title'] = "<i class='zmdi zmdi-account mr-10'></i>ข้อมูลเด็กพิเศษ";
			$answer['message'] = $this->load->view('pages/admin/modals/view_child',$child,TRUE);
			
		} else {
			$answer['status'] = 0;
			$answer['message'] = $this->_error_retreived;
		}
		echo json_encode($answer);
	}

	public function child_insert(){
		$data['main_menu'] = $this->load_main_menu();
		$data['page_title'] = 'เด็กพิเศษ';
		$data['source_url'] = 'admin/child/list';
		$this->load->model('admin/child_type_model');
		$data['all_child_types'] = $this->child_type_model->_getAll();
		$this->breadcrumb($this->title,'admin/child');
		$this->breadcrumb('เพิ่มข้อมูลเด็กพิเศษ','admin/child/insert');
		$this->response('child_card_insert',$data);
	}

	public function child_edit($chi_id){
		$data['main_menu'] = $this->load_main_menu();
		$data['page_title'] = 'เด็กพิเศษ';
		$data['source_url'] = 'admin/child/list';
		$data['child_data'] = $this->child_model->_get_specific('chi_id',$chi_id);
		$data['child_in_type'] = [];
		if ($child_type = $this->child_model->_get_child_type($chi_id)){
			foreach ($child_type as $type) {
				array_push($data['child_in_type'],$type->ct_code);
			}
		}
		$this->load->model('admin/child_type_model');
		$data['all_child_types'] = $this->child_type_model->_getAll();
		$this->breadcrumb($this->title,'admin/child');
		$this->breadcrumb('แก้ไขข้อมูลเด็กพิเศษ','admin/child/edit');
		$this->response('child_card_edit',$data);
	}

	public function test_noseries($code){
		$this->load->model('no_series_model');
		$new_nos = $this->no_series_model->_get_specific_noseries($code);
		echo json_encode($new_nos);
	}

	private function _return_404(){
		redirect('404_override');
	}

	public function grocery_crud_fn(){
		$this->config->load('grocery_crud');
		$crud = new grocery_CRUD();

		$crud->unset_jquery();
		$crud->unset_export();
		$crud->unset_print();

		$crud->set_table('inno_child');
		$crud->set_subject('เด็กพิเศษ');
		$crud->columns('chi_full_name','chi_age','chi_school');
		$crud->display_as('chi_full_name','ชื่อเด็กพิเศษ')
			->display_as('chi_age','อายุ')
			->display_as('chi_school','สังกัด โรงเรียน')
			->display_as('chi_first_name','ชื่อ')
			->display_as('chi_last_name','นามสกุล')
			->display_as('chi_type_a','ประเภทเด็กพิเศษ')
			->display_as('chi_birth_date','วัน/เดือน/ปี เกิด');
		$crud->add_fields('chi_first_name', 'chi_last_name', 'chi_birth_date', 'chi_school','chi_type_a');
		$crud->edit_fields('chi_first_name', 'chi_last_name', 'chi_birth_date', 'chi_school','chi_type_a');
		$crud->required_fields('chi_first_name', 'chi_school','chi_birth_date');

		$crud->set_relation('chi_school','inno_school','{sch_name}');
		$crud->set_relation_n_n('chi_type_a','child_in_type','child_type','cit_child_id','cit_type_id','ct_description','cit_order');

		$crud->callback_column('chi_full_name',array($this,'full_name_callback'));
		$crud->callback_column('chi_age',array($this,'cal_age'));
		
		$output = $crud->render();
		$data['crud_data'] = $output->output;

		$this->script($output->js_files); // load js script from crud

		$this->style($output->css_files); // load css script from crud

		$data['main_menu'] = $this->load_main_menu();
		$this->breadcrumb($this->title,base_url('admin/home'));
			$this->response('home',$data);
	}

	//call back function
	public function full_name_callback($value, $row){
		return $row->chi_first_name." ".$row->chi_last_name;
	}
	public function chi_type_callback($value, $row){
		$temp_array = json_decode($row->chi_type);
		return $temp_array;
	}
	public function bi_chi_type_callback($post_array){
		$post_array['chi_type'] = json_encode($post_array['chi_type']);
		return $post_array;
	}
	public function cal_age($value, $row){
		$answer = $this->globals->get_age($row->chi_birth_date) .' ('. $this->globals->dateformat_mini($row->chi_birth_date,'thai').')';
		return $answer;
	}
}