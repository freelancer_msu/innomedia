<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class No_series_model extends Core_Model {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function _get_specific_noseries($code){
		$this->db->select('*');
		$this->db->from('noseries');
		$this->db->where('nos_code',$code);

		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			$row = $query->row();
			$nos_prefix = $row->nos_prefix_code;
			$year2digit = substr(date("Y"),-2);
			$running_update = ($row->nos_running+1);
			$fill_nos = substr('0000'.$running_update,-3);
			$temp_nos = $nos_prefix.$year2digit.$fill_nos;
			$answer = new stdClass;
			$answer->code = $code;
			$answer->nos = $temp_nos;
			$answer->running = $running_update;
			return $answer;
		}else {
			return false;
		}
	}
}