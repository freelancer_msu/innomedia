<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_groups extends Core_Acontroller {

	public $title = 'Groups';

	public function __construct()
	{
		parent::__construct();
		$this->config->load('grocery_crud');
	}

	public function index() {
		$crud = new grocery_CRUD();

		$crud->unset_jquery();
		$crud->unset_export();
		$crud->unset_print();

		$crud->set_table('inno_groups');
		$crud->set_subject('Groups');
		$crud->columns('grp_name','grp_description','grp_status');
		$crud->set_relation('grp_status','inno_group_status','{gsta_name}');

		$output = $crud->render();
		$data['crud_data'] = $output->output;

		$this->script($output->js_files); // load js script from crud

		$this->style($output->css_files); // load css script from crud

		$data['main_menu'] = $this->load_main_menu();
		$this->breadcrumb($this->title,base_url('admin/home'));
		$this->response('home',$data);
  }
}